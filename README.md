## Author

Name:
* Takeru Yamada

Education:
* 2017.03: Bachelor of Science in Physics, Rikkyo University, Tokyo, Japan
* 2019.03: Master of Science in Physics, Rikkyo University, Tokyo, Japan

Publications:
* [Yamada, T., Imamura, T., Fukuhara, T. et al. Influence of the cloud-level neutral layer on the vertical propagation of topographically generated gravity waves on Venus. Earth Planets Space 71, 123 (2019). https://doi.org/10.1186/s40623-019-1106-7](https://doi.org/10.1186/s40623-019-1106-7)

Interests:
* Venus atmosphere
* Venus Climate Orbiter (Akatsuki)
* IDL (Interactive Data Language)
* Python
* Texpad
* Macbook
* Book
* Movie (e.g. Documentary)
* DIY furniture (e.g. Standing desk)
