---

title: 
slug: /
date: 
tags: []
image: 
socialImage: 
imageAlt: eyecatch

marp: true
theme: typememo
paginate: true
_class: lead
_header: typememo.jp
_footer: © 2020 typememo

---

Contents

...

---

## はじめに

...

---

## おわりに

...

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

---

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k) (ステッパー)
- [HHKB Professional 墨](https://amzn.to/3dGtLS1) (キーボード)
- [HHKB キートップセット 白](https://amzn.to/2HzzPQD) (キートップ)
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk) (マウス)
- [Apple MacMini](https://amzn.to/34jvsSt) (PC)
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B) (ディスプレイ)
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ) (コップ)
