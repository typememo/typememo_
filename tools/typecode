#!/bin/bash


# Define
PATH_TO_TOOLS=$(cd $(dirname $0); pwd)
TEMPLATE="$PATH_TO_TOOLS/res/template/manuscript.md"
MANUSCRIPT=$1


# Functions
usage() {
  cat << EOT
Usage: typecode /path/to/manuscript.md
EOT
  return 0
}

help() {
  cat << EOT
Usage:
  typecode /path/to/manuscript.md

Description:
  Generate blog post template.

Options:
  -h  Show help
EOT
  return 0
}


# Error handling
if [ $# -eq 0 ]; then
  usage
  exit 0
fi

if [ -d ${MANUSCRIPT} ]; then
  echo -e "Invalid Argument: ${MANUSCRIPT} is a directory"
  usage
  exit 0
fi

# Option analysis
for OPT in "$@"
do
  case $OPT in
    -h)
      help
      exit 0
      ;;
  esac
done


# Open existed manuscript with vscode
if [ -f ${MANUSCRIPT} ]; then
  echo "Open existed manuscript"
  code ${MANUSCRIPT}
  exit 0
fi


# Create new manuscript
mkdir -p $(dirname $MANUSCRIPT)
mkdir -p $(dirname $MANUSCRIPT)/img
touch $MANUSCRIPT
cat $TEMPLATE > $MANUSCRIPT
echo "Create new manuscript"


# Open new manuscript with vscode
code $MANUSCRIPT
echo "Open new manuscript"
