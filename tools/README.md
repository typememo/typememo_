# tools

- [gmail.py](./gmail.py)
  - Gmail API Wrapper
- [typecode](./typecode)
  - Template generator for Gatsby JS blog post
  - [template.md](./res/template.md) is the template
