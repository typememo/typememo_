---

title: Texpadでタイプセットが終わらない時の対処方法
slug: /tech/texpad-endless-typeset/
date: 2018-12-24
tags: [tech, texpad]
image: ./img/retrosupply-jLwVAUtLOAQ-unsplash.jpg
socialImage: ./img/retrosupply-jLwVAUtLOAQ-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [sierra => mojave にアップデートしたことが原因なのでは？](#sierra--mojave-にアップデートしたことが原因なのでは)
- [OSのアップデートが原因ではないとしたら、ファイルがクラッシュしているのでは？](#osのアップデートが原因ではないとしたらファイルがクラッシュしているのでは)
- [おわりに](#おわりに)

<!-- /TOC -->

---

## はじめに

texpadを使って文書を編集して、タイプセットしても、一向にタイプセットが終わらず、編集結果が反映されないというトラブルに巻き込まれたことはありませんか？

僕の経験に基づく解決策は２つです。

以下にその解決策を紹介します。

## sierra => mojave にアップデートしたことが原因なのでは？

Mac OSをmojaveに変えたのが原因でtexpadの動きがおかしくなっているのなら、以下の記事を読んで今抱えている問題を解決してください。

- https://typememo.com/texpad-sierra-mojave-warning/

## OSのアップデートが原因ではないとしたら、ファイルがクラッシュしているのでは？

それでも、texpadの動きがおかしいという方は以下の通り作業してみてください。

- いま問題になっているファイルの本文を全選択してコピーする。
- 新規ファイルを作成して、そのコピーした本文をその新規ファイルに貼り付ける。
- ダメ元で、タイプセットしてみる。

## おわりに

僕の場合は、これでうまくいったのですが、いかがでしょうか？

確かな理由はわかりませんが、何かしらの理由でファイルがクラッシュしてしまったんだと思われます。

クラッシュしてしまったファイルは早々と諦めましょう。

最後まで、お読みいただきありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

