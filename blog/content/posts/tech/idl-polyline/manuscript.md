---

title: idl polyline の使い方
slug: /tech/idl-polyline/
date: 2019-09-15
tags: [tech, idl]
image: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
socialImage: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [具体例](#具体例)
- [参考資料 & おすすめ書籍](#参考資料--おすすめ書籍)

<!-- /TOC -->

----

## はじめに

polyline() を使うことで、図に線を引くことができます。

polyline() の基本的な文法は以下の通りです。

```python
plt = plot(X, Y, /buffer)
line = polyline([x1, x2], [y1, y2], target = plt, /data)
```

基本的には (x1, y1) が始点で (x2, y2) が終点です。

応用すれば、線をつなぐ点の個数は、(x1, y1), (x2, y2), (x3, y3), ... というようにいくつまででも増やすことができます。

つまり、四角や三角などの図形も描くことができます。

しかし、全ての点を同じ線でつなぐので、線の種類や色を変えたい場合は、`polyline()` を複数回使ってください。

## 具体例

以下に具体例を示します。
今回は、[0, 2π] の範囲で、`y = |cos(x)|` を `plot()` で描き、その図の全体にばつ印を `polyline()` で描いてみました。

出力結果とソースコードは以下の通りです。

![figure_sample_polyline](./img/figure_sample_polyline.png)

```python
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; x [0, 2π] で
;; y = abs(cos(x)) をプロットし、
;; 図の全体に バツ印を書いてみる。
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro sample_polyline

saveDirName = '~/Downloads/'
saveFileName = 'figure_sample_polyline.png'

n = 100d
x = dindgen(n, start = 0, increment = 2d*!pi/n)
y = abs(cos(x))

plt = plot(x, y,$
    ; x 
    xtitle = 'x [rad]',$
    xrange = [0, 2d*!pi],$
    xtickvalues = [0, !pi, 2d*!pi],$
    xtickname = ['0', '$\pi$', '2$\pi$'],$
    xminor = 1,$
    ; y
    ytitle = '|cos(x)|',$
    yrange = [0, 1],$
    ytickvalues = [0, 1],$
    ytickname = ytickvalues,$
    yminor = 4,$
    ; title
    title = '',$
    ; linestyle
    linestyle = 0,$ ; solid line
    thick = 2,$
    color = 'black',$
    ; symbol
    symbol = 'o',$
    sym_size = 0,$
    sym_color = 'black',$
    sym_filled = 1,$
    ; font
    font_size = 14,$
    font_name = 'Helvetica',$
    ; option
    /buffer)

line = polyline([0, 2d*!pi], [1, 0],$
    target = plt,$
    linestyle = 2,$ ; dashed line
    color = 'black',$
    /data)

line = polyline([0, 2d*!pi], [0, 1],$
    target = plt,$
    linestyle = 2,$ ; dashed line
    color = 'black',$
    /data)

plt.save, saveDirName + saveFileName

end
```

## 参考資料 & おすすめ書籍

この記事は IDL Reference を参考にして執筆しました。

- [IDL Reference polyline()](https://www.harrisgeospatial.com/docs/POLYLINE.html)
- [IDL Reference plot()](https://www.harrisgeospatial.com/docs/PLOT.html)

おすすめ書籍は以下になります。

- [IDLプログラミング入門 -基本概念から3次元グラフィック作成まで- (KS理工学専門書)](https://amzn.to/2ZZed7q)

最後までお読みいただき、ありがとうございました：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

