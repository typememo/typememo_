---

title: idl legend() を使って凡例を付ける方法
slug: /tech/idl-legend/
date: 2019-10-27
tags: [tech, idl]
image: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
socialImage: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [legend() の文法](#legend-の文法)
- [legend() の具体例](#legend-の具体例)
- [おわりに](#おわりに)

<!-- /TOC -->

## はじめに

idl の legend() 関数を使って、グラフに凡例を付ける方法を紹介します。

この記事の対象者は、idl の plot() 関数を使った図の出力はできるようになったけど、図の凡例の付け方が分からなくて困っている人です。

この記事を読んでその内容を実践すれば図の凡例が簡単に付けられるようになるはずです。

興味のある方は、そのまま読み進めていただければと思います。

それでは、参りましょう．

## legend() の文法

legend() 関数の、基本的な文法は以下の通りです。

```idl
plt0 = plot(...)

plt1 = plot(..., /overplot)

; legend() 
plt_legend = legend($
  target = [plt0, plt1],$
  position = [xUpperRigth, yUpperRight],$
  /data,$
  /auto_text_color)
```

`legend()` を使う時に定義する引数は基本的に以下の２つです。

- target = [ ]
  - : 凡例を表示する図を定義する
- position = [xUpperRight, yUpperRight] 
  -  : 凡例ボックスの右上の座標を定義する

ここでは `plt0` と `plt1` を図の凡例として出力すると想定しているので、`target = [plt0, plt2]` と定義しています。

`/data` は `plot` で使用しているプロットデータを基準にして、`position` を決定するという意味です。

`/auto_text_color` は `plot` 内で `color` を定義した場合、その定義された色と同じ色を凡例にも適応するという意味です。

補足ですが、`legend()` は最後に出力した図に、凡例を付けるようになっています。

上の例だと `plt1` は `/overplot` しているので、`plt0` に凡例がつきます。

さて、続いては実際に `legend()` を使ってみましょう。

## legend() の具体例

簡単な例を使って、`legend()` の使い方を紹介します。

sin 関数を **黒色 black** で `plt0` として出力し、cos 関数を **赤色 red** で `plt1` として出力し、その図に凡例を付けてみたいと思います。

ソースコードを以下に提示しました。
idl を読むのに慣れている人は、以下のソースコードだけ見れば何をしているのかわかると思います。

```idl
pro sample_legend

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; x [0, 2pi]
; sin (color = 'black')
; cos (color = 'red')
; + legend
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

saveDirName = '~/Downloads/'
saveFileName = 'figure_sample_legend.png'

n = 100d
x = dindgen(n, start = 0, increment = 2d*!pi/n)
y0_sin = sin(x)
y1_cos = cos(x)

plt0 = plot(x, y0_sin,$
    ; x 
    xtitle = 'x [rad]',$
    xrange = [0, 2d*!pi],$
    xtickvalues = [0, !pi, 2d*!pi],$
    xtickname = ['0', '$ \pi $', '2 $\pi $'],$
    xminor = 1,$
    ; y
    ytitle = '',$
    yrange = [-1, 1],$
    ytickvalues = [-1, 0, 1],$
    ytickname = ytickvalues,$
    yminor = 4,$
    ; title
    title = '',$
    ; name
    name = 'sin()',$
    ; linestyle
    linestyle = 0,$ ; solid line
    thick = 2,$
    color = 'black',$
    ; font
    font_size = 14,$
    font_name = 'Helvetica',$
    ; option
    /buffer)

plt1 = plot(x, y1_cos,$
    ; name
    name = 'cos()',$
    ; linestyle
    linestyle = 0,$ ; solid line
    thick = 2,$
    color = 'red',$
    ; option
    /buffer,$
    /overplot)

plt_legend = legend($
    target = [plt0, plt1],$
    position = [1.85*!pi, 0.90],$
    font_name = 'Helvetica',$
    font_size = 8,$
    /data,$
    /auto_text_color)

plt_polyline = polyline($
    [0, 2d * !pi],$
    [0, 0],$
    target = plt0,$
    /data)

plt0.save, saveDirName + saveFileName

end
```

`plot()` で分からない箇所がある方は、[idl plot の使い方](https://typememo.com/idl-plot/) をご覧ください。

`plt_legend = legend(...)` で凡例を付けています。

書き方は、`font_name` と `font_size` を指定している箇所が異なりますが、先ほど **legend() の文法** で紹介したものとほとんど同じです。

`legend()` 関数のより奥深い使い方は [IDL Reference legend()](https://www.harrisgeospatial.com/docs/LEGEND.html) をご覧ください。

## おわりに

IDL の基本から応用まで幅広く扱っている日本語の書籍は ２０１９ 年現在で 2-3 冊しかありません。
その中で私のおすすめ書籍は、[IDLプログラミング入門―基本概念から3次元グラフィックス作成まで― (KS理工学専門書](https://amzn.to/2pZgn6t) です。
基本から応用まで幅広く知ることができるようになります。
IDL の書籍って需要がほとんどないので、その分単価が高くなっていまして、本一冊で約１万円します。
個人で買うのは厳しいところがありますので、指導教員の研究費で買うのが良いと思います。

この記事が誰か一人の役にでも立てば、嬉しい限りです：）

最後までお読みいただき、ありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

