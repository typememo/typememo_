---

title: Gitlab SSH Key 設定エラーの対処方法まとめ
slug: /tech/git-ssh-key-errors/
date: 2020-08-09
tags: [tech, git]
image: ./img/yancy-min-842ofHC6MaI-unsplash.jpg
socialImage: ./img/yancy-min-842ofHC6MaI-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [そもそも SSH key が PC に生成されていない場合](#そもそも-ssh-key-が-pc-に生成されていない場合)
- [Gitlab に SSH key が設定されていない場合](#gitlab-に-ssh-key-が設定されていない場合)
- [$HOME/.ssh/config を設定していない場合](#homesshconfig-を設定していない場合)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

本記事は、[Gitlab and SSH keys](https://gitlab.com/help/ssh/README#gitlab-and-ssh-keys) に記載されている、SSH key 設定エラーの対処方法をまとめたものです。

SSH key 認証がきちんとされていると Git を快適に使えるようになります。

よく引っかかる順番に対処方法をまとめています。

## そもそも SSH key が PC に生成されていない場合

お使いの PC に SSH key が生成されていない場合は、以下のコマンドで SSH Key を生成してください。

```shell
ssh-keygen -t rsa -b 4096 -C "email@example.com"
```

上記コマンドを実行すると以下のようなメッセージが随時出力されます。

```shell
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/takeru/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```

１行目は、SSH key のペア (public と private) が生成されますよ、というメッセージです。

２行目は、生成される SSH key をどこになんという名前で保存するかを聞いています。
何も入力せずに Enter キーを叩くと $HOME/.ssh/id_rsa という名前で保存されます。

３行目は、SSH key にパスワードを設定するか聞いています。
何も入力せずに Enter キーを叩くとパスワードは設定されません。

４行目は、そのパスワードの確認です。

以上で、PC に SSH key を生成する作業が終了しました。

もしも、Gitlab に SSH key を設定していない場合は、次節も続けてお読みくださいませ。

## Gitlab に SSH key が設定されていない場合

PC に SSH key は生成したけど、Gitlab に SSH key を設定していない場合の対処方法です。

まず、Gitlab.com にアクセスして、Sign in してください。

Sign in したら

* Your Icon 
* => Settings 
* => SSH Keys 

にアクセスします。

Key 欄 (下図参照) に先ほど生成した SSH key を貼り付ける必要がありますので、以下のようなコマンドを実行して SSH key (public) をターミナルに出力してください。

```shell
# id_rsa.pub は適宜置換してください
cat $HOME/.ssh/id_rsa.pub
```

出力結果をコピーして、先ほどの Key 欄に貼り付けてください。

貼り付けたら、Title をお好きなモノに変更して、Add key をクリックしてください。

![Figure.Gitlab.SSH.Key](./img/Screenshot_2020-01-13_15.03.18.png)

以上で、Gitlab に SSH Key を設定する作業の終了です。

Gitlab に SSH key は登録しているのに、`git fetch` コマンドや `git pull` コマンドを実行した時に、SSH key 認証ができていないとエラーになってしまう方は、PC の `$HOME/.ssh/config` ファイルが正しく設定されていない場合があります。

以下にその対処方法について紹介します。

## $HOME/.ssh/config を設定していない場合

新規ターミナルで git の操作した時や、再起動したあとで git の操作をした時に、SSH key 認証エラーになる場合の対処方法です。

`git fetch` や `git pull` コマンドを実行した時に、認証ができていないというエラーになってしまう方は、`$HOME/.ssh/config` ファイルの設定ができていない場合が多いです。

特に、複数の Git アカウントを同じ PC で扱っていると頻繁にこのエラーに遭遇します。

新規ターミナルを立ち上げた時に、認証エラーになる方も `$HOME/.ssh/config` を設定していないことが多いです。

ということで、`$HOME/.ssh/config` を作成 / 編集しましょう。

```shell
vi $HOME/.ssh/config
```

以下のような設定を追記してください。
ご自身の環境に合うように適宜変更してください。

```shell
# GitLab.com
Host gitlab.com
Preferredauthentications publickey
IdentityFile ~/.ssh/gitlab_com_rsa

# Private GitLab instance
Host gitlab.company.com
Preferredauthentications publickey
IdentityFile ~/.ssh/example_com_rsa
```

なお、gitlab.com に複数のアカウントを所有していて、それぞれのアカウントに紐づいた SSH key を設定したい場合は、以下のような設定を追記してください。

```shell
# User1 Account Identity
Host user_1.gitlab.com
Hostname gitlab.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/example_ssh_key1

# User2 Account Identity
Host user_2.gitlab.com
Hostname gitlab.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/example_ssh_key2
```

あとは保存して終了です。

以上で、新規ターミナルを立ち上げた時や PC を再起動した時に、SSH 認証エラーになる場合の対処方法は終了です。お疲れ様でした。

## おわりに

Gitlab の SSH key 認証でエラーになる場合の対処方法を紹介しましたが、お目当ての対処方法はありましたでしょうか？

本記事が少しでも役に立てれば、筆者としてとても嬉しいです。

この記事は git 初心者の筆者が、頭を整理するために書いた備忘録です。

それ故に、間違いなどがあったかもしれません。

git 上級者の皆さま、温かい目で見ていただき、優しいコメントを頂ければ幸いです。

Gitlab のことを体系的に勉強したい方は、[Gitlab 実践ガイド](https://amzn.to/2tSoP9J) がオススメです。

最後までお読みいただきありがとうございました．

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

