---

title: スティッキーズ・付箋の内容をメモに保存する方法 (Macbook)
slug: /tech/macos-stickies-save-in-notes/
date: 2019-11-02
tags: [tech, mac, macos, macbook]
image: ./img/lauren-mancke-aOC7TSLb1o8-unsplash.jpg
socialImage: ./img/lauren-mancke-aOC7TSLb1o8-unsplash.jpg
imageAlt: eyecatch
---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [付箋の内容をメモに保存する手順](#付箋の内容をメモに保存する手順)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

スティッキーズの内容をメモアプリに保存する方法についての紹介です．

## 付箋の内容をメモに保存する手順

以下に付箋の内容をメモに保存するまでの手順を示します。

- **スティッキーズ > ファイル (ツールバー) > すべてを"メモ"に書き出す...** をクリック (Figure1)
- **すべてを書き出す** をクリック
- **メモを読み込む** をクリック
- メモの **読み込まれたメモ** フォルダの **Stickies-Export-...** というフォルダの中に、付箋の色で分類されて、付箋の内容がテキストとして保存される。

![Figure1](./img/Screenshot_2019-11-02_15.15.08.png)

これで、付箋の内容をメモに保存するまでが終了しました。

## おわりに

短いですが，付箋の内容をメモに保存する方法についての紹介でした．

最後までお読みいただきありがとうございました．

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

