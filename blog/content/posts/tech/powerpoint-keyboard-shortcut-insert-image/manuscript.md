---

title: Power Point for Mac の画像挿入ショートカットキー
slug: /tech/powerpoint-keyboard-shortcut-insert-image/
date: 2019-08-20
tags: [tech, powerpoint]
image: ./img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg
socialImage: ./img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [設定方法](#設定方法)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

Power Point for Mac には画像挿入のためのショートカットキーはデフォルトでは存在しません。

そのため、自分で独自に設定する必要があります。

本記事では、ショートカットキーを一発入力するだけで画像挿入画面を立ち上げる設定方法をご紹介します。

## 設定方法

- Appleアイコン
- => システム環境設定
- => キーボード
- => ショートカット 
- => アプリケーション
- => ＋ ボタン

以下のように，

- **アプリケーション = Microsoft PowerPoint**
- **メニュータイトル = 画像をファイルから挿入...**
- **キーボードショートカットキー = Ctrl + i** 

と設定します。

**追加** をクリックすれば設定完了です。

![Figure2](./img/Screenshot_2019-08-19_21.51.51.png)

## おわりに

最後までお読みいただき、ありがとうございました：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

