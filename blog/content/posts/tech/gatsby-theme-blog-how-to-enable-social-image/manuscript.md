---

title: gatsby-theme-blog で SNS アイキャッチ画像を表示する方法
slug: /tech/gatsby-theme-blog-how-to-enable-social-image/
date: 2020-07-15
tags: [tech, gatsby, image]
image: ./img/gary-bendig-6GMq7AGxNbE-unsplash.jpg
socialImage: ./img/gary-bendig-6GMq7AGxNbE-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [siteUrl を siteMetadata に追加する](#siteurl-を-sitemetadata-に追加する)
- [socialImage 投稿記事のフロントマターに追加する](#socialimage-投稿記事のフロントマターに追加する)
- [おわりに](#おわりに)
- [References](#references)

<!-- /TOC -->

----

## はじめに

`gatsby-theme-blog` で Twitter や Facebook などで記事を共有したときに表示されるアイキャッチ画像を表示する方法についての記事です．

同じ問題を抱えている人が，この記事を読んでその問題が解決できたらとても嬉しいです．

それでは，参りましょう．

## siteUrl を siteMetadata に追加する

アイキャッチ画像を表示するには `gatsby-config.js` の `siteMetadata` に `siteUrl` を追加する必要があります．[(R)](https://www.npmjs.com/package/gatsby-theme-blog#image-behavior)

まず `gatsby-config.js` をエディタで開いてください．

エディタで開いたら `gatsby-config.js` 内の `siteMetadata` を見つけてください．

`siteMetadata` を見つけたら，`siteUrl: https://www.example.com` のフォーマットでサイト URL を追加してください．

具体例:

```javascript
// gatsby-config.js
module.exports = {
  siteMetadata: {
    // Used for the site title and SEO
    title: `My Blog Title`,
    // Used to provide alt text for your avatar
    author: `My Name`,
    // Used for SEO
    description: `My site description...`,
    // Used for resolving images in social cards
    siteUrl: `https://example.com`,
    // Used for social links in the root footer
    social: [
      {
        name: `Twitter`,
        url: `https://twitter.com/gatsbyjs`,
      },
      {
        name: `GitHub`,
        url: `https://github.com/gatsbyjs`,
      },
    ],
  },
}
```

`gatsby-config.js` での作業はこれでおしまいです．

続いて，投稿記事のフロントマターを編集しに行きましょう．

## socialImage 投稿記事のフロントマターに追加する

投稿記事のフロントマターに `socialImage:` を追記する必要があります．[(R)](https://www.npmjs.com/package/gatsby-theme-blog#blog-post-fields)

`socialImage: /path/to/image` のフォーマットでフロントマターに追記すれば OK です．

具体例:

```markdown
---

title: Hello World (example)
date: 2019-04-15
image: ./some-image.jpg
socialImage: ./some-image.jpg

---
```

これでアイキャッチ画像が表示されるはずです．

お疲れ様でした．

## おわりに

`gatsby-theme-blog` でアイキャッチ画像を表示する方法についての紹介でした．

最後までお読みいただきありがとうございました．

## References

* https://www.npmjs.com/package/gatsby-theme-blog

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

