---

title: gatsby-theme-blog でリンクのスタイルを変更する方法
slug: /tech/gatsby-theme-blog-change-link-style/
date: 2020-07-21
tags: [gatsby, link]
image: ./img/chris-lawton-5IHz5WhosQE-unsplash.jpg
socialImage: ./img/chris-lawton-5IHz5WhosQE-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [`<a>` タグのスタイルをシャドーイングする](#a-タグのスタイルをシャドーイングする)
  - [シャドーイング対象を見つける](#シャドーイング対象を見つける)
  - [具体的なシャドーイング方法](#具体的なシャドーイング方法)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

`gatsby-theme-blog` のリンクスタイルを変更する方法についての紹介です．

`gatsby-theme-blog` のデフォルトリンクスタイルは `text-decoration: underline;` が常に有効になっており，`color` はテーマカラーに従っています．

`hover` しても何もスタイルが変わらないんです．

あくまでも個人的な意見ですが，すごく残念なスタイルだと思います．

そこで，定番の青色で `hover` されたら `text-decoration: underline;` が有効になるリンクスタイルに変更したいと思います．

この記事では，そのリンクスタイルのシャドーイング方法について説明しています．

同じような課題を抱えている人が，この記事を読んで，その課題を解決できたらとても嬉しいです．

それではまいりましょう．

## `<a>` タグのスタイルをシャドーイングする

[シャドーイングの方法](https://typememo.jp/tech/gatsby-theme-blog-font-shadowing/) で紹介した手法を使って，`<a>` タグのスタイルをシャドーイングします．

### シャドーイング対象を見つける

シャドーイングする対象は `${mysite}/node_modules/gatsby-theme-blog/src/gatsby-plugin-theme-ui/index.js` です．

このシャドーイング対象を見つけるのがそれなりに大変です．

シャドーイング対象の見つけ方も [シャドーイングの方法](https://typememo.jp/tech/gatsby-theme-blog-font-shadowing/) で紹介したので，シャドーイング対象のファイルをなかなか見つけられない人は一読していただければと思います．

シャドーイング対象がわかったので，実際にシャドーイングする内容を書くファイルを作成しましょう．

`${mysite}/src/gatsby-plugin-theme-ui/index.js` というファイルを作成してください．

もうすでに `${mysite}/src/gatsby-plugin-theme-ui/index.js` がある人は，同ファイルを編集してください．

### 具体的なシャドーイング方法

さて，実際にどういう風にシャドーイングするか説明します．

コードを見れば一目瞭然だと思います．

`<a>` タグのスタイルをシャドーイングするのに必要な部分だけ抜粋したのが，次に示すコードブロックです．

```javascript
import baseTheme from "gatsby-theme-blog/src/gatsby-plugin-theme-ui/index"
export default {
  styles: {
    a: {
      color: "#195ab5",
      textDecoration: "none",
      "&:hover": {
        textDecoration: "underline",
      }
    },
  },
}
```

`<a>` タグの `color` と `textDecoration` のスタイルを理想のスタイルにシャドーイングしています．

`gatsby` での `hover` の書き方は `&:hover` で記述できることを初めて知りました！

他のアクションも同様の記法で書けるのでしょう．今度試してみたいと思います．

ファイルを保存して，`${mysite}/` 直下で `gatsby develop` を実行して，[http://localhost:8000](http://localhost:8000) をブラウザで開いて，結果を確認してください．

## おわりに

`gatsby-theme-blog` のリンクスタイルをシャドーイングする方法についての紹介でした．

`gatsby` はとても便利なツールなんですが，デフォルトスタイルが少し独特なので好みが分かれるかと思います．

筆者はデフォルトスタイルが好みではないので，この記事や [アイキャッチ画像を表示する方法](https://typememo.jp/tech/gatsby-theme-blog-how-to-enable-social-image/) や [フォントをシャドーイングする方法](https://typememo.jp/tech/gatsby-theme-blog-font-shadowing/) などの記事で紹介してますが，いろいろとカスタマイズしています．

デフォルトスタイルから大きく変わったわけではないので，まだ納得できるモノには仕上がっていません．

`gatsby` とは，これから長い付き合いになると思うので少しずつ少しずつ進んでいきたいと思います．

最後までお読みいただきありがとうございました！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

