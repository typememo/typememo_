---

title: git diff ブランチ名 でブランチ間の差分パッチファイルを作る方法
slug: /tech/git-diff-branch/
date: 2019-10-18
tags: [tech, git]
image: ./img/yancy-min-842ofHC6MaI-unsplash.jpg
socialImage: ./img/yancy-min-842ofHC6MaI-unsplash.jpg
imageAlt: eyecatch

---

## git diff コマンドを実行すれば OK

例えば master ブランチと、ワーキングブランチとの差分を取りたいときは、ワーキングブランチで以下のコマンドを実行すれば良いです．

```shell
git diff master > /path/to/file/PatchFileName.patch
```

すると、`diff --git ...` で始まる diff ファイルが生成されます。
これで差分パッチファイルの作成は簡単ですね。

ちなみに、コミット間の差分パッチファイルを作成したければ、以下のようにコマンドを実行すればよいです。

```shell
git diff コミットId1 コミットId2 > /path/to/file/PatchFileName.patch
```

最後までお読み頂きありがとうございました：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

