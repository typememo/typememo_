---

title: Gitlab のはじめ方
slug: /tech/gitlab-getting-started/
date: 2019-12-02
tags: [tech, git, gitlab]
image: ./img/yancy-min-842ofHC6MaI-unsplash.jpg
socialImage: ./img/yancy-min-842ofHC6MaI-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [gitlab.com にアカウント登録する](#gitlabcom-にアカウント登録する)
- [New project で新規プロジェクトを立ち上げる](#new-project-で新規プロジェクトを立ち上げる)
- [リモートプロジェクトをローカル環境にクローンする](#リモートプロジェクトをローカル環境にクローンする)
- [References](#references)

<!-- /TOC -->

----

## はじめに

本記事では `GitLab` に初めてのリモートプロジェクト (リモートリポジトリ) を作成し，作成したリモートプロジェクトをローカル環境にクローンするまでの手順を紹介します．

ちなみにですが，[既存のディレクトリをリモートプロジェクトに紐つける方法はこちらの記事をご覧ください](https://typememo.jp/tech/gitlab-add-directory-local-to-remote/)．

`GitHub` や `Git` について知らなくても大丈夫です．

必要なのは，**メールアドレス** だけです．

それでは参りましょう．

## gitlab.com にアカウント登録する

まずは，[gitlab.com](https://gitlab.com) にアクセスしてください．

アクセスしたら右上の **Register** をクリックして，アカウントを作成してください．

次の項目を入力して，アカウントを作成してください．

- Full name
- Username
- Email
- Email confirmation
- Password

アカウントの作成が終わったら，続いて新規リモートプロジェクトを作成しましょう．

## New project で新規プロジェクトを立ち上げる

アカウントを作成したら，右上の **New project** をクリックして，新規リモートプロジェクトを作成しましょう．

次の項目を入力して，新規プロジェクトを立ち上げましょう．

- Project name
  - プロジェクト名です．なんでも良いです．
- Project URL
  - プロジェクトの URL です．
  デフォルトの値のままで良いです．
  デフォルトでは `https://gitlab.com/Username` です
- Project slug
  - プロジェクトの URL の `Username/` 以下の部分です．
  デフォルトではプロジェクト名が小文字になっています．
  編集したければご自由にどうぞ．
- Visibility level
  - 世の中に公開したくなければ `Private` を選択してください．
  公開しても良ければ `Public` を選択してください．
- Initialize repository with a README
  - チェックを入れておけばいいです．
  `README` は最初に読んで欲しい文書のコードネームみたいなものです．
  `私を読んで` という意味なので，名前からファイルの意味がわかるかもしれませんが．
  最初 `README` をみた時は "はいっ？ README ってなんですの？" でしたけどね．

全て入力したら，`Create project` をクリックすれば，新規プロジェクトが立ち上がります．

初プロジェクトの作成，おめでとうございます：）

## リモートプロジェクトをローカル環境にクローンする

右上の `Clone` をクリックして，`Clone with HTTPS` をコピーしてください．

ターミナル or コマンドプロンプト を起動してください．

先ほど作成したリモートプロジェクトを保存したいディレクトリまで移動してください．

お目当てのディレクトリまで移動したら，以下のコマンドを実行して，作成したリモートプロジェクトをローカル環境にクローンします．

```bash
# ${Username} と ${ProjectName} はご自身のモノに書き換えてください！
git clone https://gitlab.com/${Username}/${ProjectName}.git
```

すると，以下のようなメッセージが出力されて，Clone 成功です．
初めての，`git clone` お疲れ様でした：）

```bash
Cloning into '${ProjectName}'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
```

## References

GitLab について書籍で知りたい場合は [GitLab実践ガイド](https://amzn.to/37ZSvkY)をご覧ください．

Git について Web で詳細に知りたい場合は [GitPro 公式サイト](https://git-scm.com/book/en/v2) をご覧ください．

最後までお読みいただきありがとうございました：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

