---

title: Slack メッセージ自動フォーマットをオフにする方法
slug: /tech/slack-disable-auto-message-formater/
date: 2019-12-04
tags: [tech, slack]
image: ./img/morning-brew-itSX0YT9TiU-unsplash.jpg
socialImage: ./img/morning-brew-itSX0YT9TiU-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [設定方法](#設定方法)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

2019.11.21 前後で Slack のメッセージ自動フォーマット機能が実装されたようです．
その結果，メッセージを入力している最中に自動でメッセージがフォーマットされてしまいました．
個人的にはすごく使いづらくて困っていました．

しかし今確認してみると、メッセージ自動フォーマット機能を **オン・オフ** できるようになっていました！
感動したので，設定方法を記事にしました．

## 設定方法

設定方法は以下の通りです。

- Slack  
- => Preferences  
- => Advanced  
- => Format messages with markup にチェックを入れる  
(下図参照)  

![FigureSlackAutoFormatOff](./img/Screenshot_2019-12-04_12.14.11.png)

これだけで、メッセージ自動フォーマット機能がオフになります。
煩わしい自動フォーマット機能がなくなり、Slack のメッセージ入力が楽になりました。  

`Slack developer` のみなさん、ありがとうございます：）  

## おわりに

Slack のメッセージ自動フォーマット機能を無効化する設定方法についての紹介でした．

自動フォーマット機能を無効化してしまうことで，多少ですが使いづらいなと感じることがあります．

- メッセージを送った後に箇条書きがフォーマットされない
- ハイパーリンク記法 `[string](https://example.com)` がフォーマットされない

などです．自動フォーマット機能を無効化しても，メッセージ送信後には Markdown 記法に完璧にフォーマットできるとすごく良いなぁと思います．

`Slack developer` のどなたかにこの思いが届くと良いのですが．slack の github ページとかあれば，issue に投稿するところなのですが，ないんですよね，slack の github ページ．残念です．

最後までお読みいただきありがとうございました．

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

