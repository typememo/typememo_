---

title: Gmail API Scopes 日本語訳
slug: /tech/gmail-api-scopes
date: 2020-07-29
tags: [tech, gmail]
image: ./img/kon-karampelas-N82naZ0N4TY-unsplash.jpg
socialImage: ./img/kon-karampelas-N82naZ0N4TY-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

[Gmail scopes](https://developers.google.com/gmail/api/auth/scopes#gmail_scopes) によれば、Gmail API に与える権限は以下の通りです。

| Scope Code | Description | Handling Risk |
| :-- | :-- | :-- |
| https://www.googleapis.com/auth/gmail.labels | ラベルの作成、読み込み、更新、削除のみを許可する | ! |
| https://www.googleapis.com/auth/gmail.send | メッセージの送信のみを許可する | !! |
| https://www.googleapis.com/auth/gmail.readonly | 全リソースとそのメタデータの読み込みを許可する | !!! |
| https://www.googleapis.com/auth/gmail.compose | 下書きの作成、読み込み、更新、削除とメッセージと下書きの送信を許可する | !!! |
| https://www.googleapis.com/auth/gmail.insert | メッセージの挿入とインポートのみを許可する | !!! |
| https://www.googleapis.com/auth/gmail.modify | 全ての読み込みと書き込みを許可する。しかしメッセージとスレッドの即時及び永久的な削除を除く | !!! |
| https://www.googleapis.com/auth/gmail.metadata | ラベル、履歴、メッセージのヘッダーの読み込みを許可する。しかしメッセージの本文と添付の読み込みを除く | !!! |
| https://www.googleapis.com/auth/gmail.settings.basic | 基本設定の管理を許可する | !!! |
| https://www.googleapis.com/auth/gmail.settings.sharing | 転送ルールやエイリアスなどの取り扱いに注意が必要な設定の管理を許可する | !!! |
| https://mail.google.com/ | 全てを許可する | !!! |

例えば、とあるメッセージを受信して、そのメッセージを転送して、未読のラベルを削除したい、そんな時は以下のように `SCOPES` を配列として定義します。

```python
SCOPES = [
  "https://www.googleapis.com/auth/gmail.send",
  "https://www.googleapis.com/auth/gmail.modify"
]
```

## おわりに

Gmail API Scopes についての日本語訳でした。

最後までお読みいただき、ありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

