---

title: VSCode markdown アウトライン表示方法
slug: /tech/vscode-markdown-navigation
date: 2020-08-01
tags: [tech, vscode, markdown]
socialImage: ./img/iconfinder_markdown_298823.png
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Markdown Navigation をインストールする](#markdown-navigation-をインストールする)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

Visual Studio Code のマークダウンファイルにアウトラインを表示する方法の紹介です。

## Markdown Navigation をインストールする

Visual Studio Code の Extension を開いて **markdown navigation** と入力してください。

すると、次の画像に示すような [Markdown Navigation](https://marketplace.visualstudio.com/items?itemName=AlanWalk.markdown-navigation) という拡張機能が見つかるかと思います。

![figure-visual-studio-code-extension-markdown-navigation](./img/figure-vscode-extension-markdown-navigation.png)
Figure1. Visual studio code extension of markdown navigation.

**Install** をクリックして、拡張機能をインストールします。

以上で拡張機能のインストールは終了です。

適当なマークダウンファイルを開いて **Explorer** をクリックしてください。

すると、サイドバーに **Markdown Navigation** が表示されていると思います。

以上で、マークダウンファイルにアウトラインを表示する作業は終了です。

## おわりに

マークダウンファイルにアウトラインを表示する方法の紹介でした。

見出し番号などは表示されないんですけど、文書の構成をすぐに確認できるのが良いところだと思います。

最後までお読みいただき、ありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

