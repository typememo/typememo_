---

title: Gatsby JS ブログの投稿記事テンプレートジェネレータを作ってみた
slug: /tech/gatsby-theme-blog-markdown-template-generator/
date: 2020-09-16
tags: [tech, gatsby, markdown]
image: ./img/tim-arterbury-VkwRmha1_tI-unsplash.jpg
socialImage: ./img/tim-arterbury-VkwRmha1_tI-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [はじめに](#はじめに)
- [typecode の実装内容](#typecode-の実装内容)
- [typecode の実行方法](#typecode-の実行方法)
  - [実行権限を付与する](#実行権限を付与する)
  - [direnv を導入する](#direnv-を導入する)
    - [direnv について御託を並べてみる](#direnv-について御託を並べてみる)
    - [direnv を設定する](#direnv-を設定する)
- [おわりに](#おわりに)
  - [更新履歴](#更新履歴)

---


## はじめに

Gatsby JS ブログ記事用のテンプレートジェネレータを実装してみました．

Gatsby JS (gatsby-theme-blog) は markdown をいい感じに html に変換してくれてほんとありがたいです．
でも markdown のフロントマターに必要な情報を書かないとエラーになっちゃうんですよね．
しかもその量がちょっと多いんですよね．
`key: value` 記法なので全ての記事で `key` は同じなのにいちいちフロントマターを書くの面倒くさいなぁと感じていました．

なのでテンプレートジェネレータを実装しました．
その名は `typecode` です．
筆者は `typememo` を名乗っているので，それ繋がりで適当な名前を付けました．

以下では

- `typecode` の実装内容
- `typecode` の実行方法

同じような煩わしさを抱えている人の参考になれば幸いです！
あくまでも筆者の開発環境の話なので，その点はご了承ください．

それでは参りましょう．


## typecode の実装内容

筆者 Gitlab の [typecode](https://gitlab.com/typememo/typememo_/-/blob/master/tools/typecode) に書いてある通りです．

主要なコードだけ抜粋すると次の通りです．

```shell
# Define
PATH_TO_TOOLS=$(cd $(dirname $0); pwd)
TEMPLATE="$PATH_TO_TOOLS/res/template.md"
MANUSCRIPT=$1

# Create new manuscript
cat $TEMPLATE > $MANUSCRIPT

# Open new manuscript with vscode
code $MANUSCRIPT
```

`typecode` は `tools/` に保存されているので
`PATH_TO_TOOLS` に `tools/` のフルパスを代入しています．

`typecode` はテンプレートファイルとして
[manuscript.md](https://gitlab.com/typememo/typememo_/-/blob/master/tools/res/template/manuscript.md)
を使っているので，`TEMPLATE` に `manuscript.md` へのフルパスを代入しています．

`typecode` に渡される引数はたった一つで，それはブログ記事の原稿となる markdown へのパスです．
それを `MANUSCRIPT` に代入しています．

`TEMPLATE` と `MANUSCRIPT` が定義されてしまえば後は簡単で
`TEMPLATE` を `MANUSCRIPT` に `cat` コマンドで書き出してやるだけです．

筆者は `vscode` を使って開発しているので `MANUSCRIPT` を
`code` コマンドで開いてやれば執筆作業を開始できます．
`vi` や `emacs` を使って開発している方は
`code` をそれらのコマンドに書き換えてください．

以上が `typecode` の実装内容の紹介でした．

## typecode の実行方法

それでは `typecode` の実行方法について説明します．

`typecode` を実行するための文法は次の通りです．

```shell
$ typecode /path/to/manuscript.md
```

これを実行すると `/path/to/manuscript.md` がテンプレートが反映された状態で新規作成されます．
もし `/path/to/manuscript.md` に同じ名前のファイルが存在していた場合は，新規作成せずに既にあるファイルを `vscode` で開きます．

`typecode` は `.sh` 拡張子が末尾についていないただのシェルスクリプトです．

シェルスクリプトを実行するには実行権限を付与してあげる必要があります．

加えて，`PATH` に `typecode` が保存されているディレクトリを追加してあげる必要があります．
まぁ `PATH` の設定をしなくても `./path/to/typecode` で実行できるのですが，見栄えも悪いし使い勝手も悪いです．
`PATH` への追加は [direnv](https://github.com/direnv/direnv) を使って実現しました．


### 実行権限を付与する

まずはじめに `typecode` に実行権限を付与します．

ファイルやディレクトリの権限を変更するには `chmod` コマンドを使えば良いです．

```shell
$ chmod 755 typecode
```

これで `USER` に実行権限が付与されました．


### direnv を導入する

続いて `direnv` を導入して環境変数をワーキングディレクトリ毎に管理していきます．


#### direnv について御託を並べてみる

この項では `direnv` について御託を並べてみました．
"`direnv` のことは知ってるよ" という方はこの項をスキップしていただければと思います．

`direnv` は `pyenv` や `nodenv` と同じようにワーキングディレクトリ毎に環境を自由自在に管理できるツールです．

例えば `parent/child/` というディレクトリ構成で，`pyenv` を使って各ディレクトリ毎に異なるバージョンの `python` を使いたいとします．
具体的には `parent/` では `python 2.7` を使いたいのだけど，その子ディレクトリである `child/` では `python 3.6` を使いたいという場合などです．
バージョン管理ツールを使わずにこの環境を構築しようとするとまあまあ大変ですが，`pyenv` を使えば4–5回 `pyenv` コマンドを叩けばこの環境を完璧に構築できます．
という感じで半端ないくらい便利な管理ツールなわけです．

でもって `direnv` ですが，これは環境変数などを各ディレクトリ毎に管理できるツールです．
`direnv` は `.envrc` というファイルが保存されているディレクトリ配下に `cd` した瞬間に `.envrc` に記述されている処理を実行します．
`PATH` 環境変数の追加をしたい場合は `PATH_add ./path/to` を `.envrc` に記述すれば OK です．

`direnv` は [asdf](https://github.com/asdf-vm/asdf) でバージョン管理できます．
筆者は `asdf` を使って `direnv` をバージョン管理していますが，`asdf` を使わずに `brew install direnv` コマンドを実行すれば最新版の `direnv` をインストールすることができます．
`asdf` を使えば他の言語なども一元的にバージョン管理できます．
このブログの [#asdf](https://typememo.jp/tags/asdf/) を覗いていただければ `asdf` 関連の記事を読むことができます．
よければ覗いていってくださいませ．

だらだらと御託を並べてみました．
一瞬でも `へぇ` と思える瞬間はあったでしょうか？
さて，本題です．


#### direnv を設定する

`direnv` を使えるようにするには次の３ステップを踏む必要があります．

- `.envrc` を記述する
- `asdf` で `direnv` をインストールする
- `.zshrc` に `eval "$(direnv hook zsh)"` を記述する

まずはじめに `.envrc` を書きましょう．
筆者の Gitlab リポジトリ [typememo_](https://gitlab.com/typememo/typememo_) ではトップディレクトリに `.envrc` を配置しています．
例えばこの記事の環境だと `typecode` は `tools/` に保存されているので `tools/` への `PATH` を追加しました．

```
PATH_add tools/
```

続いて `asdf` で `direnv` をインストールしてアクティベートします．

```shell
$ asdf plugin-add direnv
$ asdf install direnv latest
$ asdf global direnv 2.22.0
```

`direnv` の latest が 2.22.0 なのはこの記事執筆時点の 2020/09/17 での話です．
この記事を読んでいる時点の latest は 2.22.0 ではない可能性があります．
適宜正しいバージョンに変更してください．

最後に `.zshrc` の末尾に `direnv` をフックする文を追加しましょう．

```shell
$ echo 'eval "$(direnv hook zsh)"' >> $HOME/.zshrc
$ source $HOME/.zshrc
```

これで `.envrc` があるディレクトリ配下に `cd` した瞬間に `PATH` に `tools/` へのパスが追加されます．
この記事の場合だと `typememo_` ディレクトリ配下に `cd` した瞬間に `PATH` に `tools/` が追加されて `typecode` コマンドが実行できるようになります．


## おわりに

Gatsby JS ブログ記事用テンプレートジェネレータの紹介でした．
言うても筆者のブログ記事用なのでそっくりそのままの形で皆さまが使うことはないと思います．
あくまで参考にしていただければと思います．

筆者は GatsbyJS 初心者でして，公式サイトとにらめっこしながら日々修行中です．
先日本屋で [GatsbyJSで実現する、高速&実用的なサイト構築](https://amzn.to/33zIhX8) という本を見かけました．
パラパラと読んだところ GatsbyJS の API やらが理解できそうで良さげな書籍だなと感じました．
今度本屋行ったら買おうと思っています．
買って読んだらレビュー記事書きます．

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 更新履歴

- 2020-10-23 manuscript.md への Gitlab リンク更新

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

