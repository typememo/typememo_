---

title: idl polygon で図に塗りつぶしを入れる方法
slug: /tech/idl-polygon/
date: 2018-11-03
tags: [tech, idl]
image: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
socialImage: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [polygon 関数とは](#polygon-関数とは)
- [polygon関数の文法](#polygon関数の文法)
- [polygon関数の具体的な使い方](#polygon関数の具体的な使い方)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

今回は、「IDLで図に塗りつぶしを入れる方法」についてです。

使う関数は「polygon関数」です。

それでは、早速参りましょう。

## polygon 関数とは

デジタル大辞林によれば、polygonとは

> 多角形。三次元のコンピュータグラフィックスにおける立体形状を表現するために使われる多角形を指すことが多い。物体表面を小さい多角形（主に三角形）に分割し、その位置や角度、模様、質感などの見え方を個々に計算して３次元画像を描画する。

だそうです。要は，polygonとは多角形ということですね。

つまり、polygon関数とは、IDLで作成した図に対して多角形の注釈をつけることのできる関数です。

図のある領域を塗りつぶしたい時や、誤差をエラーバーで表示するのではなく帯のように表示する時などに活躍するかと思います。

## polygon関数の文法

polygon関数の、最低限必要な文法は以下のようになっています。

```python
graphic = polygon(x, y)
```

`graphic` は 変数名なので適当に決めてください。

`x` は多角形のx座標です。

同様に、`y` は多角形のy座標です。

表1に、筆者が特に使うキーワードとプロパティとその意味についてまとめておきました。

表1. polygon関数でよく使うKeywordsとProperties

| Keywords & Properties | Description | Syntax |
| :-- | :-- | :-- | :-- |
| data | 図のx座標とy座標に対してpolygonを描画する | data = 1, /data |
| target | polygonの描画先を指定する | target = graphic |
| color | polygonの線の色を指定する | color = 'red', etc... |
| fill_color | polygonの内側の塗りつぶす色を指定する | fill_color = 'blue' |
| linestyle | 線のスタイルを指定する。[^linestyle] | linestyle = 0, 1, 2, 3, 4, 5, 6 |
| transparency | 塗りつぶす色の透明度を指定する。0が透明度0を表し、100が透明度100を表す。 | transparency = 80 |

[^linestyle]: Linestyle {
  0: solid line, 
  1: dotted line, 
  2: dashed line, 
  3: dash dot line, 
  4: dash dot dot dot line, 
  5: long dash line, 
  6: no line
}

## polygon関数の具体的な使い方

実際にIDLを動かしながらpolygon関数を使ってみましょう。

今回は適当なsin波に対してランダム数を上下に乗っけて、その範囲を灰色で塗りつぶしてみましょう。

以下がソースコードです。

```python
;; データ数は100個
n = 100l

;; 0から99までの1刻みのデータを生成する。
x = dindgen(n, start = 0, increment = 1)

;; 適当に波数を決定する。
;; 今回は波長20の波を生成する
k = 2d*!pi/20d

;; sin波を生成する
data = sin(k*x)

;; 適当にプラスの誤差とマイナスのランダム数を生成して帯を作る。
errPlus = data + randomu(seed, n)
errMinus = data - randomu(seed, n)

;; sin波を出力する
graph_plot = plot(x, data, yrange = [-2, +2], color = 'black', linestyle = 0)

;; errPlusとerrMinusの幅を灰色で塗りつぶし表示する
graph_polygon = polygon($
  [x, reverse(x)], [errPlus, reverse(errMinus)],$
  target = graph_plot, /data,$
  fill_color = 'gray', /fill_background, transparency = 75,$
  linestyle = 0, thick = 2)
```

上記ソースコードの17行目までは値を定義して、sin波を生成して、ランダム誤差を出力しています。

20行目の `graph_plot` をすると以下のような図が出力されると思います。

y座標の範囲を `-2 ~ +2` にしているだけで、残りの部分はデフォルトの設定と変わりません。

![figure1](./img/sc-2018-11-13-16.22.43.png)

23行目の `graph_polygon` を実行すると以下のような図が出力されると思います。

ただし、環境の違いによってランダム誤差の値が異なると思いますので、そっくり同じ結果は得られないと思います。

![figure2](./img/sc-2018-11-13-16.22.56.png)

## おわりに

このように、polygon関数を使えば、グラフの特定の領域を塗りつぶすことができます。

また、polygon関数の詳細は [IDL Help](https://www.harrisgeospatial.com/docs/POLYGON.html) をご覧ください。

以上になります。最後までお読みいただきありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

