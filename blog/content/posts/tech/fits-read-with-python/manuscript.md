---

title: Python fits read – Python で fits ファイルを読み込む方法
slug: /tech/fits-read-with-python
date: 2020-07-30
tags: [tech, fits, python]
image: ./img/iman-gozal-EPxVDc_xeWA-unsplash.jpg
socialImage: ./img/iman-gozal-EPxVDc_xeWA-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [astropy のインストール](#astropy-のインストール)
- [サンプルコード全文](#サンプルコード全文)
- [astropy から fits モジュールをインポートする](#astropy-から-fits-モジュールをインポートする)
- [適当な fits ファイルをダウンロードする](#適当な-fits-ファイルをダウンロードする)
- [fits ファイルを読み込む](#fits-ファイルを読み込む)
- [matplotlib で描画する](#matplotlib-で描画する)
- [おわりに](#おわりに)
- [References](#references)

<!-- /TOC -->

----

## はじめに

Python astropy を使った fits ファイルの読み込みについての紹介です。

## astropy のインストール

astropy をインストールには、`conda` もしくは `pip` でインストールします。

```bash
▶ pip install astropy
# or
▶ conda install astropy
```

## サンプルコード全文

本記事で実装したサンプルコードです。

```python
import os
import matplotlib.pyplot as plt
from astropy.io import fits

# --------------------------------------------------
# Define
# --------------------------------------------------
r_dir = "$HOME/Downloads/"
r_file = "lir_20151207_052704_pic_l2c_v10.fit"
ext_number = 1 # extname: LIR-LEVEL2c
color_table = "gist_heat"
min_value = 210
max_value = 235
tick_interval = 5

# --------------------------------------------------
# Main
# --------------------------------------------------
def main():
    # --------------------------------------------------
    # Search file
    # --------------------------------------------------
    f_fits = os.path.expandvars(r_dir + r_file)

    # --------------------------------------------------
    # Read fits
    # --------------------------------------------------
    fits.info(f_fits)
    img_fits = fits.getdata(f_fits, ext=ext_number)
    plt.figure()
    plt.imshow(img_fits, 
            cmap=color_table, 
            vmin=min_value, 
            vmax=max_value)
    plt.colorbar(ticks=range(min_value, 
                            max_value + 1, 
                            tick_interval))
    plt.show()

# --------------------------------------------------
# Run
# --------------------------------------------------
if __name__ == '__main__':
    main()
```

## astropy から fits モジュールをインポートする

```python
from astropy.io import fits
```

fits ファイルを処理できるモジュールは `astropy.io.fits` です。
python でモジュールを使うためには `import` する必要があります。

## 適当な fits ファイルをダウンロードする

本記事では [金星探査機あかつき](http://darts.isas.jaxa.jp/planet/project/akatsuki/index.html.ja) に搭載されている中間赤外カメラ LIR が撮像した画像をサンプルとして使います。
[lir_20151207_052704_pic_l2c_v10.fit](http://darts.isas.jaxa.jp/pub/pds3/vco-v-lir-3-cdr-v1.0/vcolir_1001/data/l2c/r0001/lir_20151207_052704_pic_l2c_v10.fit) をダウンロードしてください。
ダウンロードしたら適当なディレクトリに保存してください。

本記事では、`$HOME/Downloads/` に保存されていると仮定します。

## fits ファイルを読み込む

```python
import os
from astropy.io import fits

r_dir = "$HOME/Downloads/"
r_file = "lir_20151207_052704_pic_l2c_v10.fit"

def main():
    ...
    f_fits = os.path.expandvars(r_dir + r_file)
    img_fits = fits.getdata(f_fits, ext=ext_number)
```

fits ファイルを読み込むには `fits.getdata()` メソッドを使います。
詳細な使い方については、[astropy 公式ドキュメント](http://docs.astropy.org/en/stable/io/fits/api/files.html#getdata) をご覧ください。

## matplotlib で描画する

```python
    plt.figure()
    plt.imshow(img_fits, 
            cmap=color_table, 
            vmin=min_value, 
            vmax=max_value)
    plt.colorbar(ticks=range(min_value, 
                            max_value + 1, 
                            tick_interval))
    plt.show()
```

読み込んだ fits データを描画するために、`matplotlib` ライブラリを使います。

プログラム実行後、次の画像が出力されていれば OK です。

![figure-sample-fits-image](./img/figure-venus-observed-by-lir.png)

## おわりに

python で fits ファイルを読み込む方法についての紹介でした。

参考文献は、サンプルコードの中に埋め込んであります。
`astropy` や `matplotlib` ライブラリの詳しい使い方は、公式ドキュメントをお読みください。

IDL (Interactive Data Language) という解析ツールを使って、研究している方は、この際 python に乗り換えてみてはいかがでしょうか？
仕事の幅が格段に広がると思いますので、ぜひ検討してみてください。

最後まで、お読みいただきありがとうございました。

## References

- http://docs.astropy.org/en/stable/io/fits/api/files.html#astropy.io.fits.info
- http://docs.astropy.org/en/stable/io/fits/api/files.html#getdata
- https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.imshow.html
- https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.colorbar.html
- https://matplotlib.org/tutorials/colors/colormaps.html

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

