---

title: MacOS default PATH / MacOS デフォルトパス
slug: /tech/macos-default-path/
date: 2019-05-06
tags: [tech, mac, macos]
image: ./img/lauren-mancke-aOC7TSLb1o8-unsplash.jpg
socialImage: ./img/lauren-mancke-aOC7TSLb1o8-unsplash.jpg
imageAlt: eyecatch

---

## デフォルトの PATH　一覧

Macのデフォルトパスは以下のとおりです。

- `/usr/local/bin`
- `/usr/bin`
- `/bin`
- `/usr/sbin`
- `/sbin`

`$HOME/.bashrc` か `$HOME/.bash_profile` の最初の方に `PATH` を宣言しておけば、`PATH`がめちゃくちゃにならないと思います。  

```shell
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
```

最後までお読みいただき、ありがとうございました。  

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

