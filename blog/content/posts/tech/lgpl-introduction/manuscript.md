---

title: LGPL って何？
slug: /tech/lgpl-introduction
date: 2020-07-25
tags: [tech, license, lgpl]
image: ./img/lewis-keegan-skillscouter-com-XQaqV5qYcXg-unsplash.jpg
socialImage: ./img/lewis-keegan-skillscouter-com-XQaqV5qYcXg-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [LGPL とは GNU Lesser General Public License の略称である](#lgpl-とは-gnu-lesser-general-public-license-の略称である)
- [References](#references)

<!-- /TOC -->

----

## はじめに

お仕事で LGPL っていう言葉に遭遇しました．

初めて聞いた言葉で，よくわからなかったので少し勉強しました．

本記事は，その時の勉強メモです．

## LGPL とは GNU Lesser General Public License の略称である

> LGPL とは GPL をベースとしているが，LGPL のもとで公開されたソフトウェアを利用したソフトウェアを開発しても，その独自開発部分のソースコードの公開を強制しないという特徴を持っている．
> LGPL ではライセンスの継承を必要とせず，それを利用した作成されたソフトウェアのソースコードの公開は強制しない．
> しかし，LGPL として公開されているソースコードを改変して利用し作成されたソフトウェアのソースコードは強制的に公開させなければならない．

なるほど．

LGPL として公開されているソースコードを利用してソフトウェアを作成するだけなら自分が作ったソフトウェアのソースコードを公開する必要はない．

でも，LGPL として公開されているソースコードを改変して利用した場合は，その改変コードを利用して作成したソフトウェアのソースコードを公開する必要がある．

つまり，LGPL として公開されているソースコードは **利用するだけ** なら公開義務は生じない．

**改変して利用した** なら公開義務が生じる．

LGPL として公開されているソースコードは **改変せずに利用するだけに留めること** が重要ポイントですな．

## References

[https://www.weblio.jp/content/GNU+LGPL](https://www.weblio.jp/content/GNU+LGPL)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

