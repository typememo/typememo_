---

title: MacOS の Finder でファイル名を一括置換するお手軽な方法
slug: /tech/macos-finder-rename/
date: 2019-01-22
tags: [tech, mac, macos]
image: ./img/lauren-mancke-aOC7TSLb1o8-unsplash.jpg
socialImage: ./img/lauren-mancke-aOC7TSLb1o8-unsplash.jpg
imageAlt: eyecatch

---

今回は、MacのFinderでファイル名を一括置換するお手軽な方法についてです。

## 方法

ファイル名一括置換の手順は次の通りです．

- Finderを起動
- 名前を変更したいファイルを全選択
- `??項目の名前を変更...` をクリック
- `検索文字列` と `置換文字列` に文字列を入力
- `名前を変更` をクリック

以上です．

## References

- https://pc-karuma.net/mac-rename-files/

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

