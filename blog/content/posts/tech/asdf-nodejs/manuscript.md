---

title: asdf node.js のバージョン管理方法
slug: /tech/asdf-nodejs
date: 2020-07-30
tags: [tech, asdf, nodejs]
image: ./img/cookie-the-pom-gySMaocSdqs-unsplash.jpg
socialImage: ./img/cookie-the-pom-gySMaocSdqs-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [作業環境](#作業環境)
- [node.js のインストール](#nodejs-のインストール)
- [node.js のバージョン確認](#nodejs-のバージョン確認)
- [npm (node.js package manager) のバージョン確認](#npm-nodejs-package-manager-のバージョン確認)
- [node.js を他のバージョンに切り替える場合](#nodejs-を他のバージョンに切り替える場合)
- [おわりに](#おわりに)
- [References](#references)

<!-- /TOC -->

----

## はじめに

本記事は `asdf` を使った `node.js` のバージョン管理方法についての紹介です。

まだ `asdf` をインストールしていない方は、[asdf のインストール方法](https://typememo.com/2020/02/asdf-install-and-version-manage/) をお読みください。

## 作業環境

筆者の作業環境は以下の通りです。

| システム | バージョン |
| :-- | :-- |
| OS | MacOS 10.15 |
| Shell | zsh |

## node.js のインストール

以下のコマンドを実行すれば `node.js` v13.10.1 がインストールできます。
他のバージョンをインストールしたければ、他のバージョンに書き換えてください。

```bash
brew install coreutils
brew install gpg
asdf plugin-add nodejs
asdf install nodejs 13.10.1
asdf global nodejs 13.10.1
exit
```

## node.js のバージョン確認

ターミナルを起動し `node -v` を実行することで `nodejs` のバージョンを確認してみてください。
先ほど `asdf global` した `nodejs` のバージョンに切り替わっていれば OK です。

```bash
node -v
# LOG
v13.10.1
```

## npm (node.js package manager) のバージョン確認

ついでに `npm (node.js package manager)` のバージョンも確認してみましょう。

```bash
npm -v
# LOG
6.13.7
```

## node.js を他のバージョンに切り替える場合

`node.js` を他のバージョンに切り替えたい場合は以下のコマンドを実行してください。

```bash
asdf install nodejs <VERSION>
asdf global nodejs <VERSION>
```

例えば `node.js` の v13.10.0 をインストールしたい場合には、
```bash
asdf install nodejs 13.10.0
asdf global nodejs 13.10.0
exit
```
のように実行してください。

## おわりに

`asdf` で `nodejs` をバージョン管理する方法についての紹介でした。

[asdf で python をバージョン管理する方法](https://typememo.com/2020/02/asdf-install-and-version-manage/)も合わせてお読みいただければと思います。

最後までお読みいただき、ありがとうございました ：）

## References

[asdf-vm / asdf-nodejs](https://github.com/asdf-vm/asdf-nodejs)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

