---

title: VSCode Markdown TOC でエラーが発生した時の対処方法
slug: /tech/vsocde-markdown-toc-error-handling/
date: 2020-09-05
tags: [tech, vscode]
image: ./img/max-duzij-qAjJk-un3BI-unsplash.jpg
socialImage: ./img/max-duzij-qAjJk-un3BI-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [はじめに](#はじめに)
- [目次を追加・更新できないんですけど](#目次を追加更新できないんですけど)
- [目次が更新されるたびにエディターが一行スクロールするんですけど](#目次が更新されるたびにエディターが一行スクロールするんですけど)
- [そんなに不具合が多いならいっそのこと Markdown TOC をやめてみては?](#そんなに不具合が多いならいっそのこと-markdown-toc-をやめてみては)
- [おわりに](#おわりに)
- [References](#references)

---


## はじめに

[Markdown TOC](https://marketplace.visualstudio.com/items?itemName=AlanWalk.markdown-toc) 便利ですよね．
箇条書き表示やナンバリング表示で目次が挿入できて，しかもサイト内リンクも自動で貼ってくれる．
その利便性に魅了されて筆者も3–4年ほどお世話になりました．

しかし，この度 `Markdown TOC` の利用を終了することにしました．
きっかけは使っている最中の不具合が多かったからです．
決め手は保存される度に一行スクロールする不具合の発生です．
さらに，1年以上もの間コミットされておらずメンテナンスがされていないことがわかったのも鞍替えの理由の一つです．
1年以上もの間コミットがされていないということは，おそらく今後もメンテナンスはされないんだろうなと判断しました．

今後は [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) の `Create Table of Contents` 機能を使っていきます．
現状，気に入らないところはなく満足して使っています．
最新コミットも 2020/08/31 に入っており開発状況も活発なようです．
(記事執筆日時: 2020/09/04)

ここから先は，`Markdown TOC` を使っていた筆者が遭遇した不具合について紹介すると同時に，`Markdown All in One` の良いところも紹介します．


## 目次を追加・更新できないんですけど

目次に表示する見出しに特殊文字が含まれていると `Markdown TOC` は目次の追加と更新ができません．

具体的には `%` などの特殊文字が含まれているとダメなようです．

この不具合は [zsh date コマンドの使い方](https://typememo.jp/tech/zsh-date-usage/) という記事を書いているときに遭遇しました．

`vscode markdown toc 更新できない` などのキーワードで google 検索すると [Markdown TOC issue#35](https://github.com/AlanWalk/markdown-toc/issues/35) がヒットしました．

この issue#35 に

> 見出しから `%` を削除すると上手くいきますよ

と記載がありました．

この記載通りに見出しから `%` などの特殊文字を削除すると目次が期待通り更新されるようになりました．

ちなみに `Markdown All in One` の目次機能は `%` が見出しに含まれていても期待動作します！
すごいぞ `Markdown All in One`！


## 目次が更新されるたびにエディターが一行スクロールするんですけど

ファイルを保存するたびに目次が更新されるように設定していたのですが，2020/09 前後で **目次が更新される度に一行スクロールされてしまうバグ** が発生するようになってしまいました．

[Markdown TOC issue#77](https://github.com/AlanWalk/markdown-toc/issues/77) にバグチケットとして登録されていましたが，メンテナンスされていないので開発者からの回答はおそらく期待できないでしょう．

issue#77 でも指摘されている通りですが，原因は **目次が更新される度に目次上部に表示されている `Table of Contents (up to date)` という文言が一度消えてから再度表示されること** です．
手軽な解決方法が見当たらなかったので，致し方なく暫定的な対処として目次の自動更新を止めることにしました．
目次の自動更新機能をオフにすると，やはり目次の更新を忘れてしまい面倒だなと感じました．

この目次が更新される度にエディターが一行スクロールする不具合が，`Markdown TOC` から `Markdown All in One` への鞍替えの決め手になりました．
issue#77 でも `Markdown TOC` と `Markdown All in One` との副作用であると指摘されているのでぶっちゃけて言えばどちらか一方だけ残せば良いのです．
どちらか一方を選べて問われれば `Markdown All in One` 一択であります．

`Markdown All in One` はこの奇異なバグは発生していません．
非常に使い勝手が良く満足しております！


## そんなに不具合が多いならいっそのこと Markdown TOC をやめてみては?

[はじめに](#はじめに) でも書きましたが，筆者は不具合多発の `Markdown TOC` を使い続けるのではなく，より不具合の少ない `Markdown All in One` に鞍替えしました．

多くの人が使うであろう人気の機能は高確率で代替できるモノがあります．
ご自身の好みに合わせて好きなモノを使うのが精神衛生的に良いと思います．
ツールに翻弄されるなんて本末転倒ですので．


## おわりに

`Markdown TOC` の不具合対処方法の紹介ではなく，`Markdown All in One` への鞍替えの斡旋になってしまいましたw

とにかく `Markdown All in One` は素晴らしい拡張機能だから使っていない人は使ってみたら良いと思います．
すでに使っている人は `Markdown All in One` の説明文を読んで，どんな機能があるのか知って使い倒してみると良いと思います．

最小限の武器で様々な戦い方ができる人ってかっこいいと個人的に思います．

最後までお読みいただきありがとうございました！

もし良ければ [OFUSE](https://ofuse.me/typememo) していただけると嬉しいです．
OFUSE 返し致しますので，どうぞよろしくお願いします！


## References

* https://marketplace.visualstudio.com/items?itemName=AlanWalk.markdown-toc
* https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

