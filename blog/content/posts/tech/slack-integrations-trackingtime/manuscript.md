---

title: Slack with TrackingTime で気軽に工数管理する方法
slug: /tech/slack-integrations-trackingtime/
date: 2019-10-24
tags: [tech, slack]
image: ./img/morning-brew-itSX0YT9TiU-unsplash.jpg
socialImage: ./img/morning-brew-itSX0YT9TiU-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [TrackingTime とは](#trackingtime-とは)
- [TrackingTime アプリを Slack にインストールする方法](#trackingtime-アプリを-slack-にインストールする方法)
- [TrackingTime アプリの具体的な使い方](#trackingtime-アプリの具体的な使い方)
  - [`/tt start TaskName` で計測開始](#tt-start-taskname-で計測開始)
  - [`/tt stop` で計測終了](#tt-stop-で計測終了)
  - [`/tt tasks` でタスク一覧を表示する](#tt-tasks-でタスク一覧を表示する)
- [TrackingTime.co で工数を確認する](#trackingtimeco-で工数を確認する)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

Slack に TrackingTime アプリをインストールして、簡単に工数管理する方法の紹介です。

この記事の対象者は、普段から Slack を使用していて、Slack から離れることなく作業時間の記録を取りたい人です。

以下では、Slack に TrackingTime アプリをインストールする方法から、TrackingTime アプリの具体的な使い方について紹介します。

興味のある方は読み進めていただければと思います。

## TrackingTime とは

TrackingTime とはチームの工数管理が簡単にできる Web アプリケーションです。

## TrackingTime アプリを Slack にインストールする方法

- Slack を起動して、`アプリを追加する` をクリックしてください。
- アプリ一覧の検索ボックスに `TrackingTime` と入力してください。
- `インストール` をクリックしてください。
- `Slack に追加` をクリックしてください。
- `INSTALL` をクリックしてください。
- Tracking Time のアカウントを持っている方は、`YES!` をクリックしてログインしてください。
  アカウントを持っていない方は `NOPE...` をクリックしてアカウントを作成してください。
- `CONNECT` をクリックしてください。
  **Timezone** と **Workspace** は各自で選択してください。
- `GO TO SLACK!` をクリックすると Slack に戻ります。

次に TrackingTime の Slack コマンドの使い方について説明します。

## TrackingTime アプリの具体的な使い方

TrackingTime の Slack コマンドは `/tt` で始まります。

基本的には以下の３つのコマンドを

- 計測開始
- 計測終了
- タスク一覧表示

の時に使います。

```slack
/tt start TaskName
/tt stop
/tt tasks
```
### `/tt start TaskName` で計測開始

`/tt start TaskName` で TaskName というタスク名の時間計測を開始します。
TaskName には英数字はもちろんのこと日本語も使えます。

### `/tt stop` で計測終了

`/tt stop` で現在計測しているタスクの時間計測を終了します。

### `/tt tasks` でタスク一覧を表示する

`/tt tasks` でタスク一覧を表示できます。
このコマンドは基本的に使うことはないと思います。使う時としては、タスクの計測をクローズしたい時です。
`/tt tasks` を実行すると、タスク一覧が表示されて、**Close** | **Start** がクリックできます。
一度、`/tt stop` でストップしたタスクの時間計測は、実は一時停止状態になっているだけなのです。
もしも同じタスク名を `/tt start TaskName` で再度計測開始すると、計測時間が積算されます。
計測時間が積算されるのがいやであれば、`/tt stop` した後に、`/tt tasks` でタスク一覧を表示して、
タスクを **Close** して、その都度タスクの状態をクリアにすれば良いと思います。

## TrackingTime.co で工数を確認する

その日、今週、今月、年間の工数をグラフで確認したい場合は、[TrackingTime.co](https://trackingtime.co) にアクセスし、自分のアカウントでログインして、**Report** を見れば良いです。

しかし、残念なことに、週間・月間・年間の工数レポートは Pro にならないと閲覧することができません。
各ユーザーにつき、月々約 5 $ なので、それくらいなら払ってもいいと思える人は Pro にアップグレードすれば良いと思います。
私は、その都度の作業の工数を把握したいので、現状で満足していますが。

レポートを無料で見たければ、[toggl](https://toggl.com/) を好きなブラウザに拡張機能として追加して工数管理すれば、手軽にできるので良いのかなと思います。

## おわりに

最後までお読みいただきありがとうございました。

工数を減らすためには、集中力が大事だと自分は思います。
[ヤバい集中力 著.鈴木裕](https://amzn.to/2JpbTgn) という本がおすすめです。
科学的根拠を元にしていて内容はガッチリしていますが、著者の作文力が高くてすんなりと読み切れます。
ぜひ、読んでみてください：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

