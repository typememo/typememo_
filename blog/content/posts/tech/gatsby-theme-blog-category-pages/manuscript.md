---

title: カテゴリーページを作成しました
slug: /tech/gatsby-theme-blog-category-pages/
date: 2020-09-19
tags: [tech, gatsby, blog]
image: ./img/la-so-vk4vjTNVrTg-unsplash.jpg
socialImage: ./img/la-so-vk4vjTNVrTg-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [はじめに](#はじめに)
- [カテゴリーページの実装方法](#カテゴリーページの実装方法)
- [おわりに](#おわりに)


---


## はじめに

[tech](https://typememo.jp/tech)， [life](https://typememo.jp/life)， [science](https://typememo.jp/science)， という３つのカテゴリーページを作成した報告記事です．

カテゴリーページ作成のきっかけを与えてくれたのは [Google Analytics](https://analytics.google.com/) の **行動フロー** です．

この [typememo.jp](https://typememo.jp) の [#tech](https://typememo.jp/tags/tech/) タグがついている記事を読んでくれた人の約１割が `https://typememo.jp/tech/` にアクセスしていました．
しかし 2020/09/17 辺りまではカテゴリーページを実装しておらず，`https://typememo.jp/tech/` にアクセスすると [404 ページ](https://typememo.jp/404/) が表示されていました．
その後は当然なのですが，ほとんどの人がこのサイトから離脱していました．

この解析結果を目の当たりにして，何もせずに放置しておくことはできませんでした．
せっかく `https://typememo.jp/tech/` ページにアクセスして他の記事がないかどうか探してくれた人に対して失礼だと感じました．
404 ページが表示された人はきっとガッカリしたと思います．

そこで簡易的なカテゴリーページを作成しました．
カテゴリーページと言っても表示されるページの構成はタグページと同じです．

この記事では，カテゴリーページの実装方法についてご紹介します．
よければ読んでいってください．


## カテゴリーページの実装方法

カテゴリーページはタグページと同じ構成で作成しました．

実装方法はかなり適当でして，特定のタグ—今回で言えば `tech` `life` `science` —のタグページを作成する時はカテゴリーページも作成させるようにしました．

ページを作成するので [gatsby-node.js](https://gitlab.com/typememo/typememo_/-/blob/master/blog/gatsby-node.js) のタグページ作成処理の箇所にカテゴリーページ作成処理を追加しました．

```javascript
exports.createPages = async ({ actions, graphql, reporter }) => {
  // ...
  // Make tag pages
  const categories = [ "tech", "life", "science", "etc" ]
  tags.forEach(tag => {
    createPage({
      path: `/tags/${_.kebabCase(tag.fieldValue)}/`,
      component: tagTemplate,
      context: {
        tag: tag.fieldValue,
      },
    })
    // Make category pages
    if ( categories.indexOf(tag) ) {
      createPage({
        path: `/${_.kebabCase(tag.fieldValue)}/`,
        component: tagTemplate,
        context: {
          tag: tag.fieldValue,
        },
      })  
    }
  })
}
```

`tag` イテレータが `categories` 定数リストのいずれかに一致したらカテゴリーページを `createPage` API で作成する実装になっています．

GatsbyJS の `createPage` API はよく使うので，公式ドキュメントを一読しておくことを強くお勧めします．

- [GatsbyJS createPage API](https://www.gatsbyjs.com/docs/actions/#createPage)

GatsbyJS を書籍で一通り勉強したい人は次の書籍がいいと思います．

- [GatsbyJSで実現する、高速&実用的なサイト構築](https://amzn.to/2RIvuvX)


## おわりに

Google Analytics の解析結果からカテゴリーページのニーズがあることに気付いて，カテゴリーページを実装するまでのお話でした．

カテゴリーページの作りはだいぶ適当です．

でも，今回の収穫は Google Analytics を使っているとユーザーのニーズが垣間見えることが実感できたことです．

このようなデータが教えてくれる改善活動のヒントを見落とさないようにして行けたらと思います．

直接のご要望もできる限り受け付けたいと思っています．
何かあれば Gitlab の Issue にご要望を書いていただけると嬉しいです．

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

