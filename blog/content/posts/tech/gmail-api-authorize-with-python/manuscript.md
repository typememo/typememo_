---

title: Gmail API Python を使った認証方法
slug: /tech/gmail-api-authorize-with-python
date: 2020-07-28
tags: [tech, gmail, python]
image: ./img/kon-karampelas-N82naZ0N4TY-unsplash.jpg
socialImage: ./img/kon-karampelas-N82naZ0N4TY-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [まずは credential.json を取得する](#まずは-credentialjson-を取得する)
- [Gmail API に必要なライブラリをインストールする](#gmail-api-に必要なライブラリをインストールする)
- [ソースコード全文](#ソースコード全文)
- [`quickstart.py` を実行する](#quickstartpy-を実行する)
- [ソースコード詳細](#ソースコード詳細)
  - [必要なライブラリを import する](#必要なライブラリを-import-する)
  - [Gmail API に与える権限 `SCOPES` を定義する](#gmail-api-に与える権限-scopes-を定義する)
  - [Gmail API を認証するための鍵を発行する](#gmail-api-を認証するための鍵を発行する)
  - [`main()` 関数で実行する](#main-関数で実行する)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

Gmail の API を Python で叩き、認証する方法についての紹介です。

使っているパソコンは [MacMini](https://amzn.to/2VACkFe) でして、作業環境は以下のとおりです。

| 環境 |
| :-- |
| macOS Catalina 10.15.4 |
| Python 3.6.5 :: Anaconda, Inc. |

ターミナルから `python ???.py` を実行して、Gmail API を叩き、ブラウザ経由で認証させます。

認証した後は、[スレッドやメッセージの取得](#)、[メッセージの送信](#)、[メッセージのラベル変更](#)、などができるようになります。
(これらの記事は現在執筆中です。公開まで少々お待ちください。)

参考文献は [Python Quickstart](https://developers.google.com/gmail/api/quickstart/python) と [Choose Auth Scopes](https://developers.google.com/gmail/api/auth/scopes) です。

それでは参りましょう。

## まずは credential.json を取得する

Gmail API を認証させるためには、`credentials.json` が必要です。

`credentials.json` の入手方法の一例は次のとおりです。

* [Python Quickstart Step1: Turn on the Gmail API](https://developers.google.com/gmail/api/quickstart/python) の **Enable the Gmail API** をクリックしてください
![figure enable the gmail api](https://typememo.com/wp-content/uploads/2020/04/Screenshot_2020-04-18_13.42.44.png)
* **Desktop app** を選択して、Create をクリックしてください
![figure select Desktop app](https://typememo.com/wp-content/uploads/2020/04/Screenshot_2020-04-18_13.44.31.png)
* **Download client configuration** をクリックすれば、`credentials.json` がダウンロードされます
![figure download client configuration](https://typememo.com/wp-content/uploads/2020/04/Screenshot_2020-04-18_13.44.54.png)

`credentials.json` を、今回実装する python ファイルがあるフォルダに移動しておいてください。もしくは、`credentials.json` を `$HOME/.ssh/` 配下などに保存して、python に探してきてもらう方法でも良いかと思います。

## Gmail API に必要なライブラリをインストールする

次に、Gmail API に必要なライブラリをインストールしましょう。

以下の `pip` コマンドを実行してください。

```bash
pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
```

エラーはでないはずです。もしもエラーがでたら、ググって解決してくださいませ。

## ソースコード全文

以下に Gmail API 認証コードの全文を示します。

```python
from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']

def gmail_authorize():
    """Shows how to authorize Gmail API.
    """
    creds = None
    # The file token.pickle stores the user's access and 
    # refresh tokens, and is created automatically when 
    # the authorization flow completes for the first time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    return creds

def main():
    """Shows the basic usage of Gmail API.
    1. Authorize Gmail API.
    """
    creds = gmail_authorize()
    service = build('gmail', 'v1', credentials=creds)
    print(creds)
    print(service)

if __name__ == '__main__':
    main()
```

## `quickstart.py` を実行する

コードをコピペして `quickstart.py` というファイル名で `credentials.json` と同じフォルダに保存してください。

`quickstart.py` を保存したら、以下のコマンドを実行してください。

```bash
python quickstart.py
```

すると、デフォルトブラウザに Gmail API の認証ページが表示されます。

* まずはアカウントを選択してください
(画像ではアカウントリストを表示していませんが、実際には表示されているはずです)
![figure select your account](https://typememo.com/wp-content/uploads/2020/04/Screenshot-2020-04-18-14.02.39.png)
* 権限を与えるかどうか問われますので、許可しましょう
![figure authorize gmail api readonly](https://typememo.com/wp-content/uploads/2020/04/Screenshot-2020-04-18-14.02.55.png)
* 権限を許可したので、いまブラウザで表示されているページは閉じていただいて結構です。
![figure authorized](https://typememo.com/wp-content/uploads/2020/04/Screenshot-2020-04-18-14.03.51.png)

ターミナルに戻り、以下のログが出力されていれば認証成功です！
(メモリアドレス, e.g. 0x7f81e0047be0, は各環境によって異なります)

```bash
<google.oauth2.credentials.Credentials object at 0x7f81e0047be0>
<googleapiclient.discovery.Resource object at 0x7f81f0535710>
```

認証に成功したかどうかは、`token.pickle` というファイルが存在するかでも確認できます。

ターミナルで `ls` コマンドを実行して、`token.pickle` というファイルが見つかりますか？

見つかれば、認証に成功しています。

---

## ソースコード詳細

ひとまず「Gmail API を認証する」という主目的が達成できました。

これ以降で、具体的にソースコードの中身を説明します。

### 必要なライブラリを import する

`pip` コマンドでインストールしたライブラリを import します。ここは特に説明することはないかなと思います。

```python
from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
```

### Gmail API に与える権限 `SCOPES` を定義する

`SCOPES` で、Gmail API に与える権限を定義します。今回は、全てのリソースへの読み込みを許可します。

```python
# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']
```

`SCOPES` は [readonly (読み込み許可) 意外にも modify (更新) や send (送信) など、様々なオプション](https://typememo.com/2020/04/gmail-api-scopes/)があります。

`SCOPES` で定義できるオプションについては [Gmail API Scopes 日本語訳](https://typememo.com/2020/04/gmail-api-scopes/) にまとめてありますので、興味のある方はお読みください。

### Gmail API を認証するための鍵を発行する

Gmail API は以下の `gmail_authorize()` 関数で認証しています。

まず、`token.pickle` というファイルがあるかどうか調べます。

もしも `token.pickle` ファイルがあれば、`token.pickle` ファイルをロードして、`creds` (credentials の略) に代入します。

`token.pickle` がなく、さらに、`creds` が有効ではない場合、`creds` を更新するようにリクエストをします。

`token.pickle` がなく、`creds` もなければ、`credentials.json` の情報に基づいて `creds` を発行し `token.pickle` を書き出します。

最後に、`creds` を返して終了です。

```python
def gmail_authorize():
    """Shows how to authorize Gmail API.
    """
    creds = None
    # The file token.pickle stores the user's access and 
    # refresh tokens, and is created automatically when 
    # the authorization flow completes for the first time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    return creds
```

### `main()` 関数で実行する

最後に、`main()` 関数で `gmail_authorize()` を実行して、Gmail API 認証鍵 `creds` を発行して、`creds` を使って Gmail API を実行できるように認証します。

```python
def main():
    """Shows the basic usage of Gmail API.
    1. Authorize Gmail API.
    """
    creds = gmail_authorize()
    service = build('gmail', 'v1', credentials=creds)
    print(creds)
    print(service)

if __name__ == '__main__':
    main()
```

## おわりに

Gmail API with python の認証方法についての紹介でした。

当ブログでは，この記事以外にも Gmail API with python の記事をいくつか書いています．

合わせてお読みいただけると嬉しいです．

最後までお読みいただき、ありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

