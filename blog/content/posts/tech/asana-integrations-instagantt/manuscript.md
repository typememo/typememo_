---

title: Instagantt with Asana は無料かつ高機能なガントチャートツールの決定版
slug: /tech/asana-integrations-instagantt/
date: 2019-10-21
tags: [tech, asana, instagantt]
image: ./img/curtis-macnewton-vVIwtmqsIuk-unsplash.jpg
socialImage: ./img/curtis-macnewton-vVIwtmqsIuk-unsplash.jpg
imageAlt: eyecatch

---

## Instagantt with Asana が全てです

無料で高機能なガンタチャートツール [Instagantt with Asana](https://asana.com/ja/guide/help/api/instagantt) の紹介記事へのリンクです。

以下の項目に一つでも当てはまる人は、下記リンクへアクセスして、[Instagantt with Asana](https://asana.com/ja/guide/help/api/instagantt) を使ってみてください。きっと満足すると思います：）

* 個人のタスク管理をガントチャートで行いたいと思っている人
* 無料なんだけど高機能なガントチャートツールを探している人
* Asana のガントチャートは有料だと知って悲しんでいる人

[Instagantt with Asana](https://asana.com/ja/guide/help/api/instagantt)

> 本当はこの記事で Instagantt with Asana とは何かから具体的な使い方まで紹介しようと思っていたのですが、上記記事に書きたいことの全てが書かれていたので、具体的な紹介を書くのは二番煎じなのでやめました。`

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

