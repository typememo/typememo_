---

title: MacOS と HHKB の組み合わせで音声入力する方法
slug: /tech/hhkb-macos-enable-voice-input
date: 2020-07-25
tags: [tech, hhkb, mac]
image: ./img/nihonorway-graphy-nCvi-gS5r88-unsplash.jpg
socialImage: ./img/nihonorway-graphy-nCvi-gS5r88-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Fn or Command キーを2回押すことを諦める](#fn-or-command-キーを2回押すことを諦める)
- [ショートカットをカスタマイズする "Alt + Space キー"](#ショートカットをカスタマイズする-alt--space-キー)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

MacOS (本記事では [MacMini](https://amzn.to/3jODSqC)) と [HHKB](https://amzn.to/3jDJFPE) の組み合わせで音声入力するのに、少し手間取りました．

本記事は，その備忘録になります．

## Fn or Command キーを2回押すことを諦める

私の環境 (MacMini) では，HHKB の Fn キーを２回押しても，Command キーを２回押しても，音声入力することができませんでした．

どう頑張っても，ショートカットキーでは音声入力できませんでした．

なので，デフォルトのショートカットキーを利用することは諦めました．

## ショートカットをカスタマイズする "Alt + Space キー"

そこで，カスタム設定することにしました．

ショートカットキーの登録は，次の画像のように，**システム環境設定 => キーボード => 音声入力** で設定できます．

![macos voice input setting](./img/Screenshot_2020-05-10_15.27.23.png)

私の場合は **Alt キー + Space キー** に設定しました．

あとは適当なイアホンなどを MacMini に挿しておけば，MacMini で音声入力ができるようになります．

MacMini には音声入力装置が備わっていないので，外部音声入力装置が必要なんですね．

買って，使って，初めて知りました．笑

もう少し事前に調べておけばよかったです．

## おわりに

ここ最近 (2020年時点) の音声入力はかなり精度が良いです．

機械的な処理なので，誤字脱字は多少ありますが，それでも十分に実用的です．

話しながら頭を整理できて，そのついでに音声でメモまで取れてしまうのですから，便利な時代になったものです．

音声入力は，英語の文章を日本語に直すときに重宝しています．

英文を頭から日本語で音声入力するので，おかしな順番の日本語になりますが，メモとしては十分です．

後で必要な部分をちゃんとした日本語に直せば良いだけなので，作業効率が上がりました．

この記事を読んでいる人は，[HHKB](https://amzn.to/3jDJFPE) をお持ちでタイピングが好きな人が多いと思いますが，たまには音声入力も使ってみてください．

最後までお読みいただき，ありがとうございました！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

