---

title: VSCode でタブ表示できなくなったときの対処方法
slug: /tech/vscode-tab-toggle-enable/
date: 2020-08-09
tags: [tech, vscode]
image: ./img/iconfinder_14_939747_.png
socialImage: ./img/iconfinder_14_939747_.png
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [原因: Command + Ctrl + w を意図せず押下したこと](#原因-command--ctrl--w-を意図せず押下したこと)
- [対策: キーボードショートカットを無効化すること](#対策-キーボードショートカットを無効化すること)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

先日、気付いたら VsCode でファイルをタブ表示できなくなってしまいました。

編集対象を切り替えたい場合は、Ctrl + Tab で切り替えねばならず操作しづらく、複数のファイルを並べた表示したいのに、それもできない。

このような状態に対する対処方法を以下に説明します。

## 原因: Command + Ctrl + w を意図せず押下したこと

以下の図をご覧ください。

![Figure1](./img/Screenshot_2019-12-25_11.28.37.png)

`View: Toggle Tab Visibility` というコマンドが、`Ctrl + Command + w` というキーボードショートカットに紐ついていることがわかります。

この `View: Toggle Tab Visibility` は、タブ切り替えができる状態とできない状態の切り替えを行うコマンドです。

デフォルトではタブ切り替えができる状態なのですが、ふとした拍子に間違えてキーボードショートカットを押してしまうと、タブ切り替えができない状態になってしまいます。

つまり原因は、`View: Toggle Tab Visibility` コマンドを誤って実行してしまったからだったのです。

## 対策: キーボードショートカットを無効化すること

手っ取り早い解決方法は、タブ切り替えができる状態に戻した上で、`View: Toggle Tab Visibility` コマンドを無効化することでしょう。

キーボードショートカットを無効化するのは簡単で、無効化したいコマンドを右クリックしてメニューを表示させ、Remove Keybinding をクリックするだけです。 (下図参照)

![Figure2](./img/Screenshot_2019-12-25_11.29.34.png)

キーボードショートカットを別の組み合わせに置き換える方法もありますが、二度と使わないようなキーボードショートカットは混乱の元となるので、削除してしまった方がいいと自分は思います。

## おわりに

Visual Studio Code でタブ切り替えができなくなったときの対処方法についてご紹介しました。

「それそれ！探していたやつ！」と思ってくれる人がいれば、嬉しいです。

最後までお読みいただきありがとうございました ：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

