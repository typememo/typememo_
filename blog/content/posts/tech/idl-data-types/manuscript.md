---

title: idl データ型まとめ
slug: /tech/idl-data-types/
date: 2019-05-07
tags: [tech, idl]
image: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
socialImage: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [データ型一覧表](#データ型一覧表)
- [参考文献](#参考文献)

<!-- /TOC -->

## データ型一覧表

IDLの型は以下のとおりです。  

| Data Type | 型変換関数 | 型名 | 定義 |
| :--: | :--:  | :--: | :--: |
| byte | byte() | バイト | 0–255の8bit符号なし整数。 |
| int | fix() | 整数 | -32,768–+32,767の16bit符号あり整数。 |
| uint | uint() | 符号なし整数 | 0–65,535の16bit符号なし整数。 |
| long | long() | 倍長整数 | -2,147,483,648–+2,147,483,647の32bit符号あり整数。 |
| ulong | ulong() | 符号なし倍長整数 | 0–4,294,967,295の32bit符号なし整数。 |
| long64 | long64() | 4倍長整数 | -9,223,372,036,854,775,808–+9,223,372,036,854,775,807の64bit符号あり整数。 |
| ulong64 | ulong64() | 符号なし4倍長整数 | 0–18,446,744,073,709,551,615の64bit符号なし整数。 |
| float | float() | 浮動小数点数 | ±10^38^で小数点6–7桁の32bit浮動小数点数。 |
| double | double() | 倍精度浮動小数点数 |  ±10^308^で小数点15–16桁の64bit倍精度浮動小数点数。  |
| complex | complex() | 複素数 | 実数・虚数対を持つ浮動小数点数。 |
| dcomplex | dcomplex() | 倍精度複素数 | 実数・虚数対を持つ倍精度浮動小数点数。 |
| string | string() | 文字列 | 0–2,147,483,647文字長の文字列。 |

## 参考文献

- [IDL Data Types](https://www.harrisgeospatial.com/docs/IDL_Data_Types.html)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

