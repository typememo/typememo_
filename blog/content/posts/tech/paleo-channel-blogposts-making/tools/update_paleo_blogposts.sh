#!/bin/bash

export PYTHONPATH=$HOME/typememo/tools
CWD=$(cd $(dirname $0); pwd)
cd $CWD
python3 -m pip install --upgrade \
  google-api-python-client \
  google-auth-httplib2 \
  google-auth-oauthlib
python3 $CWD/update_paleo_blogposts.py
