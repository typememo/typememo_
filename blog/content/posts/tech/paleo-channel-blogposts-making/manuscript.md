---

title: パレオチャンネルブロマガ過去記事全集の作り方
slug: /life/paleo-channel-blogposts-making/
date: 2020-10-12
tags: [ life, paleo, python, gmail ]
image: ./img/nielsen-ramon-ly_gX1NARIc-unsplash.jpg
socialImage: ./img/nielsen-ramon-ly_gX1NARIc-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [はじめに](#はじめに)
- [実装方針](#実装方針)
- [実装方法](#実装方法)
  - [特定の送信者からのメールを取得する方法](#特定の送信者からのメールを取得する方法)
  - [メール本文のデコード処理](#メール本文のデコード処理)
  - [ブロマガへのリンク追加処理](#ブロマガへのリンク追加処理)
- [おわりに](#おわりに)


---


## はじめに

先日 [パレオチャンネルブロマガ過去記事全集](https://typememo.jp/life/paleo-channel-blogposts/) という記事を投稿しました．
その記事の制作過程を紹介したいと思います．

[パレオチャンネルブロマガ過去記事全集](https://typememo.jp/life/paleo-channel-blogposts/) を書く時に Gmail API を使いました．
Gmail API を叩く時に使った言語は `python` です．
Gmail API の基本的な使い方や `gmail` 自作ライブラリの使い方は [#gmail](https://typememo.jp/tags/gmail/) に譲るとして，メインの処理に絞って紹介したいと思います．


それでは参りましょう！


## 実装方針

実装は次の要求を満たすようにしました．

- `鈴木祐` からのメールだけを取得する
- メール本文から記事タイトルと記事 URL を取得する
- 取得したタイトルと URL を [パレオチャンネルブロマガ過去記事全集](https://typememo.jp/life/paleo-channel-blogposts/) に書き出す


## 実装方法

実装方針が決まったので具体的に実装していきました．

ソースコード全文をご覧になりたい方は次の３ファイルを読んでください．
- [update_paleo_blogposts.py](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/paleo-channel-blogposts-making/tools/update_paleo_blogposts.py)
- [update_paleo_blogposts.sh](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/paleo-channel-blogposts-making/tools/update_paleo_blogposts.sh)
- [update-paleo](https://gitlab.com/typememo/typememo_/-/blob/master/tools/update-paleo)

以下では，次の３つの処理に絞って説明します．
- 送信者が `鈴木祐` のメールを取得する処理
- メール本文のデコード処理
- ブロマガへのリンクの挿入処理


### 特定の送信者からのメールを取得する方法

特定の送信者からのメールだけに絞るにはクエリーに `from:NAME` を指定してあげれば良いです．
([Gmail で使用できる検索演算子](https://support.google.com/mail/answer/7190?hl=ja))

今回はパレオな兄貴こと `鈴木祐` さんが送信者のメールだけフィルタリングしたいので次のように書けば良いです．

```python
  # Get messages list
  msgs = gmail.get_messages(service, userid="me", query="from:鈴木祐")
```


### メール本文のデコード処理

[Gmail API で取得したメール本文をデコード処理する方法 – Python 編](https://typememo.jp/tech/gmail-api-decode-body/) をお読みください．

本記事の一部分として扱うにはボリュームがあるので分離させました．


### ブロマガへのリンク追加処理

ブロマガへのリンクの追加方法は原始的でして [パレオチャンネルブロマガ過去記事全集 (2019年2月~)](https://typememo.jp/life/paleo-channel-blogposts/) の `manuscript.md` 内にブロマガの URL が無ければ `## ブロマガ全集` ブロックにリンクを追加するようにしました．

まぁ，こんな実装ですがきちんと動いているので今のところ満足ですね．

[D-Lab](https://daigovideolab.jp/) にブロマガ機能を実装する予定とのことで，心待ちにしています．
[パレオチャンネルブロマガ過去記事全集 (2019年2月~)](https://typememo.jp/life/paleo-channel-blogposts/) には関連する記事を集めたりとか記事検索かけたりする機能はないので．
チーム DaiGo の活動を期待しています！

```python
def main():
  ...
  # Add blogpost link
  for msg in reversed(msgs):
    msg_ = gmail.get_message(service, msg)
    blogpost_subject = get_blogpost_subject(msg_)
    blogpost_url = get_blogpost_url(msg_)
    if not is_included(manuscript_file, blogpost_url):
      add_blogpost(manuscript_file, blogpost_subject, blogpost_url)
  print("End of the script.")


def add_blogpost(manuscript, subject, url):
  """Add blogpost to manuscript"""
  line_number = 0
  with open(manuscript, "r") as file:
    lines = file.readlines()
  for line in lines:
    if ("## ブロマガ全集" in line):
      lines.insert(line_number + 2, f"- [{subject}]({url})\n")
      with open(manuscript, "w") as file:
        file.writelines(lines)
      print("Add:", subject)
      return 0
    line_number += 1
```


## おわりに

[パレオチャンネルブロマガ過去記事全集 (2019年2月~)](https://typememo.jp/life/paleo-channel-blogposts/) の作り方の紹介でした．

へっぽこな実装でも公開していく過程で自分自身からフィードバックをもらえるので記事にしてみました．

たぶんほとんどの人がこの記事は読まないでしょう．
もしもこの記事をここまで読んでくれた方がいたら，その人とは友達になれる気がします．笑

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

