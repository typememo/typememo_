---

title: Visual Studio Code 新しいターミナルのキーボードショートカット
slug: /tech/vscode-keyboard-shortcut-focus-terminal/
date: 2020-08-05
tags: [tech, vscode]
image: ./img/max-duzij-qAjJk-un3BI-unsplash.jpg
socialImage: ./img/max-duzij-qAjJk-un3BI-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [結論](#結論)
- [設定方法](#設定方法)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

Visual Studio Code で新しいターミナルを開く，もしくはターミナルをフォーカスするキーボードショートカットの設定方法について紹介します．

## 結論

- Preferences 
=> Keyboard Shortcuts 
=> Terminal: Focus Terminal

にお好きなキーボードショートカットを設定すれば OK です．

## 設定方法

Visual Studio Code の左下にある設定アイコン (歯車アイコン) をクリックして、Keyboard Shortcuts をクリックします。

![FigureVisualStudioCodeKeybindings](./img/Screenshot_2020-01-25_at_11.52.11-1.png)

するとキーボードショートカット設定画面が表示されると思います。

その設定画面上部にある検索ボックスに、**Terminal: Focus Terminal** と入力します。

その検索結果から Terminal: Focus Terminal を見つけてください。

見つけたら、ダブルクリックしてください。

キーボードショートカットをどのキーに割り当てるか聞かれます。

自分は `Command + .` キーを割り当てました．

その他のキーでも問題ありませんが、他のコマンドと競合しないように気をつけてください。

Visual Studio Code のエディター画面に戻り、先ほど割り当てたキーボードショートカットを押すと、ターミナルが表示されていない状態だとターミナルが表示され、既にターミナルが表示されている状態だとターミナルがフォーカスされます。

## おわりに

これで、キーボードから手を離さずにターミナルに移動できるようになりました：）

最後までお読みいただきありがとうございました！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

