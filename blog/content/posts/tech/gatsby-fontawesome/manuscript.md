---

title: GatsbyJS に FontAwesome を導入し実装する方法
slug: /tech/gatsby-fontawesome/
date: 2020-10-14
tags: [tech, gatsby]
image: ./img/harpal-singh-_zKxPsGOGKg-unsplash.jpg
socialImage: ./img/harpal-singh-_zKxPsGOGKg-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [はじめに](#はじめに)
- [FontAwesome を導入する](#fontawesome-を導入する)
- [FontAwesome を実装する](#fontawesome-を実装する)
- [ちなみに typememo.jp では](#ちなみに-typememojp-では)
- [おわりに](#おわりに)


---


## はじめに

[Gatsby.JS](https://www.gatsbyjs.com/) で [FontAwesome](https://fontawesome.com/)
を導入して実装するまでの方法を紹介します．

"Gatsby.JS を使っているんだけど FontAwesome の記述方法がわからなくて困っている"
そんな人に読んで欲しい記事です．

一つコマンド実行して2–3文追加するだけで実現できます．

とっても簡単です．

それでは参りましょう！

参考図書
- [GatsbyJSで実現する、高速＆実用的なサイト構築](https://amzn.to/2H92paI)

参考サイト
- https://www.gatsbyjs.com/docs/recipes/styling-css/#using-font-awesome


## FontAwesome を導入する

次の `npm` コマンドを `gatsby-config.js` とかがあるディレクトリで実行してください．

```sh
$ npm install \
    @fortawesome/fontawesome-svg-core \
    @fortawesome/free-brands-svg-icons \
    @fortawesome/free-regular-svg-icons \
    @fortawesome/free-solid-svg-icons \
    @fortawesome/react-fontawesome
```

これで FontAwesome の導入が完了しました．

はい，とても簡単ですね．笑


## FontAwesome を実装する

`FontAwesomeIcon` を `@fortawesome/react-fontawesome` からインポートします．

さらに表示したいアイコン `fa?????` を `@fortawesome/free-brands-svg-icons`
からインポートします．

それぞれインポートしたら後は `<FontAwesomeIcon icon={fa?????}>`
の形式で書けば OK です．

具体例は以下のとおりです．

```js
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faReact } from "@fortawesome/free-brands-svg-icons"

const IndexPage = () => (
  <Layout>
    <FontAwesomeIcon icon={faReact} />
  </Layout>
)

export default IndexPage
```

この例では React のアイコンが表示されます．

はい，めちゃめちゃ簡単でしたね．笑


## ちなみに typememo.jp では

FontAwesome を以下の用途で使っています．
- 検索窓の虫眼鏡
- Twitter アイコン
- Gitlab アイコン
- 封筒アイコン (OFUSE のアイコンの代替)

文字よりも絵の方が直感的に多くの情報を伝えられますんで，これからも活用していきたいと思います．


## おわりに

Gatsby.JS に FontAwesome を導入して実装する方法についての紹介でした．

抽象化されているコンポーネントをただ使うだけなので，本当に簡単でした．笑

こういう抽象化の話は [達人プログラマー](https://amzn.to/3jWXHvm) に書いてありますが，
実際に抽象的にプログラミングするのって慣れていないとできないですよねー．

今はまだ全然未熟者なので，少しずつ抽象化プログラミングができるようにトレーニングします．

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

