---

title: CEF (Chromium Embedded Framework) とは何か？
slug: /tech/cef-introduction/
date: 2020-07-28
tags: [tech, cef, chromium, browser]
image: ./img/mitchell-luo-jz4ca36oJ_M-unsplash.jpg
socialImage: ./img/mitchell-luo-jz4ca36oJ_M-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [CEF とは？](#cef-とは)
- [概要](#概要)
- [Use cases](#use-cases)
- [おわりに](#おわりに)

<!-- /TOC -->

----

本記事は [Chromium Embedded Framework - Wikipedia](https://en.wikipedia.org/wiki/Chromium_Embedded_Framework) の日本語訳です。

筆者なりの日本語訳なので間違いが含まれている可能性が高いです。

CEF にお詳しい方、不適切な箇所があればコメントいただけると大変嬉しいです。

## CEF とは？

> The Chromium Embedded Framework (CEF) is an open-source software framework for embedding a Chromium web browser within another application. This enables developers to add web browsing functionality to their application, as well as the ability to use HTML, CSS, and JavaScript to create the application's user interface (or just portions of it).

Chromium Embedded Framework (CEF) は Chromium ウェブブラウザーをアプリケーションに組み込むためのオープンソースフレームワークです。

開発者は CEF を使うことで Chromium の機能 [^1] を各人のアプリケーションに加えることができるようになります。

[^1]:  いわゆる HTML CSS JavaScript を使ってアプリケーションのユーザーインタフェース (UI) を作る機能

> CEF runs on Linux, macOS, and Windows. It has many language bindings including C, C++, Go, Java, and Python.

CEF は Linux, macOS, Windows で使えます。

CEF は C C++ Go Java Python などの言語バインディングを持っています。

## 概要

> There are two versions of Chromium Embedded Framework: CEF 1 and CEF 3. Development of CEF 2 was abandoned after the appearance of the Chromium Content API.

CEF は CEF1 と CEF3 という2つのバージョンがあります。

CEF2 は Chromium の Content API の出現により開発が行われなくなりました。

> CEF 1 is a single-process implementation based on the Chromium WebKit API. It is no longer actively developed or supported.

CEF1 はシングルスレッドです。

なぜなら、Chromium の WebKit API を基にして実装されているからです。

現在は開発もされておらずサポートもされていません。

> CEF 3 is a multi-process implementation based on the Chromium Content API and has performance similar to Google Chrome. It uses asynchronous messaging to communicate between the main application process and one or more render processes (Blink + V8 JavaScript engine). It supports PPAPI plugins and extensions, both internal (PDF viewer) or externally loadable. The single-process run mode is not supported, but still present; currently is being used for debugging purposes only.

CEF3 はマルチスレッドです。

なぜなら、Chromium の Content API を基にして実装されているからです。

Google Chrome と同等のパフォーマンスがあります。

メインアプリケーションプロセス (ブラウザプロセス) と1つないしは複数のレンダリングプロセス (Blink + V8) の間で非同期の通信をしています。

PPAPI プラグインと拡張機能をサポートしています。

シングルプロセスモードでの実行はサポートされていませんが、現在でも使えます。基本的に、デバッグの目的で使用されています。

> On March 16, 2019, the CEF version numbering changed with the release of CEF 73.1.3+g46cf800+chromium-73.0.3683.75. The previous release on March 14, 2019 was CEF 3.3683.1920.g9f41a27. Both of these releases were based on Chromium 73.0.3683.75, however the new version numbering has the major number the same as the Chromium major version number it is based on.

2019.03.16 CEF のバージョンのナンバリング方法が変わりました。

2019.03.16 のバージョンは 73.1.3+g46cf800+chromium-73.0.3683.75 です。 

2019.03.14 の CEF のバージョンは 3.3683.1920.g9f41a27 でした。

これらは両方とも Chromium 73.0.3683.75 を基にしています。

新しいナンバリング方法は Chromium のバージョンが CEF のバージョンにも含まれるようになり、どのバージョンの Chromium を基にしている CEF なのか識別しやすくなりました。

> CEF comes with a sample application called CefClient that is written in C++ using WinAPI, Cocoa, or GTK (depending on the platform) and contains demos of various features. Newer versions include a sample application called CefSimple that, along with an accompanying tutorial, show how to create a simple application using CEF 3.

CEF1 は CefClient というサンプルアプリケーションを備えています。

CEF3 は CefSimple というサンプルアプリケーションを備えています。

> Documentation can be found in the header files located in the "include" directory and on wiki pages.

ドキュメントは [include](https://bitbucket.org/chromiumembedded/cef/src/master/include/) ディレクトリにあるヘッダーファイルと [wiki](https://bitbucket.org/chromiumembedded/cef/wiki/Home) にあります。

> Spotify maintains development and stable branches builds for Linux, Mac and Windows in 32- and 64-bit forms.

[Spotify が Linux Mac Windows の 32bit と 64bit 形式の開発中および安定版ブランチでのビルド済バイナリを公開しています。](https://www.spotify.com/ro/opensource/)

ちなみに、Spotify Desktop client で CEF が使用されているそうです。

## Use cases

CEF が使われているソフトウェアで筆者が「へぇー！」って思ったのは以下の３つです。

* Adobe Brackets - エディター
* Evernote - エディター
* Spotify - 音楽ストリーミングサービス

## おわりに

Chromium Embedded Framework (CEF) - Wikipedia の日本語訳でした。

本記事に不備があればコメントしていただけると大変嬉しいです。

次は CEF を使ってみたいと思います。

最後までお読みいただきありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

