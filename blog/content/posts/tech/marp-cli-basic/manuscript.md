---

title: marp 自作テーマが適用できない人は marp-cli を使えばいいと思う
slug: /tech/marp-cli-basic/
date: 2020-10-31
tags: [tech, marp, cli]
image: ./img/hannah-joshua-46T6nVjRc2w-unsplash.jpg
socialImage: ./img/hannah-joshua-46T6nVjRc2w-unsplash.jpg
imageAlt: eyecatch

marp: true
theme: typememo
paginate: true
_class: lead
_header: typememo.jp
_footer: © 2020 typememo

---

Contents

- [はじめに](#はじめに)
- [marp-cli をインストールする](#marp-cli-をインストールする)
- [marp コマンドでスライドを作成する](#marp-コマンドでスライドを作成する)
  - [marp コマンドの基本的な文法](#marp-コマンドの基本的な文法)
  - [marp コマンドのテーマオプション設定](#marp-コマンドのテーマオプション設定)
  - [/path/to/theme.css は .zshrc に定義しておくと良い](#pathtothemecss-は-zshrc-に定義しておくと良い)
- [おわりに](#おわりに)
  - [愛用品](#愛用品)

---

## はじめに

**marp の自作テーマが適用できないー！** という風に頭を抱えている人向けの救済記事です．

**vscode に自作テーマ css のパスを通しているのに，なぜ** と困っている人は是非読んで欲しいです．

その問題 [marp-cli](https://github.com/marp-team/marp-cli) を使えば解決できますよ．

本記事では `marp-cli` の最低限の使い方をご紹介します．

気になる方は読み進めていただければと思います．

---

## marp-cli をインストールする

さて，まずは `marp-cli` を `npm` コマンドでインストールしましょう．

```shell
$ npm install -g @marp-team/marp-cli
```

`npm` コマンドがまだ有効になっていない方は，
[asdf node.js のバージョン管理方法](https://typememo.jp//tech/asdf-nodejs/)
を読んで適当な `nodejs` をインストールしてください．

`nodejs` がインストールされればそれに付随して `npm` もインストールされますので，ぜひ！

---

## marp コマンドでスライドを作成する

`marp-cli` をインストールしたら `marp` コマンドが使えるようになっているはずです．

`marp` コマンドがインストールされているかどうかは
`$ which marp` など実行すればわかります．

ちなみに筆者の環境 (MacOS 10.15.7) では以下の通りでした．

```shell
$ which marp
/usr/local/bin/marp
```

---

### marp コマンドの基本的な文法

ということで早速 `marp` コマンドを実行してみましょう．

基本的な文法は `$ marp /path/to/example.md` です．

```shell
$ marp /path/to/example.md
```

このコマンドを実行すると
`/path/to/example.md` と同じディレクトリに
`/path/to/example.pdf` が生成されます．

続いて，`marp` コマンドのオプションについてみてみましょう．

---

### marp コマンドのテーマオプション設定

`marp` コマンドで主に筆者が使っているオプションは次の３つです．

- `--pdf` pdf を生成することを明示的に宣言する
- `--preview` プレビューを表示する
- `--theme /path/to/theme.css` テーマを指定する

これらのオプションを使った `marp` コマンドは例えばこんな感じ．

```shell
$ marp \
  --pdf \
  --preview \
  --theme /path/to/theme.css \
  /path/to/example.md
```

---

### /path/to/theme.css は .zshrc に定義しておくと良い

自作テーマへのパスをいちいち書くのは面倒くさいので，
`.zshrc` などに環境変数を定義しておくと良いかなと思います．

例えば，筆者は以下の文を `.zshrc` に書いています．

```shell
# --------------------------------------------
# Marp mytheme
# --------------------------------------------
MARP_THEME_TYPEMEMO="$HOME/typememo/tools/res/marp_theme_typememo.css"
```

やっぱり環境変数を定義しておくと，とても便利ですねー．

---

## おわりに

`marp-cli` を使うとめっちゃ便利だぞ，という筆者の思いは伝わりましたでしょうか？

`marp` の自作テーマが適用できずに悶々している人がこの記事を読んで，すっきりすることを祈っています．

`marp` は本当に便利ですねー．

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

---

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
- [HHKB Professional 墨](https://amzn.to/3dGtLS1) x 
[HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
- [Apple MacMini](https://amzn.to/34jvsSt)  
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
