---

title: Chrome のアイコンがモンスターボールに見えて仕方ないのは私だけだろうか？
slug: /tech/chrome-icon-looks-like-pokemon-monster-ball/
date: 2020-10-30
tags: [tech, chrome]
image: ./img/mitchell-luo-jz4ca36oJ_M-unsplash.jpg
socialImage: ./img/mitchell-luo-jz4ca36oJ_M-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [はじめに](#はじめに)
- [Chrome v ポケモンモンスターボール](#chrome-v-ポケモンモンスターボール)
- [おわりに](#おわりに)
  - [愛用品](#愛用品)

---

## はじめに

最近，ふと思ったのですが，

> Google Chrome のアイコン (ロゴ) がどうしようもなくポケモンモンスターボールに見えるなー

と．

そもそもどうしてこんな疑問をもったのかといいますと，
"Chrome のロゴデザインってどこから着想を得たんだろう？"
と疑問に思ったからです．

そこで早速 Google 先生に
"chrome ロゴ デザイン"
とか
"chrome logo design"
とかでキーワード検索かけました．

しかし，それっぽい情報はヒットしませんでした．

情報がヒットしなかったので，自分の考えを適当に書いておこう，と思い立ちまして記事を書きました．

画像を載せて面白半分で比較しています．

みなさまも面白半分で読んでいただければと思います．

## Chrome v ポケモンモンスターボール

まずは現在の Chrome のロゴです．

[日本では Chrome のシェアは約50%](https://gs.statcounter.com/browser-market-share/all/japan)
なので多くの方は見たことがあるロゴですよね．

![chrome](./img/512px-Google_Chrome_icon_September_2014.png)
_[wikipedia より引用](https://en.wikipedia.org/wiki/Google_Chrome)_

続いて，ポケモンモンスターボールのデザインです．

![polemon monster ball](./img/518SezoWVIL._AC_SL1100_.jpg)
_[amazon より引用](https://amzn.to/3e9e4Tr)_

ここまで読んでくれた方に聞きたいことがあります．

**ほら，なんかこの２つ似てませんか!?**

特に **中央の丸とその丸を取り囲むドーナツ型の円** の感じとか．

ドーナツ型の円の外側の領域の切り方は，３等分と２等分で異なりますが，それでも何となく似てませんか？

Chrome 側の公式見解を筆者は見つけられていないのであくまで想像の範疇ですが，
**Chrome のロゴはポケモンモンスターボールから着想を得た** と筆者は推測しています．

Chrome および Chromium のロゴデザインをした人からの公式見解が欲しいです．

どこから着想を得たのかすごく気になりますねー．

## おわりに

Chrome のロゴがポケモンモンスターボールにしか見えない件についての記事でした．

つらつらと書き連ねたネタ記事でしたがいかがだったでしょうか？

もしも気に入っていただけたらどこかでシェアしていただけるとありがたいです．

Chrome のロゴデザイナーのもとに届きますように．

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．
