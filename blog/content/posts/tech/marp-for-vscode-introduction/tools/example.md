---

title: marp for vscode の具体例
slug: /tech/marp-for-vscode-introduction/example/
date: 2020-10-17
tags: [tech, marp, vscode]
image: ../img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg
socialImage: ../img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg
imageAlt: eyecatch

marp: true
theme: gaia
paginate: true
header: This is header
footer: This is footer

---


Contents

- [References](#references)
- [Title](#title)
- [Image right](#image-right)
- [Image left](#image-left)
- [Image contain](#image-contain)
- [Image contain ><^](#image-contain-)
- [Code block](#code-block)

---

## References

- [example.md](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/marp-for-vscode-introduction/tools/example.md)
- [example.pdf](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/marp-for-vscode-introduction/tools/example.pdf)

---

## Title

Hello, marp!

---

## Image right

![bg right](../img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg)

---

## Image left

![bg left](../img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg)

---

## Image contain

- Hoge hoge ...
- Fuga fuga ...

![bg right contain](../img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg)

---

## Image contain ><^

1. Hoge hoge ...
1. Fuga fuga ...

![bg left contain](../img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg)

---

## Code block

- `Python` Hello, world!

```python
def hello():
  print("Hello, world!")
```
