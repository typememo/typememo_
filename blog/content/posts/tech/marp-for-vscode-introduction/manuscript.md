---

title: パワポ使って消耗してませんか？ Marp for VSCode を使いこなして楽をする方法教えます
slug: /tech/marp-for-vscode-introduction/
date: 2020-10-16
tags: [tech, marp, vscode]
image: ./img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg
socialImage: ./img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [はじめに](#はじめに)
- [基本ルール](#基本ルール)
- [具体例](#具体例)
- [marp の有効化](#marp-の有効化)
- [テーマ設定](#テーマ設定)
- [スライド番号](#スライド番号)
- [ヘッダーとフッター](#ヘッダーとフッター)
- [その他のディレクティブ](#その他のディレクティブ)
- [画像](#画像)
- [スライド出力](#スライド出力)
- [おわりに](#おわりに)


---


## はじめに

[marp for vscode](https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode)
の使い方についてのざっくりとした説明です．

今の内に vscode にインストールしておいてください．

"発表用のスライド作らないといけないけど，パワポ使ってスライド作るの面倒くさいな．"
と思っている人向けの記事です．

マークダウンで手軽にスライドを作ることができるので marp for vscode はおすすめです．

元はマークダウンファイルでプレインテキストなので Git でのバージョン管理も楽チンです．

それでは参りましょう！

参考:
- [達人プログラマー](https://amzn.to/3iVuY8R) 第3章 プレインテキスト 読んでおくと良いです．
- https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode
- https://marp.app/
- https://marpit.marp.app/


## 基本ルール

基本ルールはとてもシンプルで `---` で区切られた内容が一つのスライドになることだけです．

その他の細かい marp 独特の文法は以下で説明します．


## 具体例

具体例を見ながらの方がよく理解できると思いますのでリンクを記載しておきます．

- [example.md](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/marp-for-vscode-introduction/tools/example.md)
- [example.pdf](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/marp-for-vscode-introduction/tools/example.pdf)


## marp の有効化

マークダウンファイルのフロントマターに `marp: true`
と書くだけで marp for vscode が有効になります．

適当にスライドの中身を書いて
`>Open preview to the side`
をクリックしてプレビューを表示するとこんな感じのスライドができます．

![marp-example-1](./img/Screenshot_2020-10-15_17.56.45.png)


## テーマ設定

テーマ設定はフロントマターに
`theme: gaia` や `theme: uncover`
のように書けば OK です．

個人的に左上から始まるスライドが好きなので
[gaia](https://github.com/marp-team/marp-core/tree/master/themes#gaia)
というテーマを使っています．

[uncover](https://github.com/marp-team/marp-core/tree/master/themes#uncover)
などの中央寄せのモダンなテーマも用意されているようです．


## スライド番号

スライド番号を降りたい場合はフロントマターに
`paginate: true`
と書けば OK です．


## ヘッダーとフッター

ヘッダーとフッターはそれぞれフロントマターに
`header: This is header` や `footer: This is footer`
のように書けば OK です．


## その他のディレクティブ

そのほかのディレクティブについては
[Local directives](https://marpit.marp.app/directives?id=local-directives-1)
を参照してください．


## 画像

画像を挿入するには `![]()` の記法を使えば良いのですが少し独特の書き方をします．

`![bg right](./path/to/img)` でスライド右半分に画像が挿入されます．

`![bg left](./path/to/img)` でスライド左半分に画像が挿入されます．

`![bg right contain](./path/to/img)` でスライド右側に画像が挿入されます．
`![bg left contain](./path/to/img)` でスライド左側に画像が挿入されます．
しかし，スライド左側に画像が挿入されるとスライド見出しが右側に移動するので少し違和感が生じます．


## スライド出力

スライドを PDF 形式などで出力する時は
`>Marp: Export slide deck...`
を実行してください．


## おわりに

marp for vscode を使って楽をするための marp for vscode 使い方についての紹介でした．

画像とかコードブロックとかが挿入できれば基本的なスライドは作れると思います．

プレインテキストなのでついついたくさん書いてしまいそうになるので気をつけてください．

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

