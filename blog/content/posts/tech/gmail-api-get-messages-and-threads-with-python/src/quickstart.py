#!/usr/bin/env python

import gmail
import json
import os


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']
TOKEN_FILE = "./token.pickle"
CREDS_FILE = "./credentials.json"


# Main
def main():
  """Shows the basic usage of Gmail API.
  - Authorize Gmail API.
  - Get messages
  - Get threads
  - Check results
  """
  
  # Pick files
  token_file = os.path.abspath(TOKEN_FILE)
  creds_file = os.path.abspath(CREDS_FILE)

  # Authorize Gmail API
  creds = gmail.authorize(SCOPES, token_file, creds_file)

  # Build Gmail API
  service = gmail.build_service(creds)

  # Get messages list
  msgs = gmail.get_messages(service, userid="me", query="newer_than:1h")

  # Get a head message
  msg = gmail.get_message(service, msgs[0], userid="me")

  # Get threads list
  threads = gmail.get_threads(service, userid="me", query="newer_than:1h")

  # Get a head thread
  thread = gmail.get_thread(service, threads[0], userid="me")

  # Show results
  print("Messages list")
  print(msgs)
  print("\n")
  print("Head message")
  print(json.dumps(msg, indent=2))
  print("\n")
  print("Threads list")
  print(threads)
  print("\n")
  print("Head thread")
  print(json.dumps(thread, indent=2))

if __name__ == '__main__':
  main()
