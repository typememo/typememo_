---

title: R-Car って何？ —勉強メモ
slug: /tech/r-car-introduction/
date: 2020-07-19
tags: [tech, car]
image: ./img/a-l-moheR9rdRGY-unsplash.jpg
socialImage: ./img/a-l-moheR9rdRGY-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [R-Car とは，車載情報システム SoC のニックネームである](#r-car-とは車載情報システム-soc-のニックネームである)
- [R-Car のラインナップ](#r-car-のラインナップ)
- [おわりに](#おわりに)
- [References](#references)

<!-- /TOC -->

----

## はじめに

お仕事で R-Car という言葉に遭遇しました．

何回か聞いたことあったけど，"R-Car って何？" って聞かれたときに答えられないなぁと思い，少し調べてみることにしました．

## R-Car とは，車載情報システム SoC のニックネームである

[Renesas Electronics => All Products => 車載用デバイス => 車載用プラットフォーム](https://www.renesas.com/jp/ja/products/automotive/automotive-lsis.html) によれば，

> 車載情報システム用Socを、「R-Car」というニックネームで展開しています。

だそうです．

[IT 用語辞典 e-Words](http://e-words.jp/w/SoC.html) によれば，

> Soc (System On a Chip) とは，ある装置やシステムの動作に必要な機能の全てを，一つの半導体チップに実装する方式のことである．
> ターゲットとなる装置により構成は異なるが，マイクロプロセッサを核に各種のコントローラ回路やメモリなどを統合したチップが多い．

だそうです．

なるほど．

つまり， **R-Car とは車載情報システムの動作に必要な機能の全てが実装された半導体チップ** ，ということですな．

## R-Car のラインナップ

R-Car—半導体チップ—ですが，幅広いラインナップがあるようで，大きく分けて5つのグレードに分けられています．

---

![figure R-Car loadmap](https://www.renesas.com/img/products/automotive/rcar-roadmap.png)
**Figure.1** R-Car ロードマップ
([Renesas Electronics => All Products => 車載用デバイス => 車載用プラットフォーム => R-Car](https://www.renesas.com/jp/ja/products/automotive/automotive-lsis/r-car.html#productInfo) より転載)

---

Figure.1 右半分の 3rd Generation の R-Car についての説明は以下の通りです．

以下の R-Car についての説明は，[Renesas Electronics => All Products => 車載用デバイス => 車載用プラットフォーム => R-Car](https://www.renesas.com/jp/ja/products/automotive/automotive-lsis/r-car.html#productInfo) の内容を転載しました．

タイピングして内容を理解する人間なので，打たせていただきました．

* [R-Car H3](https://www.renesas.com/jp/ja/solutions/automotive/soc/r-car-h3.html)
自動運転時代の車載コンピューティング・プラットフォームとして，安全運転支援システムに向けて，車載カメラなど各種センサーから入力される大量の情報を，リアルタイムかつ正確に処理を行うコンピューティング性能を強化しました．
ユーザは障害物の検知やドライバ状態の認識，さらには危険予測や危険回避判断のような複雑な処理を実現することが可能になります．
さらに，このような高度なアプリケーションによる安全運転支援システムの実用化を加速させるため，R-Car H3 は自動車用機能安全規格 ISO26262 (ASIL B) に対応しました．

* [R-Car M3](https://www.renesas.com/jp/ja/solutions/automotive/soc/r-car-m3.html)
先行する R-Car H3 に対し，様々な用途に幅広く活用できるように機能・性能の最適化を行いました．
ミディアムクラスの車載コンピューティングシステムに最適です．

* [R-Car E3](https://www.renesas.com/jp/ja/solutions/automotive/soc/r-car-e3.html)
大型ディスプレイ搭載のクラスターシステムに最適です．
また，クラスターとディスプレイオーディオの統合化システムも実現可能です．

* [R-Car D3](https://www.renesas.com/jp/ja/solutions/automotive/soc/r-car-d3.html)
フルグラフィックスクラスターシステム向けに開発した製品であるため，高性能なグラフィックス描画とシステムコスト低減の両立が可能です．

* [R-Car V3M](https://www.renesas.com/jp/ja/solutions/automotive/soc/r-car-v3m.html)
R-Car V3M は Renesas autonomy の第一弾となるフロントカメラ向け SoC で，NCAP 安全基準を満たすカメラシステム開発に最適です．
高効率な画像認識を実現する画像認識エンジンを搭載し，高度な機能安全のサポート，カメラ一体のインテグレーションによりコスト削減も可能にする ISP を搭載しています．

* [R-Car V3H](https://www.renesas.com/jp/ja/solutions/automotive/soc/r-car-v3h.html)
R-Car V3H はステレオフロントカメラ・アプリケーションに最適です．
コンピュータの画像処理の高性能化にフォーカスしたアーキテクチャで，NCAP (New Car Assessment Program) の機能から完全自動運転まで，関連する全ての ADAS 機能を可能にします．

## おわりに

という感じで R-Car 3rd Generation について書き写してみたわけですが，書けば書くほど知らない言葉に遭遇しました．．．

R-Car の具体的な仕様ってどうなってるんだろう？

自動車用機能安全規格 ISO26262 (ASIL B)って具体的にどういう内容の規格なんだろう？

クラスターシステムってなんだろう？

NCAP ってなんだろう？

ISP ってなんだろう？

知れば知るほど，己が無知だということを痛感します．

日々勉強できて，それはそれで筆者としては望んだ生き方なので楽しいですが：）

最後までお読みいただき，ありがとうございました．

## References

* [https://www.renesas.com/jp/ja/products/automotive/automotive-lsis/r-car.html#productInfo](https://www.renesas.com/jp/ja/products/automotive/automotive-lsis/r-car.html#productInfo)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

