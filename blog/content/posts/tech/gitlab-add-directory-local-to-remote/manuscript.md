---

title: 既存のディレクトリを GitLab でバージョン管理する方法
slug: /tech/gitlab-add-directory-local-to-remote/
date: 2019-12-08
tags: [tech, git, gitlab]
image: ./img/yancy-min-842ofHC6MaI-unsplash.jpg
socialImage: ./img/yancy-min-842ofHC6MaI-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Gitlab に新規プロジェクトを作成する](#gitlab-に新規プロジェクトを作成する)
- [既存のディレクトリを Git 化するための具体的な手順](#既存のディレクトリを-git-化するための具体的な手順)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

先日、[Gitlab で新規リモートプロジェクトを作成してローカル環境にクローンする方法](https://typememo.com/gitlab-first-step) という記事を投稿しました。

でも Git を使いはじめてみようと思った人にとって最初に必要なことって今ある既存のディレクトリを Git 化することですよね。

ということで本記事では、既存のディレクトリを Git 化してリモートプロジェクトとリンクする方法について紹介します。

具体的には、Google Drive のマイドライブを Git 化して GitLab でバージョン管理する方法を紹介していきます。

## Gitlab に新規プロジェクトを作成する

まずは GitLab に新規プロジェクトを立ち上げましょう。  

[GitLab.com](https://gitlab.com) にアクセスして Sign In or Sign Up をしてください。  

右上の New Project をクリックしてください。  

次の項目を適当に入力します。

- Project name
- Project URL
- Project slug
- Project description

入力した後，Private にするか Public にするか選択してください。

Initialize repository with README に **チェックを入れないでください** 。

最後に Create project を押します。

すると，**ローカル環境の既存ディレクトリもしくは新規ディレクトリの Git 化するコマンド** が表示されていると思います。

次節からはターミナルでの作業になります。

ターミナルを起動してから、次節を読んでください。

## 既存のディレクトリを Git 化するための具体的な手順

今回は、既存のディレクトリを Git 化するので、**Push an existing folder** の手順に従います。  

例えば、`$HOME/sample` を Git 化する場合、以下のコマンドを実行してください。  

```bash
cd $HOME/sample

# git init を実行すると .git ディレクトリが生成され、ローカル環境を Git 化できます。
# しかし、まだリモートプロジェクトとは繋がっていません。
git init

# ${Username} と ${ProjectName} はご自身のユーザー名とプロジェクト名に書き換えてください。
# git remote add コマンドを実行するとローカルとリモートのリンクが完了します。
git remote add origin git@gitlab.com:${Username}/${ProjectName}.git

# git add . コマンドを実行すると、全てのファイルをステージングします。
# すべてを Git 管理したくない場合は .gitignore ファイルを作成して、
# そこにディレクトリ名 or ファイル名を記入してください。
git add .

# git commit -m コマンドを実行すると、先ほどステージングしたファイルを、
# コミットメッセージ付きで、コミットします。
git commit -m "first commit"

# git push コマンドを実行すると、今までのコミットを、リモートプロジェクトに反映できます。
# git push コマンドを実行しない限り、リモートプロジェクトに今までのコミットは反映されません。
git push origin master
```

上記のコマンドを実行したら、gitlab.com の先ほど作成したリモートプロジェクトのページを更新してください。

`first commit` というコミットメッセージとともに `$HOME/sample` の内容が Gitlab に反映されているはずです。

以上で、既存ディレクトリを Git 化して Gitlab.com でバージョン管理する準備は終了です。

あとは、Git 化したファイルを編集するたびに、`git add`、`git commit`、`git push` コマンドを使ってバージョン管理するだけです。

## おわりに

本記事の内容は [ProGit Web版](https://git-scm.com/book/) を参考にしています。

また、GitLab の詳細を知りたければ、[GitLab実践ガイド](https://amzn.to/3445PBn) を参照していただければと思います。

最後まで、お読みいただきありがとうございました．

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

