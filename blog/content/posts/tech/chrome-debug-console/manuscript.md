---

title: Chrome をターミナルでデバッグする方法
slug: /tech/chrome-debug-console/
date: 2020-08-02
tags: [tech, chrome]
image: ./img/mitchell-luo-jz4ca36oJ_M-unsplash.jpg
socialImage: ./img/mitchell-luo-jz4ca36oJ_M-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [結論: google-chrome --enable-logging=stderr --vmodule](#結論-google-chrome---enable-loggingstderr---vmodule)
- [References](#references)

<!-- /TOC -->

----

## はじめに

Chrome をターミナルでデバッグする人なんて、ほとんどいないと思います。

基本的に DevTools を使えばデバッグできるからです。

ターミナルでデバッグすると、DevTools には出力されないメッセージも見ることができます。

本記事では、Chrome をターミナルでデバッグする方法について紹介します。

## 結論: google-chrome --enable-logging=stderr --vmodule

結論ですが、Chrome をターミナルでデバッグするためのコマンドは以下のとおりです。

```shell
# on your console
google-chrome --enable-logging=stderr --vmodule
```

このコマンドを実行すると、Chrome が起動して、ターミナル上に大量のログが出力され始めます。

コマンドの意味を説明しますと、以下のような意味になります。

`--enable-logging=stderr` は stderr 標準エラー出力を有効にする、という意味です。

`--vmodule` は verbose logging on a per module basis という意味です

## References

参考文献は以下になります。

https://www.chromium.org/for-testers/enable-logging

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

