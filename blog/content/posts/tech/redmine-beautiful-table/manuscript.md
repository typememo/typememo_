---

title: Redmine 表の超絶見やすい書き方
slug: /tech/redmine-beautiful-table/
date: 2019-10-18
tags: [tech, redmine]
image: ./img/jo-szczepanska-5aiRb5f464A-unsplash.jpg
socialImage: ./img/jo-szczepanska-5aiRb5f464A-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Redmine 表の基本](#redmine-表の基本)
- [Redmine 表の超絶見やすい書き方 (本記事の主題)](#redmine-表の超絶見やすい書き方-本記事の主題)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

はじめに Redmine の表の基本について説明します。

その後に、本記事の主題である Redmine 表の超絶見やすい書き方を紹介します。

## Redmine 表の基本

[Redmine](http://redmine.jp) の表の書き方は `|` (縦棒と呼ぶ) で囲えばよいです。(とても簡単)

文字の配置やセルの結合などは以下の通りです。

* 表見出しは `|_. Head`
* 左寄せは `|<. item|`
* 右寄せは `|>. item|`
* センタリングは `|=. item|`
* 縦方向のセルの結合は `|/2 ...|`
* 横方向のセルの結合は `|\2 ...|`

具体的な書き方は [Redmine.JP Texttile 記法 テーブル](http://redmine.jp/tech_note/textile/) をご覧ください。

## Redmine 表の超絶見やすい書き方 (本記事の主題)

以下に示すように、縦棒 `|` を縦に並べて書くと、超絶見やすくかけます。

```textile
|_. Head1
|_. Head2
|_. Head3
|_. Head4
|
| item1
| item2
| item3
| item4
|
```

Redmine のプレビューでは整形された表が表示されます。

複数行にまたがる文章も素直に改行すればよいです。

一度、ご自身で試してみるとすぐにわかると思います：）

## おわりに

最後までお読み頂きありがとうございました：）

参考書: [Redmineによるタスクマネジメント実践技法 ](https://amzn.to/32pWPXf)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

