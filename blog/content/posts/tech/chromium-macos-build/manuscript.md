---

title: Chromium を MacOS でビルドしてみた
slug: /tech/chromium-build-macos/
date: 2020-07-23
tags: [tech, browser, chrome, chromium]
image: ./img/mitchell-luo-jz4ca36oJ_M-unsplash.jpg
socialImage: ./img/mitchell-luo-jz4ca36oJ_M-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [作業環境](#作業環境)
- [ビルドコマンド全文](#ビルドコマンド全文)
  - [asdf.sh を depot_tools のパス反映よりも後に実行してはならない](#asdfsh-を-depot_tools-のパス反映よりも後に実行してはならない)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

[Chromium](https://chromium.org/Home) を MacOS でビルドしてみました．

[Checking out and building Chromium for Mac](https://chromium.googlesource.com/chromium/src.git/+/HEAD/docs/mac_build_instructions.md) の内容に基づいています．

## 作業環境

筆者の作業環境は以下の通りです．

* [MacMini 10.15.4](https://amzn.to/2Ta3qmm)
* zsh 5.7.1 (x86_64-apple-darwin19.0.0)
* Python 2.7.16

## ビルドコマンド全文

以下のコマンドを実行すれば，chromium をビルドできます．

```bash
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.
echo -e 'export PATH="$PATH:/path/to/depot_tools"' >> $HOME/.zshrc
source $HOME/.zshrc
git config --global core.precomposeUnicode true
mkdir chromium && cd chromium
fetch chromium
cd src
gn gen out/Default
autoninja -C out/Default chrome

# ビルドが終わったら chromium を起動しましょう！
out/Default/Chromium.app/Contents/MacOS/Chromium
```

ビルドコマンドは上記の通りなのですが，ビルドが成功するまでに紆余曲折ありましたので，メモしておきます．

### asdf.sh を depot_tools のパス反映よりも後に実行してはならない

python や node.js のバージョンを asdf で管理しているのですが，これが原因で vpython 関連でエラーが発生する場合がありました．

`.zshrc` 内で asdf を有効にするために，MacOS では `. $HOME/.asdf/asdf.sh` を実行するのですが，これが `PATH=$PATH:$HOME/chromium/depot_tools` よりも後にあると，`vpython` 関連でエラーになりました．

`asdf shims` を実行する必要があるんじゃない？とコンソールログは教えてくれます．

asdf の有効化は `$PATH` の `export` よりも前に行なっておきましょう．

このエラーを解決するまでに約5日かかりました．

きっと，同じようなエラーで苦しんでいる人がいると思います．

その人がこの記事を見つけてくれることを切に願っています．

## おわりに

MacOS で chromium をビルドする方法についての備忘録でした．

chromium をビルドするコマンドは google が整備しているので完璧なのですが，その完璧に整備されたコマンドを実行する環境が整備されていないとドツボにハマります．

実際，筆者はドツボにハマりました．

MacOS に新規アカウントを作成して，まっさらな環境で chromium をビルドして，バイナリの実行だけは普段使っているアカウントから sudo コマンドでやってしまえ，とか．

いっそのこと，virtual box で linux 版の chromium で遊んでみようかなとか．

virtual box だともっさりしてるから，docker でビルドして，chromium をリモートデバッグして動作確認しようかな，とか．

とても面倒くさいことをしようとしていました．

asdf は便利なのですが，`.zshrc` 内で有効化する順番次第で諸々エラーが発生するのは，今後の良い教訓になりました．

ビルドできる環境になったので，chromium のソースコードをちょこちょこと改造して，遊んでみたいと思います．

何か面白いハックができたら，ブログ記事として投稿したいと思います．

最後までお読みいただきありがとうございました！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

