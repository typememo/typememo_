---

title: Gitlab イシューテンプレートの作り方 – 何でも自動化したい精神
slug: /tech/gitlab-issue-template/
date: 2020-10-27
tags: [tech, git, gitlab, template]
image: ./img/andrej-lisakov-3A4XZUopCJA-unsplash.jpg
socialImage: ./img/andrej-lisakov-3A4XZUopCJA-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [はじめに](#はじめに)
- [.gitlab ディレクトリを作成する](#gitlab-ディレクトリを作成する)
- [.gitlab/issue_templates/Hogehoge.md を作成する](#gitlabissue_templateshogehogemd-を作成する)
- [.gitlab/merge_request_templates/Hugahuga.md を作成する](#gitlabmerge_request_templateshugahugamd-を作成する)
- [Issue または Merge Request 作成時に Choose a template でテンプレートを選択する](#issue-または-merge-request-作成時に-choose-a-template-でテンプレートを選択する)
- [デフォルトテンプレート設定は Starter と Bronze 以上なら定義可能](#デフォルトテンプレート設定は-starter-と-bronze-以上なら定義可能)
- [おわりに](#おわりに)
  - [愛用品](#愛用品)

---

## はじめに

Gitlab のイシューテンプレート並びにマージリクエストテンプレートの設定方法についての紹介です．

イシューやマージリクエストに記載する内容を共通化しておくと便利です．

Gitlab は無料でテンプレート設定機能が実装されていますので，使った方が良いです．

本記事ではテンプレートの設定方法を手短に紹介します．

それでは参りましょう！

参考:
- [Description templates](https://docs.gitlab.com/ee/user/project/description_templates.html)  
本記事は `Description templates` を抜粋して日本語に意訳したものです．
英語を読むのに苦手意識がなければ本家を読むのが一番です．
- [GitLab実践ガイド](https://amzn.to/3osLtxs)  
書籍で Gitlab のことをマルッと知りたい方はこちらがおすすめです．

## .gitlab ディレクトリを作成する

まずは `.gitlab` という名前のディレクトリを作成してください．
作成する場所はリポジトリのトップディレクトリがおすすめです．

```shell
$ mkdir .gitlab
```

## .gitlab/issue_templates/Hogehoge.md を作成する

`.gitlab` ディレクトリ内に `issue_templates`
という名前のディレクトリを作成してください．

続いて `issus_templates` ディレクトリ内にテンプレートファイル (.md) を作成します．
このテンプレートファイルの名前が Gitlab の WebUI で表示される名前になります．
なので `Bug.md` とか `QnA.md` などの名前でテンプレートファイルを作成すると良いと思います．

次の具体例では `Hogehoge.md` という名前のテンプレートファイルを作成しています．

```shell
$ mkdir .gitlab/issue_templates
$ touch .gitlab/issue_templates/Hogehoge.md
```

テンプレートファイルの中身はお好きにどうぞ！
良いテンプレートの例は `バグチケット テンプレート` とかググれば出てきますので，
気に入ったものを参考にしてテンプレートファイルの中身を埋めると良いと思います．

筆者の場合は次のコマンドで
[Article.md](https://gitlab.com/typememo/typememo_/-/blob/master/.gitlab/issue_templates/Article.md)
というイシューテンプレートを作成しました．

```shell
$ code .gitlab/issue_templates/Article.md
```

## .gitlab/merge_request_templates/Hugahuga.md を作成する

イシューテンプレートと同様の手順でマージリクエストのテンプレートも作成できます．

`.gitlab` ディレクトリ内に `merge_request_templates` ディレクトリを作成して，
その中にテンプレートファイル (.md) を作成すれば良いです．
例えば，次の具体例のように．

```shell
$ mkdir .gitlab/merge_request_templates 
$ touch .gitlab/merge_request_templates/Hugahuga.md
```

## Issue または Merge Request 作成時に Choose a template でテンプレートを選択する

イシューやマージリクエストのテンプレートファイルをリポジトリのメインブランチに
`push` しましたか？

もしも `push` していればイシューやマージリクエストを作成するときに
`Choose a template` で作成したテンプレートが選択可能になっていると思います．

![Gitlab Choose a template](img/Screenshot_2020-10-25_16.38.45.png)

この画像ではご自身のテンプレートは表示されていませんが，メインブランチにテンプレートファイルを
`push` している状態であればテンプレートが選択可能になっていますのでご安心を．

## デフォルトテンプレート設定は Starter と Bronze 以上なら定義可能

2020.10.27 時点の情報ですがデフォルトテンプレートの設定は
`Starter` もしくは `Bronze` 以上のユーザーなら設定可能だそうです．

## おわりに

Gitlab のイシューテンプレート並びにマージリクエストテンプレートの設定方法についての紹介でした．

> 簡単にできそうだから使ってみよう

と思っていただけたら嬉しいです．

そして実際に使ってみてくれたらもっと嬉しいです．

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．
