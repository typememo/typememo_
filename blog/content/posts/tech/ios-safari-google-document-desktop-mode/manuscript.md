---

title: ios13 Safari Google Document のデスクトップ版を表示する方法
slug: /tech/ios-safari-google-document-desktop-mode/
date: 2019-10-03
tags: [tech, ios]
image: ./img/ben-kolde-xdLXPic3Wfk-unsplash.jpg
socialImage: ./img/ben-kolde-xdLXPic3Wfk-unsplash.jpg
imageAlt: eyecatch

---

## 設定方法

- まずはダウンロードしているアプリ版の Google Document を削除します。
- 続いて Safari で Google Document を開きます。
- デスクトップ版の Google Document が開けない場合は、一度ログインし直すと良いです。
- ログインし直したら、Safari で デスクトップ版の Google Document が開けると思います。

良き文書作成の日々をお過ごしください :)

最後までお読みいただき、ありがとうございました :)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

