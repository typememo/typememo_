---

title: Bracket Pair Colorizer で "括弧どこだっけ？" 時間がゼロになるぞ
slug: /tech/vscode-bracket-pair-colorizer/
date: 2020-10-24
tags: [tech, vscode]
image: ./img/benoit-gauzere-AZ48BffFJz0-unsplash.jpg
socialImage: ./img/benoit-gauzere-AZ48BffFJz0-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [Bracket Pair Colorizer 2 マジで優秀](#bracket-pair-colorizer-2-マジで優秀)
- [おわりに](#おわりに)
  - [愛用品](#愛用品)

---

## Bracket Pair Colorizer 2 マジで優秀

小ネタです．

vscode にて
[Bracket Pair Colorizer 2](https://github.com/CoenraadS/Bracket-Pair-Colorizer-2/tree/master)
という拡張機能をインストールすると  

> 括弧はどこだー？

という括弧を探す無意味な時間がゼロになるぞ，という報告記事です．

実際の動作はこんな感じです．

<video
  src="./video/Screenshot_2020-10-24_10.50.51.mov"
  autoplay
  loop>
  Demo - Bracket Pair Colorizer 2
</video>

括弧にカーソルが移動すると，その括弧のペアとなる括弧の位置がハイライトされます．

このハイライトされた線を辿っていけば対応する括弧の位置を把握することができます．

ネストが激しいソースコードを目の当たりにしても，もう大丈夫ですね．

// まあ，そもそもネストが激しいソースコードを書くなよ，って話ですけど

## おわりに

Bracket Pair Colorizer 2 が本当に優秀で感動したので，小ネタとして記事を書きました．

"この拡張機能使えそうじゃん！" と思っていただけたらぜひインストールして使ってみてください．

開発をしている
[@CoenraadS](https://github.com/CoenraadS)
にこの場を借りて御礼申し上げます．

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
- [HHKB Professional](https://amzn.to/3dGtLS1)
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)
- [Apple MacMini](https://amzn.to/34jvsSt)
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)
