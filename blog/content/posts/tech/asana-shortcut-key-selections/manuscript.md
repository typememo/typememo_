---

title: Asana よく使うショートカットキー
slug: /tech/asana-shortcut-key-selections/
date: 2020-08-05
tags: [tech, asana]
image: ./img/indian-yogi-yogi-madhav-5L8c1y40RZA-unsplash.jpg
socialImage: ./img/indian-yogi-yogi-madhav-5L8c1y40RZA-unsplash.jpg
imageAlt: eyecatch

---

Contents:

<!-- TOC -->

- [はじめに](#はじめに)
- [Asana ショートカットキーチートシート](#asana-ショートカットキーチートシート)
- [Asana よく使うショートカットキー](#asana-よく使うショートカットキー)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

Asana はシンプルな UI でタスク管理ができる Web アプリケーションです。

本記事の対象者は、すでに Asana を使っているが、Asana のキーボード・ショートカットキーを知りたい人です。

本記事では、まず最初に Asana のキーボードショートカットキーのチートシートを提示します。
その次に、よく使うショートカットキーを9つ紹介します。

## Asana ショートカットキーチートシート

Asana のキーボード・ショートカットキーのチートシートは以下の通りです。

![FigureAsanaKeyboardShortcutSheet](./img/Screenshot_2020-01-26_21.19.43.png)

MacOS で Chrome を使用している場合は、`Command + /` でチートシートを表示できます。
(OS 及びブラウザに依存するので、適宜ご自身の環境に置き換えてください)

次のセクションでは、このチートシートの中でよく使うショートカットキーを紹介していきます。

## Asana よく使うショートカットキー

Asana は基本的に Tab キーを起点にしてショートカットキーを実行します。

以下が、よく使う9つのショートカットキーです。

0. Tab + Q
タスクをクイック追加する。
Quick task の Q ですね。
0. Tab + Enter
タスクの詳細を表示する
Enter 押してファイルを開くのと同じ感じですね。
0. Tab + D
タスクの期日を設定する
Date の D ですね。
0. Tab + A
タスクのアサイン先を設定する
Assigned の A ですね。
0. Tab + C
タスクにコメントを追加する
Comment の C ですね。
0. Tab + R
タスクの説明を表示する
これはなんの略か分かりません。笑
0. Tab + /
プロジェクト、タスク、人、会話またはタグを検索する
vi とかで検索するときと同じ感じですね。
0. Tab + P
プロジェクトにタスクを追加する
Project task の P ですね。
0. Tab + S
サブタスクを追加する
Sub task の S ですね。

## おわりに

ショートカットキーなんて覚えられない、という人も大丈夫です、
何回か打っているうちに身体が覚えます。笑

最初は慣れなくても、ショートカットキーを打ち続けてみましょう。

一度身に付くと Asana での作業がかなり楽になるかと思います。

最後までお読みいただきありがとうございました．

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

