---

title: 達人プログラマーが使っているツールとは?
slug: /tech/pragmatic-programmer-chapter3/
date: 2020-10-15
tags: [tech, book, pragmatic-programmer]
image: ./img/clayton-robbins-GNMaYHU3N9A-unsplash.jpg
socialImage: ./img/clayton-robbins-GNMaYHU3N9A-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [達人プログラマーが使っているツールまとめ](#達人プログラマーが使っているツールまとめ)
- [筆者コメント](#筆者コメント)
- [おわりに](#おわりに)


---


## 達人プログラマーが使っているツールまとめ

[達人プログラマー 第3章](https://amzn.to/3dyvSaj)
の筆者なりのまとめです．

- プレインテキスト
- シェル
- エディタ
- ソースコード管理システム (Git)
- デバッガ
- テキスト操作言語 (awk, sed, Perl, Python)
- コードジェネレータ


## 筆者コメント

- 全てをプレインテキストで管理して同じエディタを使うこと．
- 一つのエディタを徹底的に極めること．
- 一つのデバッガに精通すること．
- コードジェネレータは生成物の更新機能を備えたモノを積極的に使うこと．
- シェルとテキスト操作言語を使って爆速で作業を終わらせること．


## おわりに

達人プログラマーが使っているツールはどれも普段使っているツールと変わりないですね．

一つ特徴的な点としては `コードジェネレータ` でしょうか？

「`コードジェネレータ` は更新機能を備えておくとより便利になるぞ」
という視点は良い刺激になりました．

とにかく達人プログラマーに近づくためには一つのことを極める必要があるようです．

日々使っているからこそ意識して便利機能がないか探してみないとですね．

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

