---

title: idl 比較演算子まとめ
slug: /tech/idl-comparison-operator/
date: 2019-08-17
tags: [tech, idl]
image: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
socialImage: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
imageAlt: eyecatch

---

## 比較演算子一覧表

idl で主に使われる比較演算子は以下の表の通りです．

| 比較演算子 | 意味 |
| :-- | :-- |
| eq | 等しい ( equal ) |
| ne | 等しくない ( not equal ) |
| le | 以下 ( less equal ) |
| ge | 以上 ( greater equal ) |
| lt | より小さい ( less than ) |
| gt | より大きい ( greater than ) |

[if 文](https://typememo.jp/tech/idl-if-else-for/) の条件式は以上の比較演算子でコントロールしてください。

最後までお読みいただき、ありがとうございました：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

