---

title: idl plot の使い方
slug: /tech/idl-plot/
date: 2019-09-09
tags: [tech, idl]
image: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
socialImage: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [まずは plot してみる](#まずは-plot-してみる)
- [綺麗目に plot する](#綺麗目に-plot-する)
  - [軸](#軸)
  - [タイトル](#タイトル)
  - [線](#線)
  - [シンボル](#シンボル)
  - [フォント](#フォント)
- [おわりに & 参考資料](#おわりに--参考資料)

<!-- /TOC -->

----

## はじめに

IDL の plot の使い方についてご紹介します。

plot の基本的な文法は以下の通りです。

```python
plt = plot(x, y, /buffer)
```

`x` , `y` はプロットする配列で、要素数は等しくなければなりません。
要素数が等しくない場合はプロットできないので注意してください。

`/buffer` はプロットした図を画面に表示させないということを意味しています。
プロットした図をその場で確認したい方は、 `/buffer` を削除してください。

## まずは plot してみる

基本的な文法を抑えたので、まずは plot してみましょう。

例として、2019-09-01 (日) 東京の気温をプロットします。

気温データは気象庁の[過去の気象データ・ダウンロード](https://www.data.jma.go.jp/gmd/risk/obsdl/index.php)からダウンロードしました。

気象庁の皆さま、貴重なデータの取得ありがとうございます：）

以下のプログラムを拡張子 .pro のファイルとして IDL のパスが通っているフォルダに保存してください。

`dindgen(配列の要素数, start = 配列の初期値, increment = 配列の間隔)`でdouble 型の配列を作成しています。

例えば、要素数10個の奇数配列を作成したければ、`oddArray = dindgen(10, start = 1, increment = 2)`とすれば良いです。

気温はベタ打ちで汎用性がなくてごめんなさい。

気象庁からダウンロードしたデータは .csv ファイルなので本来なら`readcsv()`を使ってデータを読み込むべきです。

`plt.save, saveName` でプロットした図を保存しています。

今回は `saveName` を `saveDirName + saveFileName`に分けて書いていますが、分けずに書いてもOKです。

個人的には分けて書くことをお勧めします。

複数の図を別名で同じディレクトリに保存する時に、ディレクトリとファイル名を分けて書いておいたほうが分かりやすいと思いますので。

```python
pro sample_plot1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; x [1, 24] h の範囲で、
; y 2019-09-01 (日) 東京の気温を plot する
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

saveDirName = '~/Downloads/'
saveFileName = 'figure_sample_plot1.png'

hour = dindgen(24, start = 1, increment = 1)
temperature = [$
    26.1, 25.7, 25.5, 25.5, 25.4, 25.7,$
    25.9, 26.6, 28.8, 29.3, 30.3, 29.6,$
    30.6, 31.5, 28.9, 30.4, 29.6, 29.2,$
    28.4, 27.6, 27.1, 26.2, 25.7, 25.2]

plt = plot(hour, temperature, /buffer)
plt.save, saveDirName + saveFileName

end
```

このプログラムを実行すると、ダウンロードフォルダに以下に示すような **figure_sample_code1.png** という画像ファイルが保存されます。

![figure_sample_code1](./img/figure_sample_plot1.png)

この図には x 軸および y 軸のタイトルがないので、この図が何を表しているのかさっぱり分かりません。

研究室の進捗報告会で上の図を出されて「横軸が 1~24 で 縦軸が 25~35 とかだから、これは１日の気温変化の図だな。」と深読みしてわかってくれる優しい同期・先輩・教員は滅多にいません。

パッと見で理解できないような不親切な図を描くのはもう終わりにしましょう。

以下では、

- 軸タイトル
- 軸範囲
- 軸の目盛り
- 図のタイトル
- 線の種類
- 線の色
- 線の太さ
- データ点のシンボルの種類
- データ点のシンボルの大きさ
- タイトルフォントの大きさ
- タイトルフォントの種類

などに注意を払った綺麗目な plot を紹介します。

## 綺麗目に plot する

綺麗目に plot した図と、この図を出力するためのソースコードを示します。

![figure_sample_code2](./img/figure_sample_plot2-1.png)

```python
pro sample_plot2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; x [1, 24] h の範囲で、
; y 2019-09-01 (日) 東京の気温を plot する
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

saveDirName = '~/Downloads/'
saveFileName = 'figure_sample_plot2.png'

hour = dindgen(24, start = 1, increment = 1)
temperature = [$
    26.1, 25.7, 25.5, 25.5, 25.4, 25.7,$
    25.9, 26.6, 28.8, 29.3, 30.3, 29.6,$
    30.6, 31.5, 28.9, 30.4, 29.6, 29.2,$
    28.4, 27.6, 27.1, 26.2, 25.7, 25.2]

plt = plot(hour, temperature,$
    ; x 
    xtitle = 'Hour [ JST ]',$
    xrange = [0, 25],$
    xtickvalues = [0, 6, 12, 18, 24],$
    xtickname = ['0 h', '6 h', '12 h', '18 h', '24 h'],$
    xminor = 5,$
    ; y
    ytitle = 'Temperature [$ ^{\circ}C $]',$
    yrange = [20, 35],$
    ytickvalues = [20, 25, 30, 35],$
    ytickname = ytickvalues,$
    yminor = 4,$
    ; title
    title = '2019-09-01 (Sun) @Tokyo',$
    ; linestyle
    linestyle = 2,$ ; dashed line
    thick = 2,$
    color = 'black',$
    ; symbol
    symbol = 'o',$
    sym_size = 1.0,$
    sym_color = 'black',$
    sym_filled = 1,$
    ; font
    font_size = 14,$
    font_name = 'Helvetica',$
    ; option
    /buffer)
plt.save, saveDirName + saveFileName

end
```

以下で、ソースコードを解説します。

### 軸

`xtitle` `ytitle` で軸のタイトルを定義しています。

シングルクオーテーション ( ' ) 、もしくはダブルクオーテーション ( " ) で囲ってください。

ドルマーク ( $ ) で囲えば LaTeX 風に特殊文字を表示できます。

`xrange` `yrange` で軸の範囲を [x1, x2] [y1, y2] のように定義します。

定義しなければ、IDL 側が自動的に範囲を指定します。

`xtickvalues` `ytickvalues` で軸のどの箇所に目盛り名を表示するか、数字の1次元配列として定義します。

今回の例では、0 h ( 真夜中 ) , 6 h (  明け方 ) , 12 h ( 正午 ) , 18 h ( 日暮れ ) , 24 h ( 真夜中 ) のように時間の間隔を出来るだけ分かりやすく定義しています。

なお、どの箇所に目盛り名は表示するか自由に定義できます。

どの箇所に目盛り名を表示するか定義したら、そこに表示する目盛り名を定義しましょう。

`xtickname` `ytickname` で軸の目盛り名を、文字列の1次元配列として定義します。

今回の例では、1時間毎の観測なので x 軸に 「 数字 h 」 という目盛り名を定義しました。

`tickvalues` と `tickname` が同じ場合は、`[xy]tickname = [xy]tickvalues` のようにしておけば良いです。

`tickname` を有効に使うことで物理量の単位が把握しやすくなるので、まだ使っていない人は是非使ってみてほしいです。

`xminor` `yminor` で軸の補助目盛りの間隔を定義しています。

軸の補助目盛りを適切に設定することで、より分かりやすい図が描けます。

### タイトル

`title` で図のタイトルを定義しています。

個人的には図のタイトルは補足情報を載せるところだと考えています。

積極的に使う必要はないのではないかと思います。

### 線

`linestyle` で線の種類を定義しています。

以下に定義可能な線の種類をまとめました。

| 線の種類の定義 | 線の種類 |
| :-- | :-- |
| `linestyle = 0` | 実線 |
| `linestyle = 1` | 点線 |
| `linestyle = 2` | 破線 |
| `linestyle = 3` | 一点鎖線 |
| `linestyle = 4` | 二点鎖線 |
| `linestyle = 5` | 超破線 |
| `linestyle = 6` | 線なし |

`thick` で線の太さを定義しています。

デフォルトでは `thick = 1` です。

適宜、その状況に応じて線の太さを変更すると良いです。

`color` で線の色を定義しています。

相手にとって視覚的に分かりやすい色を適宜、定義するとよいです。

個人的には、白黒印刷のことも考えて、線の色ではなく、線の種類を使い分けることをお勧めします。

### シンボル

`symbol` でデータ点のシンボルの形状を定義しています。
形状は様々あるので、視覚的に分かりやすい形状を定義すると良いです。

`sym_size` でデータ点のシンボルの大きさを定義しています。
見やすい大きさとなるように適切に定義すると良いです。

`sym_color` でデータ点のシンボルの色を定義しています。
デフォルトでは `color` の色と同じです。

`sym_filled = 0 or 1` でデータ点のシンボルを塗り潰さないか、それとも塗りつぶすか定義しています。

### フォント

`font_size` で図のタイトル、軸、目盛りなどの文字列のフォントサイズを定義しています。

スライドに載せる時などは、`font_size = 16` などにして、会場の一番後ろに座っている人でも難無く見えるように工夫すると良いです。

`font_name` でフォント名を定義しています。
デフォルトでは確か `Courier` となっているはずですが、個人的に `Helvetica` が見やすくて好きなのでお勧めしています。


## おわりに & 参考資料

plot の基本的な内容を紹介しました。

この先には `/overplot` による重ね書き、`errorplot` による誤差棒付きのプロット、などの応用編が待ち構えています。

応用編についても記事を書いていますので、公開するまで少々お待ちください。

本記事は下記の資料を参考にして作成しました。

* [plot() ](https://www.harrisgeospatial.com/docs/PLOT.html)

お勧め参考書は以下の書籍です。
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=typememo-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B07L2W3LQ4&linkId=594edb4cb61c9176a962d727bb57a802"></iframe>

最後までお読みいただき、ありがとうございました：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

