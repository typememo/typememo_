---

title: ないから作った HHKB Type-S 墨
slug: /tech/hhkb-types-black-handmade/
date: 2020-08-07
tags: [tech, hhkb]
image: ./img/FigureHHKBType-SBlackOverview-scaled.jpg
socialImage: ./img/FigureHHKBType-SBlackOverview-scaled.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [自作 HHKB Type-S 墨](#自作-hhkb-type-s-墨)
- [作り方](#作り方)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

HHKB いいですよね：）

HHKB Type-S、タイピング音が静かで重めの打鍵感でいいですよね：）

HHKB Pro2 墨、真っ黒なあのイケてるデザインいいですよね：）

でも，HHKB Typs-S 墨、ないんですよね。。。

----

**2020.01.25 追記:**
[Happy Hacking Keyboard Professional HYBRID Type-S 英語配列／墨](https://amzn.to/37qpm1t)が販売されていました！

一体いつ、こんな素晴らしいキーボードが販売されていたんだ。。。

Bluetooth 機能を搭載した、Type-S って HHKB 愛好者が待ち望んでいたものですね。

いいなぁ、欲しいなぁ。

でも、Type-S 墨 を自作したので買いませんが。笑

----

HHKB Type-S 墨 あったら最高ですよね：）

ないなら、作ってしまいましょう：）

## 自作 HHKB Type-S 墨

ということで、[HHKB Typs-S 白](https://amzn.to/30BSuSD) を塗装して、HHKB Type-S 墨を作りました！

それがこちら！

![HHKBType-SBlackOverview](./img/FigureHHKBType-SBlackOverview-scaled.jpg)

いい感じに黒いですね：）

本家の墨色に近い艶消しされた黒でいい感じですね：）

ちなみで近くで見ると、こんな感じです。

![HHKBType-SBlackFocus](./img/FigureHHKBType-SBlackFocus-scaled.jpg)

スプレー塗装は初めてだったので、液垂れしてしまいました。
まあ、でも、手作り感があって個人的には好きですね。

## 作り方

キーボードの分解は[こちらの記事](https://keyboard.tokyo/hyohaku/)を参考にしました。

墨色に塗装するためのスプレーは[アサヒペン 高耐久ラッカースプレー 300ML ツヤ消し黒](https://amzn.to/2NT9qwT)を使いました。

塗装の方法は、上段と下段のフレームをそれぞれ大きめの紙袋に入れ、スプレー塗装しては１時間待ち、スプレー塗装しては１時間待ち、を繰り返しました。

さらに完全に乾燥させるために丸一日ベランダに放置しました。

HHKB を塗装する人は専用の塗装スペースを作るそうですが、自分は紙袋を塗装スペースにすることで単純化しました。

最後に、バラバラになった部品を組み立て直して完成です。

組み立て直すときの注意点ですが、キートップを支えている部品についているゴムリングを無くさない様に気をつけましょう。
自分はゴムリングを一つ無くしてしまい、`6` キーが引っかかりやすくなってしまいました。

慎重に作業をすれば何も問題ありませんのでご安心を。
まぁ、言うても、全ては自己責任ですが。

## おわりに

HHKB Type-S 墨 を作ってみた、という内容の記事でした。

この記事は塗装が終わった HHKB Type-S 墨 で執筆しました。

HHKB Type-S 墨 は販売されていないので、
どうしても欲しい人は自分で作るしかありません。

ないから諦めるのではなく、ないなら作ってしまいましょう：）

最後までお読みいただき、ありがとうございました！

> Note:
> 本記事の内容を元に作業した結果、キーボードに不具合が生じても，筆者は一切の責任を負いませんのでご了承くださいませ。
> 全ては自己責任です。お気をつけて。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

