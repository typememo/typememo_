---

title: Crowiのはじめ方、教えます！
slug: /tech/crowi-getting-started/
date: 2018-08-02
tags: [tech, crowi]
image: ./img/you-x-ventures-6awfTPLGaCE-unsplash.jpg
socialImage: ./img/you-x-ventures-6awfTPLGaCE-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Crowi のホームページにアクセスする](#crowi-のホームページにアクセスする)
- [See Demo Here をクリックする](#see-demo-here-をクリックする)
- [新規登録はコチラをクリックする](#新規登録はコチラをクリックする)
- [ユーザー情報を入力する](#ユーザー情報を入力する)
- [おわりに](#おわりに)

<!-- /TOC -->

---

## はじめに

本ページに足を運んでくださり、ありがとうございます。

今回は、「Crowiのはじめ方」についてご紹介します。

はじめ方は非常にシンプルで、以下の４ステップで始められます。

それでは各ステップでどのような作業をすればよいか見てみましょう！

## Crowi のホームページにアクセスする

まずは、[Crowiのホームページ](http://site.crowi.wiki/) にアクセスしましょう．

## See Demo Here をクリックする

ヘッダー画像のすぐ下に書いてある、[SEE DEMO HERE](http://demo.crowi.wiki) をクリックしてください。

## 新規登録はコチラをクリックする

表示された画面中央の [新規登録はこちら](http://demo.crowi.wiki/login#register) をクリックしてください。


## ユーザー情報を入力する

次の項目を入力してください．

- ユーザーID
- 名前
- メールアドレス
- パスワード

入力が終わったら **新規登録** をクリックしてください。

## おわりに

ようこそ Crowi へ！
ガンガン Crowi を使い倒して行きましょう！

僕も Crowi を始めたばかりなのでわからないことばかりですが、便利な使い方などわかったら記事にしたいと思います。

最後までお読みいただきありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

