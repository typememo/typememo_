---

title: Asana ダークモード (dark-mode) の設定方法
slug: /tech/asana-dark-mode/
date: 2020-08-07
tags: [tech, asana]
image: ./img/steve-harvey-U4wcrDteZ2Y-unsplash.jpg
socialImage: ./img/steve-harvey-U4wcrDteZ2Y-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Stylish 拡張機能をインストールする](#stylish-拡張機能をインストールする)
- [Asana Darkness テーマをインストールする](#asana-darkness-テーマをインストールする)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

Asana はタスク管理ができる Web アプリケーションです。

デフォルトの UI は白を基調としたものとなっています。

ですが、基本色が白だと、目が痛くなってしまうんですよね。

そこで、asana にダークモードがあるのか探してみたんですが、デフォルトではいい感じのものがないんですよね。

`プロフィール設定` -> `表示` -> `スプーギー` にすれば基本色が黒っぽくなります。
でも、装飾多めで個人的にはいまいちなんですよね。

そこで、[Stylish](https://chrome.google.com/webstore/detail/stylish-custom-themes-for/fjnbnpbmkenffdnngjfgmeleoegfcffe?hl=en-US) という拡張機能 (chrome firefox 対応なことは確認済み) を使って、Asana の UI をシンプルなダークモードに設定することにしました。

それが、こんな感じ！

![AsanaDarkMode](./img/Screenshot_2020-01-25_at_11.35.11.png)

目に優しい色で、いい感じですよね：）

> Note:
> タスク名などは都合上、黒く塗りつぶしています。

本記事では、この Asana ダークモードの設定方法について紹介します。

## Stylish 拡張機能をインストールする

まずは chrome もしくは firefox を起動してください。

ブラウザを起動したら [Stylish](https://chrome.google.com/webstore/detail/stylish-custom-themes-for/fjnbnpbmkenffdnngjfgmeleoegfcffe?hl=en-US) をクリックして、Stylish をインストールしてください。

Stylish は特定の Web ページの UI を用意されているテーマに変更できる拡張機能です。

![StylishOnChrome](./img/Screenshot_2020-01-25_at_11.24.20.png)

インストールすると Stylish の説明ページが表示されますが、特に読む必要はないので閉じて結構です。

次は Asana 用のダークテーマ Asana Darkness をインストールしましょう。

## Asana Darkness テーマをインストールする

[Asana Darkness インストールページはこちら](https://userstyles.org/styles/150563/asana-darkness) をクリックすれば、Asana Darkness のインストールページに移動します。

インストール をクリックすればインストール完了です。

![StylishAsanaDarkness](./img/Screenshot_2020-01-25_at_11.25.47.png)

Asana を開いているタブに移動して、更新すると UI が変わっているはずです。

Asana Darkness 以外のテーマを試してみたい、という方は Stylish を起動して、検索ボックスに Asana と入力してお好きなテーマをインストールしてください。

## おわりに

Asana をダークテーマに設定する方法の紹介でした。

暗い部屋でも目に優しいダークテーマ、良いですね。

Asana Darkness は黒と緑を基調としたテーマなので、プロジェクトカラーを緑に設定すると、より統一感が出て綺麗だと思います．

ちなみに Asana を使っている人で、無料のガントチャートも使いたい人がいれば、[Asana と Instagantt を連携させて無料で高機能なガントチャートを使う方法](https://typememo.com/2019/10/instagantt-with-asana-introduction/)も合わせてお読みいただければと思います。

最後までお読みいただきありがとうございました．
### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

