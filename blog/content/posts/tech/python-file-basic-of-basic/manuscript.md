---

title: Python ファイル処理の基本
slug: /tech/python-file-read-write-basic/
date: 2019-12-09
tags: [tech, python]
image: ./img/iconfinder_python_308445.png
socialImage: ./img/iconfinder_python_308445.png
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [ファイルパス取得 => `os` モジュール](#ファイルパス取得--os-モジュール)
- [ファイル書き出し](#ファイル書き出し)
- [ファイル読み込み](#ファイル読み込み)
- [csv ファイルの処理 => `csv` モジュール](#csv-ファイルの処理--csv-モジュール)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

[独学プログラマー](https://amzn.to/2DZugFI) 第９章のファイル処理の自分用メモです。

継続してアウトプットするためのメモなので、間違いなどがあるかもしれません。

その点、ご了承くださいませ。

## ファイルパス取得 => `os` モジュール

ファイルパスを取得するなら os モジュールを使うべし。

手入力だと、OS の環境が異なるとファイルパスの書き方が異なるため。

この `os` モジュールの使い方には感動した。初めてみたので。

```python
os.path.join()

# for example
os.path.join("Users", "bob", "st.txt")
>> '/Users/bod/st.txt'
```

## ファイル書き出し

ファイル書き出しの手順は、

- `open()`
- `write()`
- `close()`

である。

```python
file = open("Path/to/File", "w", encoding = "utf-8")
file.write("Something you want to write")
file.close()

# もしくは with 文を使うと close() が省略できる。
with open("Path/to/File", "w", encoding = "utf-8") as file
    file.write("Something you want to write")
```

## ファイル読み込み

ファイル読み込みの手順は、

- `open()`
- `read()`
- `close()`

である。

```python
file = open("Path/to/File", "w", encoding = "utf-8")
contentOfFile = file.read()
file.close()

# with 文を使うと close() を省略できる。
contentOfFile = []
with open("Path/to/File", "w", encoding = "utf-8") as file
    contentOfFile.append(file.read())
```

## csv ファイルの処理 => `csv` モジュール

csv (comma separated value) ファイルの取り扱いには、csv モジュールを使うと良い。

書き込みと読み込みの始まりは、

- `csv.writer()`
- `csv.reader()`

である。

```python
# 書き込みの場合
with open("Path/to/File", "w", encoding = "utf-8") as file
    csvWriteFile = csv.writer(file, delimeter = ",")
    csvWriteFile.writerow(["a", "b", "c"])
    csvWriteFile.writerow(["d", "e", "f"])

# 読み込みの場合
with open("Path/to/File", "w", encoding = "utf-8") as file
    csvReadFile = csv.reader(file, delimeter = ",")
    for line in csvReadFile:
        print(",".join(line))
```

## おわりに

本記事の内容はすべて、[独学プログラマー](https://amzn.to/2DZugFI) に則っています。

最後までお読みいただき、ありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

