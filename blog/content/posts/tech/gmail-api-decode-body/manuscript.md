---

title: Gmail API で取得したメール本文をデコード処理する方法 – Python 編
slug: /tech/gmail-api-decode-body/
date: 2020-10-11
tags: [tech, gmail, python]
image: ./img/kon-karampelas-N82naZ0N4TY-unsplash.jpg
socialImage: ./img/kon-karampelas-N82naZ0N4TY-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [はじめに](#はじめに)
- [メール本文を探す](#メール本文を探す)
- [メール本文をデコードする](#メール本文をデコードする)
- [おわりに](#おわりに)


---


## はじめに

Gmail API を叩いて取得したメール本文をデコードする方法を紹介します．

"Gmail API を叩いてメールを取得することはできたのだけどメール本文を取得できずに困っている" そんな人にぜひとも読んでもらいたい記事です．

メール本文を取得するには次の２ステップが必要です．
1. メール本文を探す  
=> `message["payload"]["body"]["data"]` の値がメール本文である
1. メール本文をデコードする  
=> `gmail.decode(encoded)` で base64 形式でデコードする

それぞれ説明しますが，すでに何かしらのメールメッセージを取得している状態を前提として説明します．
まだメールメッセージを取得していない方は [#gmail](https://typememo.jp/tags/gmail/) の関連記事をお読みください．

参考書籍:
- [退屈なことはPythonにやらせよう](https://amzn.to/3iP4OVo)
- [入門 Python 3](https://amzn.to/3dhDzBx)


## メール本文を探す

取得したメールメッセージは次のような json 形式で定義されていると思います．([REST Resource: users.messages](https://developers.google.com/gmail/api/reference/rest/v1/users.messages))

```json
{
  "id": string,
  "threadId": string,
  "labelIds": [
    string
  ],
  "snippet": string,
  "historyId": string,
  "internalDate": string,
  "payload": {
    object (MessagePart)
  },
  "sizeEstimate": integer,
  "raw": string
}
```

この中の `MessagePart` は次のような json 形式になっています．([MessagePart](https://developers.google.com/gmail/api/reference/rest/v1/users.messages#messagepart))

```json
{
  "partId": string,
  "mimeType": string,
  "filename": string,
  "headers": [
    {
      object (Header)
    }
  ],
  "body": {
    object (MessagePartBody)
  },
  "parts": [
    {
      object (MessagePart)
    }
  ]
}
```

そしてお目当てのメール本文は `MessagePartBody` に次のような json 形式で定義されています．([Resource: MessagePartBody](https://developers.google.com/gmail/api/reference/rest/v1/users.messages.attachments#resource:-messagepartbody))

```json
{
  "attachmentId": string,
  "size": integer,
  "data": string
}
```

この json 形式の `data` キーに紐つく値が探していたメール本文です．
この `data` の値を出力させてみればわかりますが base64 形式でエンコードされた文字列となっています．

メール本文を json から抽出する方法は `message["payload"]["body"]["data"]` にアクセスできれば何でも良いです．
筆者は自作関数 `get_message_body(message)` を使ってメール本文を一行で抽出しています．

See https://gitlab.com/typememo/typememo_/-/blob/master/tools/gmail.py

```python

def get_message_body(message):
  """Get message body."""
  part = get_message_part(message)
  body = part["body"]["data"]
  return body

def get_message_part(message):
  """Get message part."""
  message_part = message["payload"]
  return message_part
```


## メール本文をデコードする

ということでめでたくメール本文を見つけることができました．
しかも base64 形式でエンコードされている文字列であるということもわかりました．

メール本文を base64 形式でデコードしてあげれば人が読める形式になります．
`base64` ライブラリを使えば簡単にデコードすることができます．

筆者は `base64` ライブラリを使った `decode(encoded)` という関数をメール取得関数などと同じファイルに記述しています．
`gmail.decode(encoded)` と書けば `base64` 形式でデコードされるようになっているので扱いやすいです．

See https://gitlab.com/typememo/typememo_/-/blob/master/tools/gmail.py

```python
import base64

def decode(encoded):
  """Decode message body."""
  decoded = base64.urlsafe_b64decode(encoded).decode()
  return decoded
```


## おわりに

Gmail API を叩いてメール本文を取得してデコードする方法についての解説記事でした．

参考になりましたでしょうか？

何か不明点などあれば Gitlab のイシューに書いていただけるとありがたいです．

この記事を気に入っていただけましたら，twitter などで `#typememo` とハッシュタグをつけて呟いていただけるととても嬉しいです．

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

