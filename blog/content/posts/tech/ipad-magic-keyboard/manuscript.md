---

title: iPad Pro 用の Magic Keyboard が販売されててビビった話
slug: /tech/ipad-magic-keyboard
date: 2020-08-01
tags: [tech, ipad]
image: ./img/sebastian-willius-POS-pi8bcjw-unsplash.jpg
socialImage: ./img/sebastian-willius-POS-pi8bcjw-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [草稿](#草稿)
- [追記](#追記)

<!-- /TOC -->

## 草稿

持ち物減らしたいなぁ、でもブログ書くのはキーボードの方が楽なんだよなぁ。

iPad で使えるいいキーボードないかなぁ、と思い apple.com 覗いたら、なんと！

**[Magic Keyboard for iPad Pro](https://www.apple.com/jp/shop/product/MXQT2J/A/11%E3%82%A4%E3%83%B3%E3%83%81ipad-pro%E7%AC%AC2%E4%B8%96%E4%BB%A3%E7%94%A8magic-keyboard-%E6%97%A5%E6%9C%AC%E8%AA%9Ejis) なんてモノが2020年5月販売だと！？**

**しかも Trackpad がついてて iPad の画面角度も自由自在に変えられるフローティングデザインのキーボードだと！？**

**もはや、Macbook じゃないか！**

Macbook が Mac のラインナップからいなくなったのは、iPad Pro が Macbook の代わりになるからだったのかな。

いずれにせよ、iPad Pro をお持ちの方で、PC は重いから持ち歩きたくないなぁって思ってる人にとっては吉報でしょう。

筆者は対応する iPad Pro 持っていませんので体験できませんが。

くぅぅ、欲しいなぁ。

## 追記

amazon で売られてますねぇ．[Magic Keyboard for iPad](https://amzn.to/2EFFwL1)

くぅぅ，やっぱり気になるなぁ．

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

