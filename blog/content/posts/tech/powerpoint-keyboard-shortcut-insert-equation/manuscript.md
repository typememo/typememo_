---

title: PowerPoint 2016 for mac で数式挿入ショートカットキーを設定する方法
slug: /tech/powerpoint-keyboard-shortcut-insert-equation/
date: 2018-11-14
tags: [tech, powerpoint]
image: ./img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg
socialImage: ./img/teemu-paananen-bzdhc5b3Bxs-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [設定方法](#設定方法)
  - [PowerPoint 2016 for mac には数式挿入ショートカットキーがないことを確認](#powerpoint-2016-for-mac-には数式挿入ショートカットキーがないことを確認)
  - [システム環境設定からショートカットキーを設定する](#システム環境設定からショートカットキーを設定する)
- [使い方](#使い方)
- [おわりに](#おわりに)

<!-- /TOC -->

---

## はじめに

今回は「PowerPoint 2016 for mac で数式挿入ショートカットキーを設定する方法」についてです。

PowerPoint 2016 for mac では数式挿入のショートカットキーがありません。

Word 2016 for mac では数式挿入のショートカットキーがあるのですが，なぜ PowerPoint にはショートカットキーがないのか不思議で仕方ないです．

ないから諦めるのではなく、ないからこそコチラ側で数式挿入のショートカットキーを設定してしまいましょう。

資料作成の時間と手間が大幅に改善されると思います。

以下では、その手続きを紹介します。

## 設定方法

### PowerPoint 2016 for mac には数式挿入ショートカットキーがないことを確認

以下の図のように、PowerPoint 2016 for mac にはデフォルトの設定では数式挿入ショートカットキーがありません。

![figure1](./img/sc-2018-11-14-14.45.19.png)

### システム環境設定からショートカットキーを設定する

次のステップでショートカットキーを設定します．

- システム環境設定
- キーボード
- ＋ ボタン

次のように項目を設定します．

- アプリケーション
  - Microsoft PowerPoint.app
- メニュータイトル
  - 数式
- キーボードショートカット
  - `Control + -` キー

`追加` をクリックすれば、数式挿入のショートカットキーの設定は完了です。

![figure2](./img/sc-2018-11-14-14.50.20.png)

## 使い方

例えば、スライドの真ん中に `sin[kx + ly + mz - σt]` という数式をトラックパッドやマウスを操作せずに入力することができます。

設定したショートカットキーを入力すると，数式入力モードになります．

![figure3](./img/sc-2018-11-14-14.51.13.png)

あとは数式入力モードで数式を入力するだけです．

![figure4](./img/sc-2018-11-14-14.51.51.png)

## おわりに

PowerPoint 2016 for Mac で数式挿入のショートカットキーを設定する方法の紹介でした．

最後までお読みいただきありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

