---

title: Marp for VSCode テンプレートジェネレータの開発
slug: /tech/marp-for-vscode-template-generator/
date: 2020-10-23
tags: [tech, marp, vscode]
image: ./img/tim-arterbury-VkwRmha1_tI-unsplash.jpg
socialImage: ./img/tim-arterbury-VkwRmha1_tI-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [はじめに](#はじめに)
- [テンプレートファイルの作成](#テンプレートファイルの作成)
- [テンプレートジェネレータの開発](#テンプレートジェネレータの開発)
- [テンプレートジェネレータの実行](#テンプレートジェネレータの実行)
- [おわりに](#おわりに)
  - [愛用品](#愛用品)

---

## はじめに

Marp for VSCode 用のマークダウンファイルのテンプレートジェネレータを作ってみました．

いくつか必須のフロントマターなどをその都度入力するのは面倒くさいので自動化しようと思います．

あくまで筆者が使うことを想定していますので，参考程度に読んでいただけるとありがたいです．

それでは参りましょう！

## テンプレートファイルの作成

[slide.md](https://gitlab.com/typememo/typememo_/-/blob/master/tools/res/template/slide.md)
というテンプレートファイルを作成しました．

言うてもただのマークダウンファイルに次のようなフロントマターを記載しただけですが．

```markdown
---
marp: true
theme: typememo
paginate: true
_class: lead
_header: typememo.jp
_footer: © 2020 typememo
---
...
```

## テンプレートジェネレータの開発

さて，本題のテンプレートジェネレータを開発していきます．

[typeslide](https://gitlab.com/typememo/typememo_/-/blob/master/tools/typeslide)
という名前のテンプレートジェネレータを実装しました．

`typeslide` は
[slide.md](https://gitlab.com/typememo/typememo_/-/blob/master/tools/res/template/slide.md)
というスライドテンプレートファイルを引数として渡されたファイルパスに書き出すだけのコマンドです．

[typecode](https://gitlab.com/typememo/typememo_/-/blob/master/tools/typecode)
というブログ記事テンプレートジェネレータとほとんど同じ実装です．

ほとんど同じ実装なのでメタテンプレートジェネレータを作成した方が後々になって拡張しやすそうです．

[#30](https://gitlab.com/typememo/typememo_/-/issues/30)
としてイシューには登録しておいたので忘れる心配はないと思います．

## テンプレートジェネレータの実行

スライドテンプレートジェネレータが実装できたので，テストも兼ねて実行してみたいと思います．

```shell
$ typeslide blog/content/posts/tech/marp-for-vscode-template-generator/tools/example_slide.md
Create new slide
Open new slide
```

コマンド実行終了と同時に `example_slide.md` が vscode で表示されました．

では，もしも `typeslide` の引数に何も渡さなかった場合はどうなるか．
Usage が表示されており意図した動作になっています．

```shell
$ typeslide
Usage: typeslide /path/to/slide.md
```

`typeslide` の引数にディレクトリが渡された場合はどうなるか．
"不適切な引数ですよ" とエラーメッセージが出力されており意図した動作になっています．

```shell
$ typeslide blog 
Invalid Argument: blog is a directory
Usage: typeslide /path/to/slide.md
```

## おわりに

スライドテンプレートジェネレータの開発についての紹介でした．

完全に個人用のテンプレートジェネレータでしたが，何かしら学びになるポイントがあれば幸いです．

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)
- [HHKB Professional](https://amzn.to/3dGtLS1)
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)
- [Apple MacMini](https://amzn.to/34jvsSt)
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)
