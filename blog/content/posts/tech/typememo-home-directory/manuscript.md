---

title: typememo の .zshrc を公開します
slug: /tech/typememo-home-directory/
date: 2020-10-28
tags: [tech, zsh, direnv]
image: ./img/scott-webb-1ddol8rgUH8-unsplash.jpg
socialImage: ./img/scott-webb-1ddol8rgUH8-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [はじめに](#はじめに)
- [.zshrc のキモ](#zshrc-のキモ)
  - [シェルは見やすいよね](#シェルは見やすいよね)
  - [ls したらハイライトされるよね](#ls-したらハイライトされるよね)
  - [asdf でなるべくバージョン管理するよね](#asdf-でなるべくバージョン管理するよね)
  - [direnv で環境変数を管理するよね](#direnv-で環境変数を管理するよね)
- [おわりに](#おわりに)
  - [愛用品](#愛用品)

---

## はじめに

全く需要がないと思っているのですが，なんとなく環境設定ファイルを web に公開したくなりました．

筆者の `.zshrc` を公開します．

ソースコード:
- [.zshrc](https://gitlab.com/typememo/typememo_home/-/blob/master/.zshrc)

環境設定ファイルに記載されていてみなさまが知っておくと便利かもしれない事柄を以下に紹介します．

## .zshrc のキモ

### シェルは見やすいよね

筆者のシェルの画面はこんな感じです．

![typememo shell](./img/Screenshot_2020-10-28_23.22.41.png)

こんな感じのシェルにする設定は次のとおりです．

```shell
# --------------------------------------------
# ZSH manager
# --------------------------------------------
autoload -U compinit && compinit
autoload -Uz vcs_info
setopt prompt_subst
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' stagedstr "%F{yellow}!"
zstyle ':vcs_info:git:*' unstagedstr "%F{red}+"
zstyle ':vcs_info:*' formats "%F{green}%c%u[%b]%f"
zstyle ':vcs_info:*' actionformats '[%b|%a]'
precmd () { vcs_info }
PROMPT='
%F{magenta}[%~]%f ${vcs_info_msg_0_}
$ '
. $HOME/.git-prompt.sh
# See https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh
```

この設定にすると現在のディレクトリが表示されるのはもちろんですが，現在の
Git ブランチの状態も一目瞭然になってとても便利です．
ブランチ名が緑色の時は特に何も変更がない状態ですが，赤色だと変更が生じている状態だとすぐにわかります．
`git add` でステージングした状態になるとブランチ名が黄色くなります．
色も自由自在に変えることができるのでお好みの色合いにしていただければと思います．

### ls したらハイライトされるよね

`ls` コマンドは alias で `ls="ls -G"` でハイライトされるように設定しておくのは当然ですよね．

```shell
# --------------------------------------------
# alias
# --------------------------------------------
alias ls="ls -G"
alias l="ls -G"
alias la="ls -Ga"
```

ハイライトしている状態とされていない状態では見やすさが段違いですので，ハイライトしておくと良いです．

### asdf でなるべくバージョン管理するよね

`asdf` っていうバージョン管理ツールを使うと `python` とか `nodejs` とかを手軽にバージョン管理できます．
`asdf` じゃなくても良いのですが，何かしらのバージョン管理ツールは使っておいて損はないです．
`docker` でそういうのも含めて管理しちゃう人はあえて導入する必要はないかもしれませんが．

```shell
# --------------------------------------------
# asdf manager
# --------------------------------------------
. $HOME/.asdf/asdf.sh
```

### direnv で環境変数を管理するよね

`direnv` で各ディレクトリでの環境変数を管理するととても便利です．
`direnv` は導入しておいて損をすることはないと思います．
使ってみるとその便利さがわかると思います．
できれば今すぐ使ってみて欲しいです．

```shell
# --------------------------------------------
# Direnv
# --------------------------------------------
eval "$(direnv hook zsh)"
```

## おわりに

`.zshrc` の紹介でした．

ほんの少しだけでも参考になる箇所があればとても嬉しいです．

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．
