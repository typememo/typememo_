---

title: Docker for Mac のインストール方法
slug: /tech/docker-for-mac-install/
date: 2020-08-10
tags: [tech, docker]
image: ./img/thomas-lipke-kkXDhAUnxYI-unsplash.jpg
socialImage: ./img/thomas-lipke-kkXDhAUnxYI-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Docker ID で Sign In する](#docker-id-で-sign-in-する)
- [Get Stated!](#get-stated)
- [Docker Desktop for Mac をダウンロードしてアプリを起動する](#docker-desktop-for-mac-をダウンロードしてアプリを起動する)
- [doodle を git clone する](#doodle-を-git-clone-する)
- [docker build で docker コンテナをビルドする](#docker-build-で-docker-コンテナをビルドする)
  - [エラー対策 docker コマンドが見つかりませんと言われた場合](#エラー対策-docker-コマンドが見つかりませんと言われた場合)
- [docker run で docker コンテナを起動する](#docker-run-で-docker-コンテナを起動する)
- [docker push で docker コンテナを公開する](#docker-push-で-docker-コンテナを公開する)
- [docker hub で docker コンテナを確認する](#docker-hub-で-docker-コンテナを確認する)
- [おわりに](#おわりに)
- [References](#references)

<!-- /TOC -->

----

## はじめに

docker for MacOS のインストール方法の紹介です．

情報源は [Install Docker Desktop on Mac](https://docs.docker.com/docker-for-mac/install/) です．

## Docker ID で Sign In する

[Download from Docker Hub](https://hub.docker.com/?overlay=onboarding) にアクセスして Docker ID を作成して / 使用して Docker Hub にログインします。

## Get Stated!

Docker Hub にログインしたら、**Get started with Docker Desktop** をクリックします。

## Docker Desktop for Mac をダウンロードしてアプリを起動する

**Download Docker Desktop for Mac** をクリックして、Docker Desktop をダウンロードします。

ダウンロードした Docker.dmg ダブルクリック or ⌘ + ↓ で起動します。

起動させたら、Docker アプリを Application にドラッグアンドドロップしてください。

Application を開いて、Docker アプリを起動します。

メニューバーに Docker のアイコンが表示されるので、そのアイコンをクリックし、Sign In してください。

Sign In まで終わったら、docker hub に戻って、**Next Step** をクリックしてください。

## doodle を git clone する

続いて、docker リポジトリをクローンします。

ホームディレクトリ、もしくは任意の作業用ディレクトリに移動して、以下のコマンドを実行します。

```shell
git clone https://github.com/docker/doodle.git
```

以下のログが出力されれば成功です。

```bash
Cloning into 'doodle'...
remote: Enumerating objects: 73, done.
remote: Total 73 (delta 0), reused 0 (delta 0), pack-reused 73
Unpacking objects: 100% (73/73), done.
```

docker hub に戻り **Next Step** をクリックしてください。

## docker build で docker コンテナをビルドする

必要な材料が揃ったので、docker をビルドしてみましょう。

以下のコマンドを実行してください。実行する前に、`${DockerId}` はあなたの Docker ID に変更してください。

```shell
cd doodle/cheers2019 && docker build -t ${DockerId}/cheers2019 .
```

以下のようなログが出力されれば OK です。

```shell
Sending build context to Docker daemon   12.8kB
Step 1/9 : FROM golang:1.11-alpine AS builder
1.11-alpine: Pulling from library/golang
9d48c3bd43c5: Pull complete
7f94eaf8af20: Pull complete
9fe9984849c1: Pull complete
ec448270508e: Pull complete
65ba82af53f7: Pull complete
Digest: sha256:09e47edb668c2cac8c0bbce113f2f72c97b1555d70546dff569c8b9b27fcebd3
Status: Downloaded newer image for golang:1.11-alpine
 ---> e116d2efa2ab
Step 2/9 : RUN apk add --no-cache git
 ---> Running in 44133f7055a5
fetch http://dl-cdn.alpinelinux.org/alpine/v3.10/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.10/community/x86_64/APKINDEX.tar.gz
(1/5) Installing nghttp2-libs (1.39.2-r0)
(2/5) Installing libcurl (7.66.0-r0)
(3/5) Installing expat (2.2.8-r0)
(4/5) Installing pcre2 (10.33-r0)
(5/5) Installing git (2.22.2-r0)
Executing busybox-1.30.1-r2.trigger
OK: 21 MiB in 20 packages
Removing intermediate container 44133f7055a5
 ---> d7d12967fed7
Step 3/9 : RUN go get github.com/pdevine/go-asciisprite
 ---> Running in 08a112037a4c
Removing intermediate container 08a112037a4c
 ---> 0096137d27fa
Step 4/9 : WORKDIR /project
 ---> Running in c03275c143d9
Removing intermediate container c03275c143d9
 ---> 28eb1080816e
Step 5/9 : COPY cheers.go .
 ---> 693ea2999105
Step 6/9 : RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o cheers cheers.go
 ---> Running in 5a2551c9c2ed
Removing intermediate container 5a2551c9c2ed
 ---> 0b89764faab6
Step 7/9 : FROM scratch
 --->
Step 8/9 : COPY --from=builder /project/cheers /cheers
 ---> 6a3e8915df08
Step 9/9 : ENTRYPOINT ["/cheers"]
 ---> Running in 447424a3ae2c
Removing intermediate container 447424a3ae2c
 ---> 20e9577e7178
Successfully built 20e9577e7178
Successfully tagged typememo/cheers2019:latest
```

### エラー対策 docker コマンドが見つかりませんと言われた場合

もしも以下のようなログが出力された場合は、Docker アプリを起動してから、doodle リポジトリをクリーンしたディレクトリに移動して、同じコマンドを実行すればうまくいきます。docker コマンドへのパスが通っていないだけだと思いますので．


```bash
zsh: command not found: docker
```

## docker run で docker コンテナを起動する

docker のビルドが終わったので、docker コンテナを起動してみましょう。

そのために、以下のコマンドを実行してください。

`${DockerId}` は書き換えてください。

```bash
docker run -it --rm ${DockerId}/cheers2019
```

以下のようなログが出力されれば、コンテナの起動は成功です。

```bash
                                                                     ,---,
                                                                  ,`--.' |
     ,----..    ,---,                                             |   :  :
    /   /   \ ,--.' |                              ##     .  .    '   '  ;
   |   :     :|  |  :                        ##_## ##.      ==    |   |  |
   .   |  ;. /:  :  ,                     ## ## ##/##|  .--===.   '   :  ;
   .   ; /--` :  |  |,--.   ,---.     /""""""""""""""""\___/ ===  |   |  '
   ;   | ;    |  :  '   |  / ________{                      /`.==='   :  |
   |   : |  __....::::::::::''''''' / \______ o          __/....__;   |  ;
   .   | .:'::.  :  | | |.    ' / |.    \    \        __/ \    .::':.-'. |
   '   ; :':.|':::::.___.____,_________________,:_._____..:::::' ,'`--..`;
   '   | '/ ':.  : '::::::::::::::::::::::::::::::::::o::'/`--': '.--,_
   |   :    / ':.| ,'    |   :    ||   :    | ---'    '--'.  .:'/ |    |`.
    \   \ .'  `-':.       \   \  /  \   \  /            `--.:'-'  `-- -`, ;
     `---`        '.       `----'    `----'       o       .'        '---`"
                    '-._                              _.-'
                        '- .._                 _.. -'
                              ''' - .,.,. - '''
                                   (:' .:)
                                    :| '|
                                    |. ||
                                    || :|
                                    :| |'
                                    || :|
                                    '| ||
                                    |: ':
                                    || :|
                              __..--:| |'--..__
                        _...-'  _.' |' :| '.__ '-..._
                      / -  ..---    '''''   ---...  _ \
                      \  _____  ..--   --..     ____  /
                       '-----....._________.....-----'


                             Press 'ESC' to quit.
```

ESC キーを押下して終了させましょう。

## docker push で docker コンテナを公開する

作成した docker コンテナを世界中の人に使ってもらえるようにしてみましょう。

そのためには以下のように `docker login` コマンドと `docker push` コマンドを実行します。

```bash
docker login && docker push ${DockerId}/cheers2019
```

以下のようなログが出力されれば成功です。

```bash
Authenticating with existing credentials...
Login Succeeded
The push refers to repository [docker.io/typememo/cheers2019]
ceedec3d242f: Pushed
latest: digest: sha256:df1f655cfbc7f22cf48f1c49294ef81b0a49b9caea0094947660070fc2cd94dd size: 528
```

## docker hub で docker コンテナを確認する

**View my repository** をクリックすれば、先ほど `docker push` した docker コンテナを確認できます。

## おわりに

自分の勉強不足で用語の説明まではできませんでした。できれば，追って、加筆 / 修正 したいと思っています。

最後までお読みいただきありがとうございました．

## References

* https://docs.docker.com/docker-for-mac/install/

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

