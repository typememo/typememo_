---

title: gitlab wiki をローカル環境で gollum を使わずに書く方法
slug: /tech/gitlab-wiki-local-writing/
date: 2020-08-01
tags: [tech, git, gitlab]
image: ./img/iconfinder_Gitlab_2613296.png
socialImage: ./img/iconfinder_Gitlab_2613296.png
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [ローカルで wiki を書き、gitlab wiki に反映させるまでの手順](#ローカルで-wiki-を書きgitlab-wiki-に反映させるまでの手順)
- [ディレクトリ構成の工夫: 同じ名前のディレクトリとマークダウンファイルを同じ階層に置くこと](#ディレクトリ構成の工夫-同じ名前のディレクトリとマークダウンファイルを同じ階層に置くこと)
- [wiki 内部のリンク方法: `/path_to_file` で指定すること `/path_to_file.md` ではなく](#wiki-内部のリンク方法-path_to_file-で指定すること-path_to_filemd-ではなく)
- [注意: origin/master ブランチの内容しか wiki には反映されない](#注意-originmaster-ブランチの内容しか-wiki-には反映されない)
- [全て `wikis/` 直下で管理する方法](#全て-wikis-直下で管理する方法)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

gitlab wiki をローカル環境で gollum を使わずに書く方法を紹介します。

> Note:
> 以下の前提で話を進めますのでご留意ください。
> * gitlab wiki で Home ページを作成していること。
> * `git clone` で wiki を clone していること。

## ローカルで wiki を書き、gitlab wiki に反映させるまでの手順

wiki をローカル環境で書き gitlab の wiki に反映させるまでの手順は以下の通りです。

1. wiki の中身はローカル環境で好きなエディターで書く。
1. master ブランチに修正を push する
1. gitlab の wiki で情報を共有する

master ブランチに push して gitlab の wiki で情報を共有するところに工夫は必要ありません。
しかし wiki をローカル環境で書くときには、**ディレクトリ構成にちょっとした工夫が必要です。**

## ディレクトリ構成の工夫: 同じ名前のディレクトリとマークダウンファイルを同じ階層に置くこと

ディレクトリ構成の工夫とは、**同じ名前のディレクトリとマークダウンファイルを同じ階層に置くこと**です。

あくまでも一例ですが、wiki のディレクトリ構成は以下のようにしました。

`home.md` と同じ階層に `home/` を作成します。
`home/` 配下には `common/` と `homes/` があります。
`common/` がある階層には `common.md` があり、
`homes/` がある階層には `homes.md` があります。
`common/` の中には `rule.md` があります。
`homes/` の中は、それぞれ `user1/` `user1.md` `user2/` `user2.md` `user3/` `user3.md` があります。

**wiki ディレクトリ構成**

```bash
.
├── home
│   ├── common
│   │   └── rule.md
│   ├── common.md
│   ├── homes
│   │   ├── user1
│   │   ├── user1.md
│   │   ├── user2
│   │   ├── user2.md
│   │   ├── user3
│   │   └── user3.md
│   └── homes.md
└── home.md
```

このようなディレクトリ構成にすることで wiki の url が成立して、gitlab の wiki でマークダウンファイルの内容を表示することができるようになります。

上記の **wiki の url が成立する**とはどういうことか説明します。
wiki の Home ページの url は以下のようになっています。

```bash
https://gitlab.com/${USERNAME}/${PROJECT}/-/wikis/home
```

url 末尾付近 の `/-/wikis/home` 部分に注目してください。
`/-/wikis` は gitlab の wiki ページを意味しています。
`/home` は `home.md` を意味しています。
gitlab は `.md` ファイルを表示すべきファイルだと解釈します。
表示すべきファイルだと解釈された `.md` ファイルのファイル名を url の末尾として設定し、`.md` ファイルを整形して表示します。

この gitlab が `.md` ファイルを表示するロジック (仕組み) が分かれば、自由自在に wiki が書けます。

## wiki 内部のリンク方法: `/path_to_file` で指定すること `/path_to_file.md` ではなく

`/home/common` を作成して、`/home/common.md` を作成します。
`/home/common.md` を編集して `/home.md` にリンクを貼る方法は以下の通りです。

```html
<!-- /home/common.md -->
# Common

hogehoge fugafuga.

back to [Home](/home)
```

`/-/wikis/home` と url 部分が同じになるように `/-/wikis` 以下をリンク先として記述します。
リンク先は `/home.md` ではなく `/home` とするのが大事なポイントです。
gitlab は url 末尾の `.md` ファイルを表示すべきファイルとして解釈します。
なので、リンク先に `.md` を含めると url が正しく認識されません。
結果として、リンクに失敗しますのでご注意ください。

以上の内容を踏まえれば、wiki 内部リンクの記述方法は簡単で、`/home.md` から `/home/common.md` にリンクを貼るには以下のように記述できます。

```html
<!-- /home.md -->
# Home

Welcome

[common:](/home/common)
```

では、`/home/common/rule.md` へのリンクを、`/home/common.md` に記述するにはどうすれば良いでしょうか？
少し考えて、適当な場所に書いてみてください。

以下のように書けていれば、wiki 内部リンク記述方法はバッチリですね ：）

```html
<!-- /home/common.md -->
# Common

hogehoge fugafuga.

[Rule:](/home/common/rule)

back to [Home](/home)
```

あとはお好きなようにディレクトリ構成をカスタマイズしてください。
wiki 内部リンクの記述方法が分かればカスタマイズは自由自在です。


## 注意: origin/master ブランチの内容しか wiki には反映されない

一つ、注意です。
**origin/master ブランチの内容しか wiki には反映されないので注意してください。**
clone した ローカル環境の wiki でブランチを切ることはできますが、gitlab の wiki に反映される内容は master ブランチだけです。
ブランチを切って作業するのは良いですが、最終的には master ブランチにマージしてください。

なので、基本的に master ブランチで作業するのが良いと思います。
もし、多人数で wiki を更新している場合は、各自で作業ブランチを切って、wiki 管理者が最終的に master ブランチにマージして、origin/master に push すると良いと思います。


## 全て `wikis/` 直下で管理する方法

もちろん、上記で紹介したような階層を持たず、全て `wikis/` 直下で管理することもできます。

適当に `author.md` を `home.md` と同じ階層に作成して、`home.md` に `author.md` にリンクを貼れば良いです。
`author.md` にも `home.md` のリンクを貼っておけば尚良いと思います。

```html
<!-- home.md -->
# Home

Welcome!

[Author](/author)
```

```html
<!-- author.md -->
# Author

Author: Hogehoge Fugafuga

back to [Home](/home)
```

## おわりに

gitlab wiki をローカル環境で gollum を使わずに書く方法の紹介でした。

おさらいですが、要点をまとめると以下の通りです。

* 同じ名前のディレクトリとマークダウンファイルを同じ階層に置く
* wiki 内部リンクは `/path_to_file` で記述する
* gitlab は url 末尾の `.md` ファイルを表示すべきファイルだと解釈する

本記事で紹介した内容が、"役に立った!" と感じていただけたら嬉しいです。

最後までお読みいただき、ありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

