---

title: Macのメールソフト mail で添付ファイルが添付されずに焦った件
slug: /tech/macos-mail-why-attachments-gone/
date: 2019-01-18
tags: [tech, mac, macos, mail]
image: ./img/lauren-mancke-aOC7TSLb1o8-unsplash.jpg
socialImage: ./img/lauren-mancke-aOC7TSLb1o8-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [設定方法](#設定方法)
  - [「メール」 => 「環境設定」](#メール--環境設定)
  - [「作成」 => 「メッセージのフォーマット」](#作成--メッセージのフォーマット)
  - [リッチテキストと標準テキストの違い](#リッチテキストと標準テキストの違い)
- [おわりに](#おわりに)
- [References](#references)

<!-- /TOC -->

---

## はじめに

つい先日，

> Aさん
> 「最近、山田くんから来るメールには、添付ファイルが添付されていないのだけど、何か特別な設定してるの？」

> 筆者
> 「えっ、何も、そのような特別な設定はしていませんが。。。」

背筋が凍りつきました。

どうやら、メールの「フォーマット」に原因があるようでした。

具体的には、メールのフォーマットを「リッチテキスト」から「標準テキスト」に変更すると、きちんと添付されるようです。

以下に、メールのフォーマットを「リッチテキスト」から「標準テキスト」に変更する方法を示します。

## 設定方法

### 「メール」 => 「環境設定」

Macの画面上端にあるメニューバーから、「メール」→「環境設定」をクリックします。

### 「作成」 => 「メッセージのフォーマット」

環境設定のリボンの中から「作成」をクリックして、「メッセージのフォーマット」を「リッチテキスト」から「標準テキスト」に変更します。

これで、メッセージを送るときに「標準テキスト」を使えるようになり、添付ファイルがきちんと送れるようになるはずです。

![figure](./img/sc-2019-01-18-12.07.02.png)

### リッチテキストと標準テキストの違い

Appleによれば、リッチテキストフォーマットと標準テキストフォーマットはそれぞれ以下の通りです。

リッチテキストフォーマット:
> リッチテキスト（HTML）フォーマットとは、書式設定、表、画像を含めることができるが、このフォーマットのメッセージを読めない受信者もいるフォーマットである。

標準テキストフォーマット:
> 標準テキストフォーマットとは、ボールドテキストやイタリックテキストなどの書式設定も、表や画像などの項目も含まれませんが、すべての受信者が読めるフォーマットである。

## おわりに

以上書いたことは、あくまで私の場合はうまくいったことなので、同じようにうまくいくかどうかはわかりません。

でも、試す価値はあると思ったので、記事にしました。

本記事が誰かの役に立てれば嬉しいです。

## References

- https://support.apple.com/ja-jp/guide/mail/mlhlp1009/mac

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

