---

title: GatsbyJS に Google Adsense のプラグインを入れて自動広告を表示する方法
slug: /tech/gatsby-google-adsense/
date: 2020-10-20
tags: [tech, gatsby]
image: ./img/anthony-rosset-5r5554u-mHo-unsplash.jpg
socialImage: ./img/anthony-rosset-5r5554u-mHo-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [はじめに](#はじめに)
- [gatsby-plugin-google-adsense をインストールする](#gatsby-plugin-google-adsense-をインストールする)
- [gatsby-config.js に publisherId を追加する](#gatsby-configjs-に-publisherid-を追加する)
- [おわりに](#おわりに)
  - [愛用品](#愛用品)


---

## はじめに

GatsbyJS に Google Adsense をプラグインを使って導入する方法についての紹介記事です．

プラグインを `npm` コマンドでインストールして `gatsby-config.js` ファイルを編集するだけです．

ややこしいことがなくて簡単だったのですが，紹介している記事が少なかったので加勢しようと思いました．

参考:
- https://www.gatsbyjs.com/plugins/gatsby-plugin-google-adsense/
- [GatsbyJSで実現する、高速＆実用的なサイト構築](https://amzn.to/2HiF46T)  
GatsbyJS の基本的な API とかカスタムタグについて広く学べます

## gatsby-plugin-google-adsense をインストールする

gatsby-plugin-google-adsense をインストールしてください．

```shell
$ npm install --save gatsby-plugin-google-adsense
```

あとは `gatsby-config.js` を更新するだけです．

## gatsby-config.js に publisherId を追加する

`gatsby-config.js` に次のコードを追加してあげてください．

`publisherId` の `xxxxxxxxxx` はご自身の ID に書き換えてください．

```javascript
// In your gatsby-config.js file
plugins: [
    {
      resolve: `gatsby-plugin-google-adsense`,
      options: {
        publisherId: `ca-pub-xxxxxxxxxx`
      },
    },
]
```

後は，デプロイしてあげれば head タグに google adsense の script タグが自動挿入されます．

めちゃめちゃ簡単でした！

## おわりに

GatsbyJS に Google Adsense を導入する方法についての紹介でした．

簡単すぎて拍子抜けしたと思います．

プラグイン使って手間がほとんどかからずに自動広告を掲載できるなんてなんて良い技術でしょう．

gatsby-plugin-google-adsense を作っていただいた方に，この場を借りて御礼申し上げます．

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)
- [HHKB Professional](https://amzn.to/3dGtLS1)
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)
- [Apple MacMini](https://amzn.to/34jvsSt)
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)
