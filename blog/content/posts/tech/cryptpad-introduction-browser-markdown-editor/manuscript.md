---

title: ブラウザ markdown エディターの決定版 Cryptpad の紹介
slug: /tech/cryptpad-introduction-browser-markdown-editor
date: 2020-07-30
tags: [tech, markdown, cryptpad]
image: ./img/kelly-sikkema-mdADGzyXCVE-unsplash.jpg
socialImage: ./img/kelly-sikkema-mdADGzyXCVE-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Cryptpad をざっくり紹介](#cryptpad-をざっくり紹介)
  - [Good: ユーザー ID とパスワードだけでサインインできる](#good-ユーザー-id-とパスワードだけでサインインできる)
  - [Good: 複数人同時リアルタイム編集ができる](#good-複数人同時リアルタイム編集ができる)
  - [Good: エディターのテーマが豊富で自分好みにカスタマイズできる](#good-エディターのテーマが豊富で自分好みにカスタマイズできる)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

ブラウザの markdown エディター決定版だと思う **Cryptpad** という Web サービスについて紹介します。

## Cryptpad をざっくり紹介

[Cryptpad](https://cryptpad.fr/index.html) は、ざっくり言えば無料の Web エディターです。
Cryptpad の詳しい説明は [What is Cryptpad](https://cryptpad.fr/what-is-cryptpad.html) をご覧ください。

Code, Rich text, Presentation, Sheet, Poll, Kanban, Whiteboard, CryptDrive の８機能があります。
この中で、基本的に **Code** と **CryptDrive** を使います。
なぜ，この２つかというと、ブラウザで markdown を書くときは Code を使い、その markdown ファイルを保存するのに CryptDrive を使うからです．

[Cryptpad](https://cryptpad.fr/index.html) の Code をクリックすると、次の画像のようなページが表示されるかと思います。

![figure-cryptpad-code](./img/figure-cryptpad-code.png)

### Good: ユーザー ID とパスワードだけでサインインできる

筆者の場合はすでに Cryptpad にログインしているので、右上に筆者のアイコンが表示されています。
しかし、初めて Cryptpad を使っている人は、匿名ユーザーのアイコンが表示されていると思います。
その匿名ユーザーのアイコンをクリックすると、サインインページが表示されるかと思います。

ユーザー ID と パスワード を入力するだけで Cryptpad にサインインできます。
メールアドレスの登録などは一切不要です。
しかし、ユーザー Id とパスワードを忘れてしまうと、二度とサインインできませんので注意してください。
忘れっぽい人は、どこかにメモっておくことを強くお勧めします。

### Good: 複数人同時リアルタイム編集ができる

お仕事で google documents を使っている方もおられるかと思います。
google documents の素晴らしいところは、複数人同時リアルタイム編集ができるところだと個人的に思っています。
でも、google documents の惜しいところは markdown 記法で書けないところ、だと個人的に持っています。

Cryptpad は markdown 記法で書けて、複数人同時リアルタイム編集ができます。
なので、markdown 記法に慣れている方が多い環境では、google document よりも Cryptpad を使った方が良いと思います。
共有方法はリンクを送るだけです。

実際、html や css などの web 技術の仕様を決める W3C の会議では、Cryptpad の Code を使って議事録を取っていました。
筆者は W3C の会議に参加して Cryptpad の存在を知りました。
これは便利だと思い、ブログ記事を書いてひっそりと普及活動をしております。

### Good: エディターのテーマが豊富で自分好みにカスタマイズできる

エディターのテーマが豊富で、自分好みにカスタマイズできるのも良いところだと思います。
テーマは 48 種類ありますので、お好みのテーマを選んでくださいませ。
筆者は dracula を使っております。

エディターのフォントサイズも設定から変更できます。
デフォルトのフォントサイズは 12px だったはずです。
筆者にとっては 12px は小さいので、16px にしております。

プレビュー部分はカスタマイズすることができません。
エディターはダークテーマなのだけど、プレビューはライトテーマになってしまうので、少し惜しいところではあります。
今後のアップデートを期待しましょう。

## おわりに

ブラウザ markdown エディター Cryptpad の紹介でした。
外出自粛のいまだからこそ、いまどきの web 技術を使って、自宅でもくもくと気楽に過ごしましょう。

Cryptpad は 2020.04.09 時点でまだ一般的に普及していない web サービスです。
でも、すごく便利なので、いろんな人に知って欲しいです。
この記事が少しでも気に入って頂けたなら、各方面に拡散していただけるととても嬉しいです。

最後までお読みいただき、ありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

