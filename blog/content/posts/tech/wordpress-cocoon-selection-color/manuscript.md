---

title: Wordpress Cocoon 選択背景色のいい感じな設定方法
slug: /tech/wordpress-cocoon-selection-color/
date: 2020-08-02
tags: [tech, wordpress]
image: ./img/launchpresso-IOM28XWsk-g-unsplash.jpg
socialImage: ./img/launchpresso-IOM28XWsk-g-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [選択背景色の色は何色にするのか？](#選択背景色の色は何色にするのか)
- [選択背景色を #fde4b3 にする](#選択背景色を-fde4b3-にする)
- [選択文字色をクリアにする](#選択文字色をクリアにする)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

Wordpress Cocoon テーマの選択背景色をいい感じに設定する方法の紹介です。

## 選択背景色の色は何色にするのか？

Google Chrome では、文字列を選択すると、以下の画像のように、選択した文字列の背景色が変化します。

![Figure1 selection color on google chrome](./img/figure-selection-example.png)
Figure.1 選択背景色の例

この選択した文字列の背景色は *#fde4b3* というカラーコードです。
このカラーコードは Google の検索トップページの選択背景色としても使われています。

このカラーコードは、Digital Color Mater というアプリを使って知りました。
Mac をお使いの方は Digital Color Meter というアプリが、デフォルトでインストールされています。
Spotlight で探してみてください。

## 選択背景色を #fde4b3 にする

さて、ここから先は Wordpress 上での作業に移ります。

- Wordpress
- => Cocoon 設定
- => 全体
- => サイト選択背景色
- => 色を選択
- => *#fde4b3* を記入する
- => 変更をまとめて保存 をクリックする

以上で、選択背景色の設定は終了です。

## 選択文字色をクリアにする

続いて、選択文字色の設定をしましょう。

- Wordpress
- => Cocoon 設定
- => 全体
- => サイト選択文字色
- => 色を選択
- => クリア
- => 変更をまとめて保存 をクリックする

以上で、選択文字色の設定は終了です。

![Figure2 selection color settings in cocoon wordpress](./img/figure-selection-color-settings-in-cocoon-wordpress.png)
Figure.2 Wordpress 選択背景色の設定方法

選択背景色を *#fde4b3* に設定し、選択文字色をクリアにしたら、
いい感じの設定になっていると思います。
ご自身のサイトの文字列を選択して確認してください。

## おわりに

背景文字色をいい感じに設定する方法の紹介でした。
選択背景色は *#fde4b3* に、選択文字色はクリアにする。
ちなみに [本サイト](https://typememo.jp) もその設定になっています。

最後に注意事項を一つ。
選択背景色の *#fde4b3* と同じ文字色の文字列は、
選択すると背景色と同化して見えなくなってしまうので注意してください。

最後までお読みいただきありがとうございました。


### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

