---

title: gifを簡単に作る方法を教えます! (Mac・Linux環境)
slug: /tech/imagemagick-gif-mac/
date: 2018-07-17
tags: [tech, imagemagick, gif, mac]
image: ./img/melanie-dretvic-hMfoKqVaRn0-unsplash.jpg
socialImage: ./img/melanie-dretvic-hMfoKqVaRn0-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [ImageMagickをインストールする](#imagemagickをインストールする)
- [Convertコマンドのエッセンス](#convertコマンドのエッセンス)
- [convertコマンドを実行する](#convertコマンドを実行する)
- [おわりに](#おわりに)

<!-- /TOC -->

---

## はじめに

簡単にgifアニメーションを作る方法をまとめました（Mac環境)。

それでは早速作業していきましょう！

## ImageMagickをインストールする

まずターミナルを開きます。

続いて以下２つのコマンドを入力して "ImageMagick" をインストールしてください。

```shell
brew install ImageMagick
brew upgrade ImageMagick
```

## Convertコマンドのエッセンス

`convert` コマンドを実行して gif を作成します．

コマンドを実行する前に、`convert` コマンドの使い方を紹介します。

基本的な使い方は次の通りです．

```shell
convert -loop x -delay time "input_filenames"+.(jpg | png) "output_filename"+.gif
```

`-loop x` の x で繰り返す回数を決定します。
デフォルトでは「x = 0」となっており、「x = 0」のときは無限に繰り返します。

`-delay time` の time で入力画像1枚あたりの表示時間を決定します。
`convert` コマンドの時間の単位は 1/100 秒となっています．
なので time = 100 とすれば入力画像1枚あたり 100 / 100 秒 = 1秒表示されることになります。

## convertコマンドを実行する

ごたくを並べても感覚的にわからないと思いますので、実際に手持ちの画像を使って gif アニメーションを作ってみましょう。

ターミナルを起動して，`cd` コマンド使って，gif アニメーションにしたい画像ファイルがあるディレクトリまで移動してください。

お目当てのディレクトリに移動したら `convert` コマンドを使ってgifアニメーションを作ってみましょう。

例えば、画像ファイル名が「image001.png」「image002.png」「image003.png」「image004.png」「image005.png」のように連番になっていて「anime.gif」というgifアニメーションを作りたいときは、

```shell
convert -loop 0 -delay 100 image???.png anime.gif
```

と入力します。

するとカレントディレクトリに「anime.gif」という「無限に繰り返す画像1枚あたりの表示時間が1秒のgifアニメーション」があっという間にできているはずです。

ここで注意していただきたいのは、入力画像ファイル名が「001, 002, 003, ...」のような1刻みの連番でなくてもいいことです。
つまり「001, 004, 007, 011, ...」のようなトビトビの連番でも数字の若い順番でgifアニメーションは作成されます。
この機能、非常に便利だと思います。

また、gifアニメーションを入力画像のあるディレクトリと同じディレクトリに保存したくないときは、パスを通して保存したい場所に保存してください。
ディレクトリ移動が面倒くさい人はフルパスを通せばよいです。

## おわりに

以上、`convert` コマンドを使ったgifアニメーションの作り方でした。

最後までお読みいただき、ありがとうございました．

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

