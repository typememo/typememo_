---

title: "gatsby-theme-blog でフォントをシャドーイングする方法"
slug: /tech/gatsby-theme-blog-font-shadowing/
date: 2020-07-08
tags: ["gatsby", "shadowing", "font"]
image: ./img/alexander-andrews-zw07kVDaHPw-unsplash.jpg

---

**Contents**

<!-- TOC -->

- [まとめ](#まとめ)
  - [イントロ](#イントロ)
  - [手順](#手順)
    - [シャドーイング対象を見つける](#シャドーイング対象を見つける)
    - [シャドーイングする](#シャドーイングする)
  - [おわりに](#おわりに)
  - [References](#references)

<!-- /TOC -->

----

# まとめ

* シャドーイング対象の `${mysite}/node_modules/gatsby-theme-blog/src/gatsby-plugin-theme-ui/index.js` を見つける
* シャドーイングするために `${mysite}/src/gatsby-plugin-theme-ui/index.js` を新規作成する
* 実際にシャドーイングしてみる

## イントロ

[gatsby-theme-blog](https://www.gatsbyjs.org/packages/gatsby-theme-blog/) でフォントをシャドーイングする方法についての紹介です．

gatsby でのシャドーイングに関する基本的な知識は [Shadowing in Gatsby Themes](https://www.gatsbyjs.org/docs/themes/shadowing/) で得られます．

その中で本記事に最も関連のある内容は [Shadowing other files](https://www.gatsbyjs.org/docs/themes/shadowing/#shadowing-other-files) です．

これには `gatsby-theme-blog` が依存しているプラグインの一つである `gatsby-plugin-theme-ui` のプリセットである `gatsby-theme-ui-preset` をシャドーイングする方法が書かれています．

このシャドーイング方法を参考にした，フォントファミリーをシャドーイングする方法を具体的に説明します．

## 手順

### シャドーイング対象を見つける

シャドーイングするにはシャドーイング対象を見つける必要があります．

シャドーイング対象は `${mysite}/node_modules` 以下のどこかにあります．

ローカルに保存されている `${mysite}/node_modules` 以下のファイルを `grep` などして目当てのシャドーイング対象を探しても良いですが，それだと少し面倒だと感じる方は [gatsby packages @GitHub](https://github.com/gatsbyjs/gatsby/tree/master/packages) にアクセスしてブラウザ上で探すと良いと思います．

てなこんなで探していくと，フォントファミリーを定義しているのは `${mysite}/node_modules/gatsby-theme-blog/src/gatsby-plugin-theme-ui/index.js` だと判明しました．

`${mysite}/node_modules/gatsby-theme-blog/src/gatsby-plugin-theme-ui/index.js` の中身を次のコードブロックに示します．

```javascript
import merge from "deepmerge"
import typography from "./typography"
import colors from "./colors"
import styles from "./styles"
import prism from "./prism"

export default merge(typography, {
  initialColorMode: `light`,
  colors,
  fonts: {
    heading: `Montserrat, sans-serif`,
    monospace: `Consolas, Menlo, Monaco, source-code-pro, Courier New, monospace`,
  },
  sizes: {
    container: 672,
  },
  styles,
  prism,
})
```

`fonts: {...}` で `heading` `monospace` のフォントファミリーを定義しています．

シャドーイング対象が分かったので，あとは [Shadowing other files](https://www.gatsbyjs.org/docs/themes/shadowing/#shadowing-other-files) の方法を参考にして実際にシャドーイングしてみましょう．

### シャドーイングする

[Shadowing other files](https://www.gatsbyjs.org/docs/themes/shadowing/#shadowing-other-files) に，

> For example, to shadow index.js from gatsby-plugin-theme-ui, create a file named user-site/src/gatsby-plugin-theme-ui/index.js.

`gatsby-plugin-theme-ui` の `index.js` をシャドーイングするには，`${mysite}/src/gatsby-plugin-theme-ui/index.js` というファイルを作成してシャドーイングせよと書いてあります．

"へぇ，そうなんだ．やってみるか．"

実際に `${mysite}/src/gatsby-plugin-theme-ui/index.js` を作成して，以下のコードを書いてみました．

```javascript
import baseTheme from "gatsby-theme-blog/src/gatsby-plugin-theme-ui/index"

export default {
  ...baseTheme,
  fontSizes: [12, 14, 16, 24, 32, 48, 64, 96, 128],
  space: [0, 4, 8, 16, 32, 64, 128, 256],
  colors: {
    ...baseTheme.colors,
    primary: '#232129',
  },
  fonts: {
    heading: 'Source Sans Pro, Helvetica Neue, sans-selif',
    body: 'Note Sans, Note Sans JP, Open Sans, Roboto sans-selif',
    monospace: 'Source Code Pro, monospace',
  },
  styles: {
    ...baseTheme.styles,
    inlineCode: {
      bg: "#f0f0f0",
    },
  },
}
```

`fonts: {...}` で `heading` `body` `monospace` のフォントファミリーを好みのフォントファミリーにシャドーイングしています．

`Helvetica Neue` 以外は [Google Web Fonts](https://fonts.google.com/) を使っています．

## おわりに

`gatsby-theme-blog` でフォントをシャドーイングする方法についての紹介でした．

`gatsby` ブログに移行して，初めて `React` に触りました．

`React` の勉強をしつつ，ブログも更新できるので，`gatsby` は良いなと思いました．

ブログを更新するのが楽しくなりそうです：）

最後までお読みいただきまして，ありがとうございました！

## References

* [gatsby-theme-blog](https://www.gatsbyjs.org/packages/gatsby-theme-blog/)
* [Shadowing in Gatsby Themes](https://www.gatsbyjs.org/docs/themes/shadowing/)
* [gatsby packages @GitHub](https://github.com/gatsbyjs/gatsby/tree/master/packages)
* [Google Web Fonts](https://fonts.google.com/)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

