---

title: Gmail 生産性向上テクニック - スレッドとビューワーを分割する
slug: /tech/gmail-mailbox-separation/
date: 2020-08-02
tags: [tech, gmail]
image: ./img/kon-karampelas-N82naZ0N4TY-unsplash.jpg
socialImage: ./img/kon-karampelas-N82naZ0N4TY-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [結論: 受信トレイをメールスレッドとメールビューワーに分離する](#結論-受信トレイをメールスレッドとメールビューワーに分離する)
- [設定方法: プレビューパネルを有効にする](#設定方法-プレビューパネルを有効にする)
- [おわりに](#おわりに)

<!-- /TOC -->

## はじめに

Gmail の受信トレイをスレッドとビューワーに分割する方法の紹介です．

大学でも仕事でもよく使うツールの一つが Gmail ですよね。

Mac とか Windows とかの純正メールアプリを使っている人も多いかと思いますが Gmail はブラウザだけで完結します．
なので、容量の大きい添付ファイルとかが積み重なって、データ容量を圧迫するような心配もありません．
やはり，使い勝手の良い Web サービスだと思います。

でも、そんな Gmail もデフォルトのままだと少し使いづらさを感じることがあります．
Gmail のトップページって受信トレイしか表示されていないんですよね．．．

Mac の純正メールアプリは、メールスレッドとメール本文が分離していて、メールスレッドを選択すれば、すぐにメール本文が読めるようになっているんです。

これを Gmail でも実現できないかといろいろ模索し、ついに解決方法を見つけました。

この記事では、その解決方法を紹介します。

## 結論: 受信トレイをメールスレッドとメールビューワーに分離する

最終的には、Figure.1 のように、メールスレッドとメールプレビューを分割して表示できるようにしました。

![gmail-separate-thread-and-preview](./img/gmail-separate-thread-and-preview.png)
Figure.1 受信トレイをメールスレッドとメールビューワーに分割した図

キーボードの↑と↓でメールスレッドを移動して、マウスやトラックパッドで本文をスクロールすれば、純正メールアプリと同じような操作性を実現できます。

以下が設定方法です。

## 設定方法: プレビューパネルを有効にする

まずは、Gmail の設定を開いてください。(Figure.2)

![gmail-settings](./img/gmail-settings.png)
Figure.2 Gmail 設定

設定を開いたら、"詳細設定" タブをクリックして、"プレビューパネル"を有効にしてください。(Figure.3)

![gmail-enable-preview-panel](./img/gmail-enable-preview-panel.png)
Figure.3 プレビューパネルの有効化

プレビューパネルを有効にしたら、"変更を保存" をクリックして、変更した設定を有効にします。

受信トレイに戻ると、設定ボタン の左隣に標準の HTML 形式と分割表示の切り替えボタンが表示されています。(Figure.4)

![gmail-enable-separate-mode](./img/gmail-enable-separate-mode.png)
Figure.4 分割表示の有効化

このボタンを押すと、分割表示に切り替えることができます。もちろん、今まで通りの受信トレイに切り替えることもできます。

以上で、設定完了です。

お疲れ様でした。

## おわりに

Gmail の受信トレイをメールスレッドとメールビューワーに分割する方法の紹介でした。

Gmail は普段使っているサービスですが、ほとんどの人がデフォルトのまま使っています。デフォルトのままでも十分使いやすいサービスですが、少しカスタマイズするだけでも格段に使い勝手が上がります。

最後までお読みいただきありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

