---

title: iOS 信頼されていないショートカットの許可方法
slug: /tech/ios-shortcut-apps-allow-unofficial-app/
date: 2020-08-05
tags: [tech, ios, shortcut]
image: ./img/ben-kolde-xdLXPic3Wfk-unsplash.jpg
socialImage: ./img/ben-kolde-xdLXPic3Wfk-unsplash.jpg
imageAlt: eyecatch

---

## ショートカットの許可方法

デフォルトでインストールされていないショートカットの許可方法についてです。

* 設定
* => ショートカット
* => 信頼されていないショートカットを許可する にチェックをいれる

![figure-allow-unofficial-shortcut](./img/FigureAllowUnofficialShortcut.png)

以上です。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

