---

title: 具体例 - Marp for VSCode 自作テーマ開発
slug: /tech/marp-for-vscode-mytheme/example/
date: 2020-10-19
tags: [tech, marp, vscode]
image: ../img/giuseppe-famiani-t8KVrEhCJuc-unsplash.jpg
socialImage: ../img/giuseppe-famiani-t8KVrEhCJuc-unsplash.jpg
imageAlt: eyecatch

marp: true
theme: typememo
paginate: true
_class: lead
_header: This is header
_footer: This is footer

---

# Marp for VSCode 自作テーマ開発

typememo

---

Contents

- [References](#references)
- [Title](#title)
- [Image right](#image-right)
- [Image left](#image-left)
- [Image contain Right](#image-contain-right)
- [Image contain Left](#image-contain-left)
- [Code block](#code-block)
- [日本語タイトル](#日本語タイトル)
- [Table](#table)
- [Quote](#quote)

---

## References

- [example.md](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/marp-for-vscode-mytheme/tools/example.md)
- [example.pdf](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/marp-for-vscode-mytheme/tools/example.pdf)

---

## Title

Hello, marp!

---

## Image right

![bg right](../img/giuseppe-famiani-t8KVrEhCJuc-unsplash.jpg)

---

## Image left

![bg left](../img/giuseppe-famiani-t8KVrEhCJuc-unsplash.jpg)

---

## Image contain Right

- Hoge hoge ...
- Fuga fuga ...

![bg right contain](../img/giuseppe-famiani-t8KVrEhCJuc-unsplash.jpg)

---

## Image contain Left

1. Hoge hoge ...
1. Fuga fuga ...

![bg left contain](../img/giuseppe-famiani-t8KVrEhCJuc-unsplash.jpg)

---

## Code block

- `Python` Hello, world!

```python
def hello():
  print("Hello, world!")
```

---

## 日本語タイトル

- いろはにほへと

---

## Table

| Item1 | Item2 |
| :-- | :-- |
| hoge | huga |
| foo | bar |

---

## Quote

He said ...

> Hello, world
