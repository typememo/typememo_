---

title: Marp for VSCode 自作テーマを作ってみよう
slug: /tech/marp-for-vscode-mytheme/
date: 2020-10-18
tags: [tech, marp, vscode]
image: ./img/giuseppe-famiani-t8KVrEhCJuc-unsplash.jpg
socialImage: ./img/giuseppe-famiani-t8KVrEhCJuc-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [はじめに](#はじめに)
- [成果物](#成果物)
- [自作テーマ設定](#自作テーマ設定)
- [自作テーマ開発](#自作テーマ開発)
	- [シャドーイングしたいテーマをインポートする](#シャドーイングしたいテーマをインポートする)
	- [ソースコードを参照しながらシャドーイングする](#ソースコードを参照しながらシャドーイングする)
- [おわりに](#おわりに)


---


## はじめに

Marp for VSCode の自作テーマ作成記録です．

基本的なやり方は "参考" に記載されている記事を見てください．

参考:
- [達人プログラマー 第3章 プレインテキスト](https://amzn.to/3dyvSaj)
- https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode


## 成果物

成果物は次の通りです．ご参考までにどうぞ．

- [marp_theme_typememo.css](https://gitlab.com/typememo/typememo_/-/blob/master/tools/res/marp_theme_typememo.css)  
`typememo` スタイルシート
- [example.pdf](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/marp-for-vscode-mytheme/tools/example.pdf)  
`typememo` スライド具体例


## 自作テーマ設定

vscode の設定ファイル `hogehoge.code-workspace` か `.vscode/settings.json`
に次のような設定を記載する必要があります．

```javascript
// in typememo.code-workspace
"settings": {
  "markdown.marp.themes": [
    "./tools/res/marp_theme_typememo.css"
  ]
},
```

上の例は `typememo.code-workspace` に `//tools/res/marp_theme_typememo.css`
を marp のテーマとして追加する設定を書いたものです．

適宜ご自身の開発環境に合わせて設定してください．


## 自作テーマ開発

[marp_theme_typememo.css](https://gitlab.com/typememo/typememo_/-/blob/master/tools/res/marp_theme_typememo.css)
というテーマを作ってみました．

カラーテーマは
[#232129](https://www.colordic.org/colorscheme/232129)
をベースカラーにしました．
[@原色大辞典さん](https://www.colordic.org/)，どうもありがとうございます！

PDF 版を見ていただけるとわかりやすいかと思います．

- [example.pdf](https://gitlab.com/typememo/typememo_/-/blob/master/blog/content/posts/tech/marp-for-vscode-mytheme/tools/example.pdf)

以下では，実装方法のエッセンスだけご紹介したいと思います．


### シャドーイングしたいテーマをインポートする

まずはシャドーイングしたいテーマをインポートします．

筆者は `gaia` テーマが好きなので `default` テーマや `uncover` テーマは使っていません．

```css
@import 'gaia';
```


### ソースコードを参照しながらシャドーイングする

インポートしたらあとは各テーマのソースコード
(e.g.
[gaia.scss](https://github.com/marp-team/marp-core/blob/master/themes/gaia.scss)
)
を参考にして変更したい箇所をシャドーイングしていきます．

例えば，各スライドは `<section>` タグで囲われているので，全スライドのフォントを変えたい場合は，

```css
section {
  font-family: "Helvetica Neue", "monospace";
}
```

のように書けば良いです．

あとはそれぞれの要素をお好きなようにシャドーイングしていけば良いです．


## おわりに

marp for vscode の自作テーマ開発記録の紹介でした．

難しいことは特になく css の知識を少し持っていればカスタマイズできるのですごく良いなと思いました．

[@yhatt](https://github.com/yhatt) さん，この場を借りて御礼申し上げます．

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

