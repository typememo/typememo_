---

title: asdf であらゆる言語のバージョンを一元管理する方法
slug: /tech/asdf-installation/
date: 2020-08-02
tags: [tech, asdf]
socialImage: ./img/iconfinder_terminal_298878.png
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [asdf インストール方法 (Mac)](#asdf-インストール方法-mac)
- [試しに Python をバージョン管理してみる](#試しに-python-をバージョン管理してみる)
  - [asdf python プラグインを追加する](#asdf-python-プラグインを追加する)
  - [asdf で python をバージョン管理する](#asdf-で-python-をバージョン管理する)
- [まとめ](#まとめ)

<!-- /TOC -->

----

## asdf インストール方法 (Mac)

asdf のインストール方法について紹介です。

情報源は [asdf-vm](https://asdf-vm.com/#/) です。

```shell
# for zsh
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.6
echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.zshrc
brew install \
  coreutils automake autoconf openssl \
  libyaml readline libxslt libtool unixodbc \
  unzip curl
# もしも以下のようなエラーが出た場合
# Error: Permission denied @ apply2files - /usr/local/lib/docker/cli-plugins
# 以下のコマンドを実行して、再度、brew install する
# sudo chown -R $(whoami) /usr/local
source .zshrc
asdf update
# 以下のログが出力されれば asdf のインストールは成功
# HEAD is now at 6207e42 Update version to 0.7.6
# Updated asdf to release v0.7.6
```

## 試しに Python をバージョン管理してみる
asdf を使って、Python をバージョン管理してみましょう。
`pyenv` を使って Python をバージョン管理している人も多いかと思います。
使い方は似ているので、この際，乗り換えてみてはいかがでしょうか？

### asdf python プラグインを追加する

以下のコマンドを実行して asdf の python プラグインを追加します．

```shell
asdf plugin add python
asdf plugin list
# python と出力されれば OK です。
```


### asdf で python をバージョン管理する

試しに Python 3.6.5 をインストールしてみましょう。

```shell
asdf install python 3.6.5
asdf global python 3.6.5
python --version
# 以下のログが出力されれば OK です
# Python 3.6.5 :: Anaconda, Inc.
asdf current
# 以下のログが出力されると思います
# python         3.6.5   
exit
```

遊びで、最新版の Python に切り替えてみましょう。

```shell
# for zsh
asdf install python latest
# 2020.02.25 時点で最新版は 3.8.1 です
asdf global python 3.8.1
python --version
# Python 3.8.1
```

簡単にバージョン管理できました。

## まとめ

asdf の基本コマンドは以下の通りです。

```shell
# for zsh
asdf plugin add $Language
asdf install $Language $Version
asdf global $Language $Version
```

最後までお読みただきありがとうございました。

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

