---

title: Dockerfile の書き方 – Docker syntax
slug: /tech/docker-dockerfile-syntax/
date: 2020-08-13
tags: [tech, docker]
image: ./img/thomas-lipke-kkXDhAUnxYI-unsplash.jpg
socialImage: ./img/thomas-lipke-kkXDhAUnxYI-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [具体例](#具体例)

<!-- /TOC -->

## はじめに

Dockerfile の書き方についての紹介です．

具体例を参考にしながら，解説していきます．

## 具体例

```docker
FROM ubuntu:bionic
LABEL maintainer="typememo@rikkyo.ac.jp"
LABEL description="DLT-Viewer for MacOS"

RUN echo "hello, world"
RUN ["/bin/bash", "-c", "echo hello"]
```

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

