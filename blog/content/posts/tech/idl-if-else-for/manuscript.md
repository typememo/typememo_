---

title: idl の if 文と for 文を fizzbuzz 問題で学ぶ
slug: /tech/idl-if-else-for/
date: 2019-08-17
tags: [tech, idl]
image: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
socialImage: ./img/markus-spiske-qjnAnF0jIGk-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [fizzbuzz 問題とは?](#fizzbuzz-問題とは)
- [fizzbuzz 問題実装例](#fizzbuzz-問題実装例)
- [if 文の使い方](#if-文の使い方)
  - [条件式１つ、実行文１つの場合](#条件式１つ実行文１つの場合)
  - [条件式１つ、実行文複数の場合](#条件式１つ実行文複数の場合)
  - [条件式複数、実行文複数の場合](#条件式複数実行文複数の場合)
- [for 文の使い方](#for-文の使い方)
  - [実行文1つの場合](#実行文1つの場合)
  - [実行文複数の場合](#実行文複数の場合)

<!-- /TOC -->

---

## はじめに

fizzbuzz 問題を題材にして idl の if 文と for 文の基本的な使い方を理解しましょう。

## fizzbuzz 問題とは?

fizzbuzz 問題については [wikipedia](https://ja.wikipedia.org/wiki/Fizz_Buzz)を参照してください。

要は、特定の数字で割り切れる場合は特定の文字列を出力するようなプログラムを組む遊びです。

fizzbuzz 問題は if 文 と for 文 ( while 文でも可 ) が使えれば簡単に実装できます。

idl を勉強し始めた人に取っては丁度いい課題だと思います。

## fizzbuzz 問題実装例

私の実装例をあげておきます ( 標準的な回答です )。

解答例のあとに if 文と for 文の使い方が書いてあります。

```python
pro idl_fizzbuzz
 
endNumberInt = 100
 
for i_number = 0, endNumberInt - 1 do begin
 
    ; 3 と 5 の両方で割り切れる場合
    if ( i_number mod 3 eq 0 ) and $
        ( i_number mod 5 eq 0 ) then begin
        print, 'FizzBuzz'
    endif
 
    ; 3 で割り切れる場合
    if i_number mod 3 eq 0 then begin
        print, 'Fizz'
    endif
 
    ; 5 で割り切れる場合
    if i_number mod 5 eq 0 then begin
        print, 'Buzz'
 
    ; 上記の条件式全てに一致しない場合
    endif else begin
        print, ':)'
    endelse
 
endfor
 
end
```

## if 文の使い方

基本的な文法は以下の3通りです。

### 条件式１つ、実行文１つの場合

```python
if (条件式) then (実行文)
```

### 条件式１つ、実行文複数の場合

```python
if (条件式) then begin
    (実行文1)
    (実行文2)
endif
```

### 条件式複数、実行文複数の場合

```python
if (条件式) then begin
    (実行文)
endif else begin
    (実行文)
endelse
```

この３通りの書き方を臨機応変に使い分けてください。

条件式で使う **比較演算子** については [idl の比較演算子まとめ](https://typememo.com/idl-comparison-operator/) をお読みください。

## for 文の使い方

基本的な文法は以下の2通りです。

### 実行文1つの場合

```python
for i = i_start, i_end do (実行文)    
```

### 実行文複数の場合

```python
for i = i_start, i_end do begin
    (実行文1)
    (実行文2)
endfor
```

この2通りの書き方を臨機応変に使い分けてください。

最後までお読みいただき、ありがとうございました：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

