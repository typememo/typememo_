---

title: iPhone で飲んだ水の量を記録するアプリ – Water
slug: /tech/ios-shortcut-apps-water/
date: 2020-08-04
tags: [tech, ios]
image: ./img/anastasia-taioglou-CTivHyiTbFw-unsplash.jpg
socialImage: ./img/anastasia-taioglou-CTivHyiTbFw-unsplash.jpg
imageAlt: eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [Water の使い方](#water-の使い方)
- [おわりに](#おわりに)

<!-- /TOC -->

## はじめに

iPhone で飲んだ水の量を手短に記録できるアプリを作りました。

その名も、[Water](https://www.icloud.com/shortcuts/42a30538eba04bdc96421352d45ab913)

使い方はシンプルで、アプリをタップして飲んだ水の量を入力するだけです。

水を飲むのってなんだかんだ大事なので、
[水を 2.5 L 飲むと気分が晴れるよ](https://yuchrszk.blogspot.com/2015/11/125.html) とか、
[食事の前に 500 ml の水を飲むと痩せるよ](https://yuchrszk.blogspot.com/2019/08/305.html) とか、
[水を 300 ml 飲むと集中力と記憶力が上がるよ](https://yuchrszk.blogspot.com/2016/11/blog-post_29.html) とか、
いろいろいい効果があるようなのです。

自分が１日にどれくらいどのタイミングで水を飲んでいたのか振り返ることができれば楽しいんじゃないかと思ってアプリを作りました。

Note:
> iPhone のショートカットライブラリ以外のショートカットを使用するには、
> 許可されていないショートカットを許可する必要があります。
> [許可されていないショートカットの許可方法はこちら](https://typememo.com/2020/02/ios-shortcut-allow-unofficial-shortcut/)

それでは、使い方を説明します。

## Water の使い方

アプリのインストールが終わったら、アプリをタップしてください。

すると、飲んだ水の量を入力するダイヤログが表示されます。

飲んだ水の量を ml 単位で入力し、OK をタップすると、ヘルスケアアプリの水分を表示します。

飲んだ時間帯と水分量、先週との比較などがひと目でわかります。

ぜひ、使い続けてみてください。

## おわりに

iOS で飲んだ水の量を記録できるアプリ Water の紹介でした。

自分もこのアプリで飲んだ水の量を記録した結果、
平日に比べて休日の水分量が約半分になっていることを知り、
休日は意識して水をたくさん飲むようにしています。

最後まで、お読みいただきありがとうございました：）

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

