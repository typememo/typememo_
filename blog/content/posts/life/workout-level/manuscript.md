---

title: Workout Level
slug: /life/workout-level/
date: 2020-09-02
tags: [life, workout]
image: ./img/victor-freitas-vjkM-0m34KU-unsplash.jpg
socialImage: ./img/victor-freitas-vjkM-0m34KU-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [ステータス](#ステータス)
- [目標重量](#目標重量)
- [履歴](#履歴)

---


## ステータス

Big3 + チンニング のステータスは次の通りです．

| Menu | Set 1 | Set 2 | Set 3 |
| :-- | :-- | :-- | :-- |
| ベンチプレス | 80kg x 8reps | 80kg x 8reps | 80kg x 8reps |
| スクワット | 110kg x 6reps | 110kg x 6reps | 110kg x 6reps |
| デッドリフト | 95kg x 8reps | 95kg x 8reps | 95kg x 8reps |
| チンニング | 0kg x 8reps | 0kg x 7reps | 0kg x 5reps |

> Note: Big3 は 60kg x 10reps のウォーミングアップ後の記録です．

> Note: レストはきっかり2分です．

> Note: 6回以下しか挙げられなかった重量を下げるようにしています．

> Note: 重量を下げつつ6セットやります．


## 目標重量

Big3 の各種目の目標重量は，

- ベンチプレス 120kg
- スクワット 150kg
- デッドリフト 150kg

なので，目標達成までにまだまだ時間が掛かりそうです．

ベンチプレスは体重の1.5倍，スクワットとデッドリフトは体重の2.0倍を目標重量としています．

RPG でレベルアップするのと同じ感覚で，日々トレーニングに励みます．


## 履歴

- 2020-10-15 スクワット 105kg クリア
- 2020-10-11 デッドリフト 90kg クリア
- 2020-10-06 ベンチプレス 75kg クリア
- 2020-09-24 スクワット 100kg クリア
- 2020-09-01 記事執筆開始

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

