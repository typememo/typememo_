---

title: 立教大学図書館が休館日でも図書返却は可能です
slug: /life/rikkyo-library-book-return/
date: 2019-09-29
tags: [life, rikkyo]
image: ./img/inaki-del-olmo-NIJuEQw0RKg-unsplash.jpg
socialImage: ./img/inaki-del-olmo-NIJuEQw0RKg-unsplash.jpg
imageAlt: eyecatch

---

## 返却場所: 図書館入口受付の返却ポスト

意外ではないかもしれませんが、**立教大学の図書館が休館日であっても、図書の返却は可能です。**

**返却場所は図書館入り口受付の返却ポスト**です。

立教大学の図書館は、月の終わりの日曜日はだいたい休館日です。

図書館の利用はできませんので、ご了承くださいませ。

[入試期間などはタッカーホール前小門脇 返却ポスト](http://library.rikkyo.ac.jp/librarypress/blog/2019/02/01/1670/)、だったりしますのでご注意ください。

詳細は[立教大学図書館ホームページ](http://library.rikkyo.ac.jp/)を参照してください。

最後までお読みいただきありがとうございました．

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

