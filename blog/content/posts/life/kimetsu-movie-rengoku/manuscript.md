---

title: 映画 鬼滅の刃 無限列車編 に猗窩座が登場すると思う理由
slug: /life/kimetsu-movie-rengoku/
date: 2020-10-13
tags: [life, movie, kimetsu]
image: ./img/cullan-smith-BdTtvBRhOng-unsplash.jpg
socialImage: ./img/cullan-smith-BdTtvBRhOng-unsplash.jpg
imageAlt: eyecatch

---


Contents

- [はじめに](#はじめに)
- [猗窩座が出る理由](#猗窩座が出る理由)
- [おわりに](#おわりに)


---


## はじめに

映画 鬼滅の刃 無限列車編に猗窩座が登場すると思う理由についてお話します．

**本記事はネタバレを含みます．**

原作をまだ読んでいない方は次の原作２巻を読んでください！  
- [鬼滅の刃 7](https://amzn.to/3doHRaD)
- [鬼滅の刃 8](https://amzn.to/3709ju3)

それでは参りましょう．


## 猗窩座が出る理由

[Youtube 劇場版「鬼滅の刃」無限列車編 特報第二弾](https://www.youtube.com/watch?v=hpLluEaD6XI)
概要欄より，

> 「俺は俺の責務を全うする！  
> ここにいる者は、誰も死なせない！」

原作をお読みの方なら分かると思います．
このセリフは煉獄さんが猗窩座との戦いの最終局面で口にした言葉です．
煉獄さんはこの言葉の後 `炎の呼吸 奥義 煉獄` を繰り出します．
しかし，猗窩座を仕留めることができずに惜しくも戦いに敗れて命を落としてしまいます．

このセリフが特報の概要欄に記載されていることから，猗窩座は登場すると考えました．
ある意味原作ファンへのサービスなのかなと思いました．

> 分かる人には分かるよね？

ということで猗窩座が出ることはほぼ確定でしょう．
むしろ猗窩座が出ないと映画として成り立たないと思います．

[LiSA 『炎』 -MUSiC CLiP-](https://www.youtube.com/watch?v=4DxL6IKmXx4)
の歌詞にも煉獄さんの言葉が至る所に散りばめられています．
普段は感極まることないのですが，この歌は感極まりました！
本当に映画を観に行くのが楽しみになりました．

さて，`映画 鬼滅の刃 無限列車編`，いや，`映画 鬼滅の刃 煉獄編` 観に行ってこよっと！


## おわりに

最後までお読みいただきありがとうございました．

[お手紙](https://ofuse.me/typememo) お待ちしております！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

