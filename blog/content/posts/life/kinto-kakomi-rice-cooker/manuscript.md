---

title: KINTO 土鍋炊飯で炊いたご飯が美味すぎた
slug: /life/kinto-kakomi-rice-cooker/
date: 2020-10-25
tags: [life, kinto]
image: ./img/paolo-bendandi-fw2djKXjEt4-unsplash.jpg
socialImage: ./img/paolo-bendandi-fw2djKXjEt4-unsplash.jpg
imageAlt: eyecatch

---

Contents

- [はじめに](#はじめに)
- [炊飯時間配分](#炊飯時間配分)
- [絶品白米! おこげ付き](#絶品白米-おこげ付き)
- [おわりに](#おわりに)
  - [愛用品](#愛用品)

---

## はじめに

[KINTO 土鍋炊飯](https://amzn.to/31nLJ6U)
で炊いたお米がめちゃめちゃ美味しかっので感想を紹介したいと思います．

生活の質をほんの少しだけ高められてすごく幸せだなと感じています．

気になる方は読み進めていただければと思います．

それでは参りましょう．

## 炊飯時間配分

我が家では米一合に対して次の時間配分で土鍋炊飯しています．

- 中火 8分30秒
- 極弱火 8分30秒
- 蒸らし 15分30秒 

[KINTO の公式サイトに掲載されている炊飯時間配分](https://kinto.co.jp/blogs/journal/exquisite-rice-with-kakomi-rice-cooker-kkmi)
と我が家の炊飯時間配分は若干異なります．
どこが異なるかと言うと，火にかける時間でして，我が家は硬めが好きなので少し長くなっています．

火にかける時間が総じて長いので黒焦げが付かないように全神経を集中させて火加減に注意を払っています．
お米に注意を向ける感覚もなかなか心地いいものです．

## 絶品白米! おこげ付き

そんでもって，土鍋炊飯したおこげ付きの白米がこちら！

![rice](./img/IMG_0622.jpg)

ちらほらとお焦げが付いていて，お米も芯が少しだけ残っていて我が家好みの炊き上がりでした．[KINTO の公式サイトに掲載されている炊飯時間配分](https://kinto.co.jp/blogs/journal/exquisite-rice-with-kakomi-rice-cooker-kkmi)
と我が家の炊飯時間配分は若干異なります．
どこが異なるかと言うと，火にかける時間でして，我が家は硬めが好きなので少し長くなっています．

火にかける時間が総じて長いので黒焦げが付かないように全神経を集中させて火加減に注意を払っています．

お米だけで食べて "美味い" と思ったのは初めてかもしれないです．

## おわりに

土鍋炊飯したお米は反則レベルと美味かったよ，という記事でした．

土鍋炊飯，本当に美味しいのでおすすめです．

最後までお読みいただきありがとうございました．

[山田武尊を応援していただけるととても嬉しいです！](https://ofuse.me/typememo)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．
