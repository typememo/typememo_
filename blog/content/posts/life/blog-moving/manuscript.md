---

title: ブログ引越し体験記
slug: /life/blog-moving/
date: 2020-08-14
tags: [life, moving]
image: ./img/erda-estremera-sxNt9g77PE0-unsplash.jpg
socialImage: ./img/erda-estremera-sxNt9g77PE0-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [はじめに](#はじめに)
- [記事をひたすら移行する](#記事をひたすら移行する)
  - [Gatsby に必要なフロントマターを追加する](#gatsby-に必要なフロントマターを追加する)
  - [記事の表現を微修正する](#記事の表現を微修正する)
  - [wordpress にリダイレクト設定する](#wordpress-にリダイレクト設定する)
  - [ちなみに不要な記事は無視した](#ちなみに不要な記事は無視した)
- [おわりに](#おわりに)

<!-- /TOC -->

----

## はじめに

ブログの環境を Wordpress + Xserver -> Gatsby + Netlify に変更しました．

ドメイン名を [typememo.com](https://typememo.com) -> [typememo.jp](https://typememo.jp) に変更しました．

そのため，いままでの３年間で書いた約100記事を [typememo.com](https://typememo.com) -> [typememo.jp](https://typememo.jp) に移行する必要があります．

この記事では，このブログ引越し体験記を取り留めもなく書き綴りました．

それでは参りましょう．

## 記事をひたすら移行する

### Gatsby に必要なフロントマターを追加する

Wordpress の記事をそのままコピペするだけでは，Gatsby で静的サイトを構築するときに必要なフロントマターが欠けていてダメでした．  
なので，必要なフロントマターを書くのが面倒臭かったです．  

### 記事の表現を微修正する

フロントマターを追加した後で各記事を読み直しまして，冗長な表現やわかりづらい表現を適宜修正しました．
2–3年前の記事とかは箇条書きを活用しておらず，とても読みづらい記事でした．
そんな読みづらい記事にも関わらず，3000回ほど読まれていました．
この場を借りて，御礼申し上げます．
これからも書き続けていきますので，お付き合いいただけると嬉しい限りです．

### wordpress にリダイレクト設定する

記事の微修正が済んだ後は，typememo.jp へリダイレクトされるように wordpress のリダイレクト項目に url を追加しました．
ブログ引越し中に typememo.com の記事にアクセスして typememo.jp にリダイレクトされたけど 404 ページが表示されてしまった方，ごめんなさい！
記事一つ一つを master ブランチに `push` して Netlify にデプロイさせるのが億劫だったので，いくつかの記事をまとめて master ブランチに `push` して Netlify にデプロイさせていました．
2020/08/13/Thu 時点で移行は終わっています．

### ちなみに不要な記事は無視した

なお，"もう，この記事は読まれないだろう．" と筆者が判断した記事は typememo.com のレンタルサーバーが凍結されたら読めなくなります．
必要ないと判断したのは，消費期限のある記事，完全な筆者なメモ記事などです．
もしも読みたい記事にアクセスして 404 ページが表示されたら，できる限り復元させますので筆者の [Twitter](https://twitter.com/typememo) までご連絡ください． 

## おわりに

結局，ブログを完全移行するのに１ヶ月ほどかかっていまいました．
ちまちまと一日で2-3記事移行していたので想定の範囲内ではありましたが，少し時間がかかってしまったなと感じています．
まぁでも完全に移行できたのでめでたしです．

今後とも，よろしくお願いします！

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

