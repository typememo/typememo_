---

title: My First Paper
slug: /science/my-first-paper/
date: 2020-07-19
tags: [science, venus]
image: ./img/annie-spratt-5cFwQ-WMcJU-unsplash.jpg
socialImage: ./img/annie-spratt-5cFwQ-WMcJU-unsplash.jpg
imageAlt: Eyecatch

---

Contents

<!-- TOC -->

- [金星大気における地形性重力波の鉛直伝播に対する中立層の影響](#金星大気における地形性重力波の鉛直伝播に対する中立層の影響)
- [Reference](#reference)

<!-- /TOC -->

----

## 金星大気における地形性重力波の鉛直伝播に対する中立層の影響

自分の修士の頃の研究が査読論文として，Earth Planets Space 誌 (EPS 誌) から出版されました．

初めての査読論文出版ということで，とても嬉しいです：）

ご協力いただいた方々，本当にありがとうございました！

タイトルは，**Influence of the cloud-level neutral layer on the vertical propagation of topographically generated gravity waves on Venus** です．

日本語に訳すと，**金星大気における地形性重力波の鉛直伝播に対する中立層の影響** です．

この論文の結論は，**東西波長の大きい地形性重力波は厚さ数kmの中立層があろうとも中立層より上空へと鉛直伝播する**　です．

今まではっきりしていなかった，中立層における重力波の振る舞いを数値的に論じた論文です．

金星大気力学の観点からすると，それなりに重要なポジションに位置する論文かなと思っています．

詳細を知りたい方は，次の URL から論文をダウンロードして読んでみてください．

URL: [https://earth-planets-space.springeropen.com/articles/10.1186/s40623-019-1106-7](https://earth-planets-space.springeropen.com/articles/10.1186/s40623-019-1106-7)

## Reference

金星について，より詳細に知りたい方は以下の２冊の本がおすすめです．

金星の研究をするなら必読・必携の２冊だと思います．

[Venus (University of Arizona Space Science Series)](https://amzn.to/2pTJ31l)

[Venus II (University of Arizona Space Science Series)](https://amzn.to/35wMf1O)

### 愛用品

- [Xiser Pro Trainer](https://amzn.to/3dJpY6k)  
１日中踏み続けられる強靭なステッパーでおすすめです．
- [HHKB Professional 墨](https://amzn.to/3dGtLS1)
x [HHKB キートップセット 白](https://amzn.to/2HzzPQD)  
ボディは墨色キートップは白色なのでめちゃめちゃ目に優しいのでおすすめです．
- [Apple Magic Mouse 2](https://amzn.to/3ksb2Mk)  
トラックパッドは指が攣りそうになりますけどマウスはその心配が無いのでおすすめです．
- [Apple MacMini](https://amzn.to/34jvsSt)  
ミニマルでパワフルなデスクトップ PC なので個人的に大好きなのでおすすめです．
- [iiyama Display 27inch FullHD](https://amzn.to/37l524B)  
鮮明すぎない画面で目も疲れにくいですし何より高さ調節できるのが最高なのでおすすめです．
- [KINTO UNITEA 550ml](https://amzn.to/2TaLLKJ)  
500ml の大容量でこの綺麗なデザインは他にみたことがないのでおすすめです．

