import { graphql } from "gatsby"
import PostsPage from "gatsby-theme-blog/src/gatsby-theme-blog-core/components/posts"

export default PostsPage

export const query = graphql`
  query postsQueryAndPostsQuery {
    site {
      siteMetadata {
        title
        social {
          name
          url
        }
      }
    }
    allBlogPost(sort: { fields: [date, title], order: DESC }, limit: 1000) {
      edges {
        node {
          id
          excerpt
          slug
          title
          date(formatString: "YYYY.MM.DD")
          tags
          image {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`
