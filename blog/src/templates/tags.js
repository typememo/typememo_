import React from "react"
import PropTypes from "prop-types"

// Components
import { css } from "theme-ui"
import { Link, graphql } from "gatsby"
import Layout from "gatsby-theme-blog/src/components/layout"
import Footer from "gatsby-theme-blog/src/components/home-footer"

const Tags = ({ 
    pageContext, 
    data, 
    location,
}) => {
  const { tag } = pageContext
  const { edges } = data.allMarkdownRemark
  const tagHeader = `#${tag}`

  return (
    <div>
      <Layout location={location} title={data.site.siteMetadata.title}>
      <h2>{tagHeader}</h2>
      <ul>
        {edges.map(({ node }) => {
          const { slug } = node.frontmatter
          const { title } = node.frontmatter
          const { date } = node.frontmatter
          return (
            <li key={slug}>
              <Link 
                to={slug}
                css={css({
                  textDecoration: `none`,
                  color: "#232129",
                  "&:hover": {
                    paddingBottom: 0.5,
                    borderBottom: "solid thin #232129"
                  },
                  })}
              >
                <small css={css({ marginRight: 2 })}>
                  {date}
                </small>
                {title}
              </Link>
            </li>
          )
        })}
      </ul>
      <Link 
        to="/tags/"
        css={css({
          textDecoration: `none`,
          color: "#232129",
          "&:hover": {
          paddingBottom: 0.5,
          borderBottom: "solid thin #232129"
          },
        })}
      >
        All tags
      </Link>
      <Footer />
      </Layout>
    </div>
  )
}

Tags.propTypes = {
  pageContext: PropTypes.shape({
    tag: PropTypes.string.isRequired,
  }),
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      totalCount: PropTypes.number.isRequired,
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            frontmatter: PropTypes.shape({
              title: PropTypes.string.isRequired,
              slug: PropTypes.string.isRequired,
            }),
          }),
        }).isRequired
      ),
    }),
  }),
}

export default Tags

export const pageQuery = graphql`
  query($tag: String) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          frontmatter {
            title
            slug
            date(formatString: "YYYY.MM.DD")
          }
        }
      }
    }
  }
`
