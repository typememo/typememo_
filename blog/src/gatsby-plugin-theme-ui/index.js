import baseTheme from "gatsby-theme-blog/src/gatsby-plugin-theme-ui/index"
import "prismjs/themes/prism-twilight.css"
import "prismjs/plugins/line-numbers/prism-line-numbers.css"

export default {
  ...baseTheme,
  fontSizes: [12, 14, 16, 24, 28, 36, 42, 64, 96],
  space: [0, 4, 8, 16, 32, 64, 128, 256],
  colors: {
    ...baseTheme.colors,
    primary: '#232129',
  },
  fonts: {
    heading: 'Source Sans Pro, Helvetica Neue, sans-selif',
    body: 'Note Sans, Note Sans JP, Open Sans, Roboto, Arial, sans-selif',
    monospace: 'Source Code Pro, monospace',
  },
  styles: {
    ...baseTheme.styles,
    inlineCode: {
      bg: "#f0f0f0",
    },
    a: {
      color: "#195ab5",
      textDecoration: "none",
      "&:hover": {
        paddingBottom: 0.5,
        borderBottom: "solid thin"
      },
    },
    h1: {
      fontSize: 4,
      padding: "0.5em",
      background: "#f3f3f2",
      borderLeft: "solid 5px #393e4f",
    },
    h2: {
      fontSize: 3,
    },
    h3: {
      fontSize: 2,
    },
  },
}