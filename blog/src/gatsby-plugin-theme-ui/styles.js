import defaultStyles from "gatsby-theme-blog/src/gatsby-plugin-theme-ui/styles"
export default {
  ...defaultStyles,
  img: {
    margin: "30px",
  }
}