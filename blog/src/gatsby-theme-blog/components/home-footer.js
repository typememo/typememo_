import React from "react"
import { css } from "theme-ui"
import Bio from "./bio"

const Footer = ({ socialLinks }) => (
  <footer
    css={css({
      mt: 4,
      pt: 3,
    })}
  >
    <hr></hr>
    <Bio />
    <small>
      本ブログで述べられている内容は，<br></br>
      すべて私の個人的な意見に基づくものであり，<br></br>
      所属する組織 / 団体とは一切関係ありません．<br></br>
    </small>
  </footer>
)
export default Footer