import React from "react"
import Layout from "gatsby-theme-blog/src/components/layout"
import SEO from "./seo"
import Footer from "./home-footer"
import PostList from "gatsby-theme-blog/src/components/post-list"

const Posts = ({ 
  location, 
  posts, 
  siteTitle, 
  socialLinks, 
}) => (
  <Layout location={location} title={siteTitle}>
    <SEO title="Home" />
    <main>
      <PostList posts={posts} />
      <small>Total <b>{posts.length}</b> articles</small>
    </main>
    <Footer socialLinks={socialLinks} />
  </Layout>
)

export default Posts
