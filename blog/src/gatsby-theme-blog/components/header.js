import React from "react"
import { Link } from "gatsby"
import { css, Styled } from "theme-ui"
import Search from "../components/search"

const Title = ({ children }) => {
  return (
    <Styled.h1
      css={css({
        my: 0,
        fontSize: 3,
        padding: "0",
        background: "transparent",
        borderLeft: "none",    
      })}
    >
      <Styled.a
        as={Link}
        css={css({
          color: `inherit`,
          boxShadow: `none`,
          textDecoration: `none`,
        })}
        to={`/`}
      >
        {children}
      </Styled.a>
    </Styled.h1>
  )
}

const Header = ({ children, title, ...props }) => (
      <header>
      <div
        css={css({
          maxWidth: `container`,
          mx: `auto`,
          px: 3,
          pt: 4,
        })}
        style={{
          "flexWrap": "wrap",
          "display": "flex",
        }}
      >
        <div
          css={css({
            display: `flex`,
            justifyContent: `space-between`,
            alignItems: `center`,
            mb: 4,
          })}
        >
          <Title {...props}>{title}</Title>
          {children}
        </div>
        <Search className={`${props.className}`} />
       </div>
      </header>
)
export default Header
