import React from "react"
import { Styled } from "theme-ui"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter } from "@fortawesome/free-brands-svg-icons"
import { faGitlab } from "@fortawesome/free-brands-svg-icons"
import { faEnvelope } from "@fortawesome/free-solid-svg-icons"

/**
 * Change the content to add your own bio
 */

export default function Bio() {
  return (
    <div>
      <Styled.code>
        山田 武尊 / 立教大学(理学) / ACCESS CO LTD
      </Styled.code>
      <Styled.div style={{ display: "flex" }}>
      <Styled.a
        href="https://twitter.com/typememo/"
        style={{
          marginRight: 10,
          paddingBottom: "unset",
          borderBottom: "unset",
          color: "#232129",
        }}
      >
        <FontAwesomeIcon icon={faTwitter} />
      </Styled.a>
      <Styled.a
        href="https://gitlab.com/typememo/"
        style={{
          marginRight: 10,
          paddingBottom: "unset",
          borderBottom: "unset",
          color: "#232129",
        }}
      >
        <FontAwesomeIcon icon={faGitlab} />
      </Styled.a>
      <Styled.a
        href="https://ofuse.me/typememo"
        style={{
          marginRight: 10,
          paddingBottom: "unset",
          borderBottom: "unset",
          color: "#232129",
        }}
      >
        <FontAwesomeIcon icon={faEnvelope} />
      </Styled.a>
      </Styled.div>
    </div>
  )
}
