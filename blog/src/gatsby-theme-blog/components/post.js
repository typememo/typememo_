import React from "react"
import { MDXRenderer } from "gatsby-plugin-mdx"
import { Link } from "gatsby"
import { Flex , css } from "theme-ui"

import Layout from "gatsby-theme-blog/src/components/layout"
import PostTitle from "gatsby-theme-blog/src/components/post-title"
import PostDate from "gatsby-theme-blog/src/components/post-date"
import PostFooter from "gatsby-theme-blog/src/components/post-footer"
import SEO from "./seo"
import PostHero from "./post-hero"

const Post = ({
  data: {
    post,
    site: {
      siteMetadata: { title },
    },
  },
  location,
  previous,
  next,
}) => (
  <Layout location={location} title={title}>
    <SEO
      title={post.title}
      description={post.excerpt}
      imageSource={
        post.socialImage
          ? post.socialImage?.childImageSharp?.fluid.src
          : post.image?.childImageSharp?.fluid.src
      }
      keywords={post.keywords}
      imageAlt={post.imageAlt}
    />
    <main>
      <PostHero post={post} />
      <PostTitle>{post.title}</PostTitle>
      <Flex css={css({ alignItems: `baseline` })}>
        <PostDate>{post.date}</PostDate>
        <small css={{ marginLeft: 10 }}>{
            post.tags.map(tag => (
              <Link 
                to={`/tags/${tag}/`}
                css={css({
                  marginRight: 1,
                  textDecoration: `none`,
                  color: "#195ab5",
                  "&:hover": {
                    paddingBottom: 0.5,
                    borderBottom: "solid thin"
                  },
                })}
              >
                #{tag}
              </Link>
            ))
          }
        </small>
      </Flex>
      <MDXRenderer>{post.body}</MDXRenderer>
    </main>
    <PostFooter {...{ previous, next }} />
  </Layout>
)

export default Post
