/** @jsx jsx */
import { Styled, jsx, Flex, css } from "theme-ui"
import { Link } from "gatsby"

const PostLink = ({ title, slug, date }) => (
    <Flex
      css={
        css({
          alignItems: `center`
        })
      }
    >
      <Styled.p
        sx={{
          fontWeight: "normal",
        }}
      >
        <Styled.a
          as={Link}
          sx={{
            color: "#232129",
            textDecoration: `none`,
          }}
          to={slug}
        >
        <small css={{ marginRight: 10 }}>{date}</small>
        <b>{title || slug}</b>
        </Styled.a>
      </Styled.p>
    </Flex>
)

export default PostLink