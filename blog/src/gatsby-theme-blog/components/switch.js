import React from "react"
import ReactSwitch from "react-switch"

export const Switch = props => <ReactSwitch {...props} />

Switch.defaultProps = {
  height: 0,
  width: 0,
}

export default Switch