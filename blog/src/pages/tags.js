import React from "react"
import PropTypes from "prop-types"

// Utilities
import kebabCase from "lodash/kebabCase"

// Components
import { Helmet } from "react-helmet"
import { Link, graphql } from "gatsby"
import { css } from "theme-ui"
import Layout from "gatsby-theme-blog/src/components/layout"
import Footer from "gatsby-theme-blog/src/components/home-footer"

const TagsPage = ({
  data: {
    allMarkdownRemark: { group },
    site: {
      siteMetadata: { title },
    },
  },
  location,
}) => (
  <Layout location={location} title={title}>
  <div>
    <Helmet title={title} />
    <div>
      <h2>Tags</h2>
      {group.map(tag => (
        <div css={css({ display: "inline-box", margin: 2, })}>
          <Link 
          key={tag.fieldValue}
          to={`/tags/${kebabCase(tag.fieldValue)}/`}
          css={css({
            border: "solid thin #aaa",
            borderRadius: 3,
            padding: 2,
            textDecoration: `none`,
            color: "#195ab5",
            "&:hover": {
              backgroundColor: "#3498db",
              color: "#fff",
            },
            })}
          >
            {tag.fieldValue}
          </Link>
        </div>
      ))}
    </div>
  </div>
  <Footer />
  </Layout>
)

TagsPage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      group: PropTypes.arrayOf(
        PropTypes.shape({
          fieldValue: PropTypes.string.isRequired,
          totalCount: PropTypes.number.isRequired,
        }).isRequired
      ),
    }),
    site: PropTypes.shape({
      siteMetadata: PropTypes.shape({
        title: PropTypes.string.isRequired,
      }),
    }),
  }),
}

export default TagsPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(limit: 2000) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`