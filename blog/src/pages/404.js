import { Link } from "gatsby"
import React from "react"
import SEO from "gatsby-theme-blog/src/components/seo"
const NotFoundPage = () => (
  <>
    <SEO title="404: Not found" />
    <div
      style={{
        width: `100vw`,
        height: `100vh`,
        display: `flex`,
        justifyContent: `center`,
        alignItems: `center`,
      }}
    >
      <div>
        <h1
          style={{
            fontSize: "165px",
            margin: 0,

          }}
        >
          404
        </h1>
        <p>
          {"Please check the URL."}
        </p>
        <Link 
          to="/" 
          className="link-style"
        >
          Back to Home
        </Link>
      </div>
    </div>
  </>
)
export default NotFoundPage